![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image5.png)

# Cumulus Networks Professional Services

### Cumulus Linux Boot Camp
>Built for Cumulus Linux v3.7.11  
>December 12, 2019

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image12.gif)


<!-- AIR:page -->
## Table of Contents
***
* [Lab 1: Verifying Connectivity and Modifying Initial Configuration](#lab-1-verifying-connectivity-and-modifying-initial-configuration)
    - [Connect to Your Lab via SSH](#connect-to-your-lab-via-ssh)
    - [Apply a Cumulus Linux License](#apply-a-cumulus-linux-license)
* [Lab 2: Using the Network Command Line Utility (NCLU)](#lab-2-using-the-network-command-line-utility-nclu)
    - [Add Configuration Using NCLU](#add-configuration-using-nclu)
    - [Rollback the Configuration Using NCLU](#rollback-the-configuration-using-nclu)
    - [Enable the HTTP API for NCLU](#Enable-the-HTTP-API-for-NCLU)
* [Lab 3: Interface Configuration](#lab-3-interface-configuration)
    - [Configure loopback addresses on leaf01 and leaf02](#configure-loopback-addresses-on-leaf01-and-leaf02)
    - [Verify loopback IP address configuration](#verify-loopback-ip-address-configuration)
    - [Configure bond on leaf01 and leaf02](#configure-bond-on-leaf01-and-leaf02)
    - [Configure bridge and access ports on leaf01 and leaf02](#configure-bridge-and-access-ports-on-leaf01-and-leaf02)
    - [Verify bridge configuration on leaf01 and leaf02](#verify-bridge-configuration-on-leaf01-and-leaf02)
    - [Configure server01 and server02 network interfaces](#configure-server01-and-server02-network-interfaces)
    - [Verify connectivity from server01 to server02](#verify-connectivity-from-server01-to-server02)
    - [Verify MAC address table on leaf01 and leaf02](#verify-mac-address-table-on-leaf01-and-leaf02)
* [Lab 4a: MLAG Configuration](#lab-4a-mlag-configuration)
    - [Remove all previous configuration](#remove-all-previous-configuration)
    - [Configure CLAG on leaf01 and leaf02](#configure-clag-on-leaf01-and-leaf02)
    - [Verify CLAG relationship](#verify-clag-relationship)
    - [Create distributed bond between leaf01 and leaf02 to server01 and server02](#create-distributed-bond-between-leaf01-and-leaf02-to-server01-and-server02)
    - [Configure Server01 and Server02 to be bond interfaces](#configure-server01-and-server02-to-be-bond-interfaces)
    - [Verify Configuration](#verify-configuration)
* [Lab 4b: MLAG + Routing High Availability](#lab-4b-mlag-routing-high-availability)
    - [Configure SVI and VRR on leaf01 and leaf02](#configure-svi-and-vrr-on-leaf01-and-leaf02)
    - [Configure server02 to use new VLAN and SVI](#configure-server02-to-use-new-vlan-and-svi)
    - [Verify Connectivity to VRR address](#verify-connectivity-to-vrr-address)
* [Lab 5a: OSPF](#lab-5a-ospf)
    - [Remove all previous configuration](#remove-all-previous-configuration)
    - [Apply loopback address to leaf01, leaf02 and spine01](#apply-loopback-address-to-leaf01-leaf02-and-spine01)
    - [Configure OSPF unnumbered on spine01, leaf01 and leaf02](#configure-ospf-unnumbered-on-spine01-leaf01-and-leaf02)
    - [Verify OSPF connectivity between fabric nodes](#verify-ospf-connectivity-between-fabric-nodes)
    - [Advertise Loopbacks from leaf01, leaf02 and spine01 into fabric](#advertise-loopbacks-from-leaf01-leaf02-and-spine01-into-fabric)
    - [Verify that OSPF is advertising the loopback routes](#verify-that-ospf-is-advertising-the-loopback-routes)
* [Lab 5b: iBGP deployment](#lab-5b-ibgp-deployment)
    - [Configure iBGP on spine01, leaf01 and leaf02](#configure-ibgp-on-spine01-leaf01-and-leaf02)
    - [Verify BGP connectivity between fabric nodes](#verify-bgp-connectivity-between-fabric-nodes)
* [Lab 6: EVPN](#lab-6-evpn)
    - [Configure EVPN on spine01, leaf01, and leaf02](#configure-evpn-on-spine01,-leaf01,-and-leaf02)
    - [Verify BGP connectivity between fabric nodes](#verify-bgp-connectivity-between-fabric-nodes)
    - [Configure SVI and VRR on leaf01 and leaf02](#configure-svi-and-vrr-on-leaf01-and-leaf02)
    - [Create VLAN to VNI mappings](#create-vlan-to-vni-mappings)
    - [Advertise the VNIs and hosts on VLAN 10 and VLAN 20](#advertise-the-vnis-and-hosts-on-vlan-10-and-vlan-20)
    - [Configure server01 and server02 trunk links](#configure-server01-and-server02-trunk-links)
    - [Verify that BGP is advertising the routes](#verify-that-bgp-is-advertising-the-routes)
* [Lab 7: Automation with Ansible](#h.aqd5s858h3xu)
    - [Check on ZTP in the Environment](#h.p6x4er6jrvxm)
    - [Setup the Automation Environment](#h.s2swt97am472)
    - [Run the Ansible playbook](#h.35mhyqtkm0tn)
* [Lab 8: Troubleshooting via Traditional Methods](#h.k51dmeedhcl0)
    - [Setup Prescriptive Topology Manager](#h.7mf0avs1uzio)
    - [Check PTM status](#h.rn99x54kq18w)
    - [Check and Clear Counters](#h.jchgheub6x0a)
    - [View Environmental Information](#h.9ekw8alot4o7)
    - [Fail a Sensor in Vx to Observe the Result](#h.hddkkx4pbjhs)
    - [Generate and Inspect a cl-support file](#h.9bl1ed9fyah1)
* [Lab 9: Network Services](#h.upho1i3m8cyu)
    - [Configure a DNS Server](#h.b3uv4dmmh0ev)
    - [Configure Remote Syslog Transmission](#h.3bnvxmki4uzn)
    - [Configure a Pre Login Message](#h.i6rbowfr1y5i)
    - [Configure a Post Login Message](#h.hm9b45nyvw8i)
    - [Enable SNMP](#h.qjxpsq6g10gb)
* [Lab 10: eBGP Unnumbered (Optional)](#h.8txu9rake7yo)
    - [Apply loopback address to leaf01, leaf02 and spine01](#h.4jy27gh5c0ko)
    - [Remove all previous configuration](#h.wsikq5mjq085)
    - [Configure BGP unnumbered on spine01, leaf01 and leaf02](#h.m0vc04xtdwj)
    - [Verify BGP connectivity between fabric nodes](#h.iskkf3ye7kj0)
    - [Verify that BGP is advertising the routes](#h.jzbt466p5vbv)
* [Lab 11: Snapshots and Package Management (Optional)](#h.zckyatp9tqju)
    - [Installing Apache2](#h.mjdt6usi9edj)
    - [Creating a Manual Snapshot](#h.8878eqg5tua2)
    - [Restore a Single File from a Previous Snapshot](#h.pwiheuud3ej6)
    - [Perform a Rollback](#h.69kyw66xasmh)
* [Lab 12: Configuring Routing Enhancements (Optional)](#h.edz3k8n3sdxg)
    - [Use route-maps to advertise loopback addresses](#h.ffmmoamm6q3l)
    - [Verify Route Advertisement](#h.1ovdqadqsqt3)
    - [Tune the BGP timers](#h.1sk3yvyo6dji)
    - [Verify the BGP timers are set](#h.2j8j7dym2yqg)
    - [Configure Bidirectional Forwarding Detection (BFD)](#h.25wj64z5ypsa)
    - [Verify BFD is in use](#h.yxon9ih250nw)
    - [Configure PTM link verification for all routing adjacencies](#h.uo2k1bjvlag3)



<!-- AIR:page -->
## Hands On Lab Modules
***

### Lab 1: Verifying Connectivity and Modifying Initial Configuration


**Objective:**

You will confirm that you can access your workbench switches. This
involves first connecting to a Cumulus Networks Education Lab workbench
via SSH which redirects you to an out-of-band management server
(oob-mgmt-server), from which you can access your switches. At this
point, we will install a Cumulus Linux license as you would on a real
(non-virtual) switch.


**Dependencies on other Labs:**
- None


**Goals:**
- Successfully SSH into your oob-mgmt-server.
- From your oob-mgmt-server, access your switches via SSH.
- Install and apply a Cumulus Linux License



**Procedure:**  

Check your e-mail for a message sent from Cumulus that will look like
the following.

```
 From: "education@cumulusnetworks.com" Subject: "Log-in instructions for Cumulus Networks Labs"
                                                                       
 Hello!
                                                                       
 To access the materials for the class, please visit:
 URL: https://cumulusnetworks.box.com/v/CLBootCamp
 Password: **********
 
 To access the lab portion of the class, please ssh to the workbench  
 using the following credentials.
 URL: ssh workbenchXX@vx.wb.cumulusnetworks.com
 Password: **********

 Thank you,
 The Cumulus Networks Education Team

```
_**Note:** If you have not received the message, please let your instructor know_


#### Connect to Your Lab via SSH

Open an SSH client, and access your workbench

- Windows users: [Download PuTTY from Chiark](https://www.chiark.greenend.org.uk/\~sgtatham/putty/latest.html)
- Mac users: Use the **Terminal** application.
- Linux users: Open a **Bash** shell.


```
user@laptop:~$​ ​ssh workbench11@vx.wb.cumulusnetworks.com
### Please enter the credentials for your education workbench ###  
### Credentials Verified... Logging Into Education Environment. ###  
############################################################################ 
#
# Welcome to the Cumulus Networks Education Lab! You will now be logged into 
# the out of band management server for your lab environment.
# 
############################################################################

Warning: Permanently added '10.1.11.126' (ECDSA) to the list of known hosts.
                                                 _
      _______   x x x                           | |
 ._  <_______~ x X x   ___ _   _ _ __ ___  _   _| |_   _ ___
(' \  ,' || `,        / __| | | | '_ ` _ \| | | | | | | / __|
 `._:^   ||   :>     | (__| |_| | | | | | | |_| | | |_| \__ \
     ^T~~~~~~T'       \___|\__,_|_| |_| |_|\__,_|_|\__,_|___/
     ~"     ~"


############################################################################
#
#                     Out Of Band Management Station
#
############################################################################
cumulus@oob-mgmt-server:~$
```
_**Note:** Workbench11 used above is an example. Use the specific workbench
number and password provided in your e-mail._


Or via PuTTY:


![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image10.png)


You now have an SSH session to your workbench, and you will be at the
BASH prompt on the oob-mgmt-server.

**(Optional)** Repeat the previous step multiple times to create separate
SSH session windows for each device if you would like.

**On oob-mgmt-server: SSH to Leaf01.**

```
cumulus@oob-mgmt-server:~$ ​ssh leaf01
The authenticity of host 'leaf01 (192.168.0.11)' can't be established.
ECDSA key fingerprint is 43:ae:76:84:80:fd:15:de:90:8b:62:ff:3f:ca:7f:77.
Are you sure you want to continue connecting (yes/no)? ​yes
Warning: Permanently added 'leaf01,192.168.0.11' (ECDSA) to the list of known hosts

Welcome to Cumulus VX (TM)

Cumulus VX (TM) is a community supported virtual appliance designed for
experiencing, testing and prototyping Cumulus Networks' latest technology.
For any questions or technical support, visit our community site at:
http://community.cumulusnetworks.com

The registered trademark Linux (R) is used pursuant to a sublicense from LMI,
the exclusive licensee of Linus Torvalds, owner of the mark on a world-wide
basis.
cumulus@leaf01:~$
```

#### Apply a Cumulus Linux License

  \
**On leaf01:**  
View the license on the oob-mgmt-server by downloading and displaying the license with the curl command.  
Then apply the license using cl-license. 
Lastly, view where the license is placed on disk once installed. 

_Note: this process is not actually required in Vx but is performed for parity with real hardware._



```
cumulus@leaf01:~$ ​curl http://192.168.0.254/license.lic
this is a fake license
cumulus@leaf01:~$ ​sudo cl-license -i http://192.168.0.254/license.lic 
--2017-09-25 16:21:57-- http://192.168.0.254/license.lic
Connecting to 192.168.0.254:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 31
Saving to: ‘/tmp/lic.mvCHDM’

/tmp/lic.mvCHDM     100%[=====================>]      31  --.-KB/s   in 0s

2017-09-25 16:21:57 (4.61 MB/s) - ‘/tmp/lic.mvCHDM’ saved [31/31]

License file installed.

cumulus@leaf01:~$ ​cat /etc/cumulus/.license 
this is a fake license
cumulus@leaf01:~$
```

**On leaf01:**  
Restart switchd to apply the license.  
Check the status of switchd to make sure it is running.


```
cumulus@leaf01:~$ ​sudo systemctl restart switchd.service 
cumulus@leaf01:~$ ​sudo systemctl status switchd.service
●​ switchd.service - Cumulus Linux Switch Daemon
  Loaded: loaded (/lib/systemd/system/switchd.service; enabled)
  Drop-In: /etc/systemd/system/switchd.service.d
           └─override.conf
  Active: ​active (running)​ since Mon 2017-09-25 16:22:30 UTC; 10s ago
  Process: 2553 ExecStopPost=/bin/sh -c /usr/bin/killall -q -s 36 clagd || exit 0
(code=exited, status=0/SUCCESS)
 Main PID: 2577 (switchd)
   CGroup: /system.slice/switchd.service
           └─2577 /usr/sbin/switchd -vx

Sep 25 16:22:30 leaf01 switchd[2577]: Initializing Cumulus Networks switch ...hd
Sep 25 16:22:30 leaf01 switchd[2577]: switchd.c:1707 switchd version 1.0-cl3u17
Sep 25 16:22:30 leaf01 switchd[2577]: switchd.c:1708 switchd cmdline: -vx
Sep 25 16:22:30 leaf01 switchd[2577]: switchd.c:446 /config/ignore_non_swps...UE
Sep 25 16:22:30 leaf01 switchd[2577]: switchd.c:446 /config/logging changed...FO
Sep 25 16:22:30 leaf01 systemd[1]: Started Cumulus Linux Switch Daemon.
Sep 25 16:22:30 leaf01 switchd[2577]: switchd.c:1403 Startup complete.
Hint: Some lines were ellipsized, use -l to show in full.
```


_**This concludes Lab 1**_




<!-- AIR:page -->
### Lab 2: Using the Network Command Line Utility (NCLU)

This lab will allow students to become familiar with the operation of
NCLU. Students will apply a configuration, validate the configuration,
commit the configuration then rollback the configuration. 

**Dependencies on other Labs:**
- None

**Goals:**
- Become familiar with NCLU
- Use NCLU to Add Configuration
- Employ Commit and Rollback operations
- Enable the HTTP API

**Procedure:**
1.  **On leaf01:**  
    Check all options available to configure Cumulus Linux via NCLU.  
    Press \<tab\> after typing "net add" to see all options. 

```
cumulus@leaf01:~$ ​net add <tab>
acl               :  access-list
bgp               :  Border Gateway Protocol
bond              :  bond, port-channel, etc
bridge            :  a layer2 bridge
clag              :  Multi-Chassis Link Aggregation
control-plane     :  A virtual interface for traffic to/from the CPU
dhcp              :  Dynamic Host Configuration Protocol
dns               :  Domain Name Service
dot1x             :  Configure, Enable, Delete or Show IEEE 802.1X EAPOL
forwarding        :  Packet forwarding parameters
hostname          :  local hostname
igmp              :  Internet Group Management Protocol
interface         :  An interface, such as swp1, swp2, etc.
lnv               :  Lightweight Network Virtualization
loopback          :  Loopback
mpls              :  Multiprotocol Label Switching
msdp              :  Multicast Source Discovery Protocol
neighbor          :  A BGP, OSPF, PIM, etc neighbor
nexthop-group     :  Nexthop group
ospf              :  Open Shortest Path First (OSPFv2)
ospf6             :  Open Shortest Path First (OSPFv3)
pbr               :  Policy Based Routing
pbr-map           :  Policy Based Routing map
pim               :  Protocol Independent Multicast
policer-template  :  Template for configured police settings
ptp               :  Precision Time Protocol
routing           :  Routing protocols
snmp-server       :  Configure the SNMP server
syslog            :  Set syslog logging
time              :  Time
username          :  Defines an SNMPv3 username.
vlan              :  Virtual Local Area Network
vrf               :  Virtual routing and forwarding
vxlan             :  Virtual Extensible LAN
```


#### Add Configuration Using NCLU

2.  **On leaf01:**  
    Apply an "alias" to swp1 interface.  
    Aliases are descriptions of the interface.  
    Interface aliases will show up automatically around the system and when the interface is queried via SNMP.

```
cumulus@leaf01:~$ ​net add interface swp1 alias Connection to Server01:eth1
```

3.  **On leaf01:**  
    View the staged changes in the commit buffer.

```
cumulus@leaf01:~$ ​net pending
--- /etc/network/interfaces 2019-12-17 14:05:34.281308000 +0000
+++ /run/nclu/ifupdown2/interfaces.tmp 2019-12-17 15:35:19.335280583 +0000 
@@ -3,10 +3,15 @@

 source /etc/network/interfaces.d/*.intf

 # The loopback network interface
 auto lo
 iface lo inet loopback

 # The primary network interface
 auto eth0
 iface eth0 inet dhcp
+
+auto swp1
+iface swp1
+    alias Connection to Server01:eth1
+

net add/del commands since the last "net commit"
================================================

User     Timestamp                   Command
-------  --------------------------
--------------------------------------------------------
cumulus  2019-12-17 15:35:16.348451  net add interface swp1 alias Connection to Server01:eth1
```

4.  **On leaf01:**  
    Commit the staged changes in the buffer with a custom description. 

```
cumulus@leaf01:~$ ​net commit description "Lab2 Commit"
--- /etc/network/interfaces 2019-12-17 14:05:34.281308000 +0000
+++ /run/nclu/ifupdown2/interfaces.tmp 2019-12-17 15:37:02.860442238 +0000 
@@ -3,10 +3,15 @@

 source /etc/network/interfaces.d/*.intf

 # The loopback network interface
 auto lo
 iface lo inet loopback

 # The primary network interface
 auto eth0
 iface eth0 inet dhcp
+
+auto swp1
+iface swp1
+    alias Connection to Server01:eth1
+

net add/del commands since the last "net commit"
================================================
User     Timestamp                   Command
-------  --------------------------
--------------------------------------------------------
cumulus  2019-12-17 15:35:16.348451  net add interface swp1 alias Connection to Server01:eth1
```

5.  **On leaf01:**  
    View the NCLU commit history for the switch.

```
cumulus@leaf01:~$ ​net show commit history
# Date Description
-- ------------------------------- ---------------- 
12​ Tue 17 Dec 2019 03:37:03 PM UTC nclu Lab2 Commit
```

_**Note:** The reference number for the commit is on the left, in this
example it is 12._


6.  **On leaf01:**  
    Show the contents of the last commit.

```
cumulus@leaf01:~$ ​net show commit last
--- /.snapshots/12/snapshot/etc/network/interfaces 
+++ /.snapshots/13/snapshot/etc/network/interfaces 
@@ -10,3 +10,8 @@
 # The primary network interface
 auto eth0
 iface eth0 inet dhcp
+
+auto swp1
+iface swp1
+    alias Connection to Server01:eth1
+
```

_**Note:** Alternatively, you can show the contents of any commit
by referencing the specific commit number instead of the keyword
"last."_


7.  **On leaf01:**  
    Check the NCLU logs to verify the same status.

```
cumulus@leaf01:~$ ​cat /var/log/netd-history.log
2019-12-17 15:37:03.072070 - net commit 
=======================================
--- /etc/network/interfaces 2019-12-17 14:05:34.281308000 +0000
+++ /run/nclu/ifupdown2/interfaces.tmp 2019-12-17 15:37:02.966442548 +0000 
@@ -3,10 +3,15 @@

 source /etc/network/interfaces.d/*.intf

 # The loopback network interface
 auto lo
 iface lo inet loopback

 # The primary network interface
 auto eth0
```

8.  **On leaf01:**  
    Verify the alias has been applied. 

```
cumulus@leaf01:~$ ​net show interface swp1 
    Name  MAC                Speed  MTU   Mode
--  ----  -----------------  -----  ----  -------
UP  swp1  44:38:39:00:00:54  1G     1500  Default

Alias
-----
Connection to Server01:eth1

cl-netstat counters
-------------------
RX_OK  RX_ERR  RX_DRP  RX_OVR  TX_OK  TX_ERR  TX_DRP  TX_OVR
-----  ------  ------  ------  -----  ------  ------  ------
   15       0       0       0     18       0       0       0
```

#### Rollback the Configuration Using NCLU

9.  **On leaf01:**  
    Rollback the last commit to remove the configuration.

```
cumulus@leaf01:~$ ​net rollback last
```

_**Note:** Alternatively, you can rollback to any commit by referencing the
unique commit number, the same number identified in the note for step 6._

10. **On leaf01:**  
    Verify rollback was applied successfully. 
    At this point the port should be in the admin down ( "ADMDN" ) state because the configuration has been reverted to a point before the interface alias existed and the port was defined.

```
cumulus@leaf01:~$ ​net show interface swp1

       Name  MAC                Speed  MTU   Mode
-----  ----  -----------------  -----  ----  -------------
ADMDN  swp1  44:38:39:00:00:54  N/A    1500  NotConfigured
```


| Important things to observe: |
|------------------------------|
| - The commit description is different than the commit number |
| - Rollback can use either description or number |
| - Rollback undoes any changes applied within NCLU |

####  Enable the HTTP API for NCLU

11. **On leaf01:**  
    Start the service to enable the HTTP API.

```
cumulus@leaf01:~$ ​sudo systemctl enable restserver
Created symlink from /etc/systemd/system/basic.target.wants/restserver.service to /lib/systemd/system/restserver.service.

cumulus@leaf01:~$ ​sudo systemctl start restserver
```

12. **On leaf01:**  
    Configure nginx to allow remote connections.  
    Using nano or vim, open and edit the file `/etc/nginx/sites-available/nginx-restapi.conf`.

```
cumulus@leaf01:~$ ​sudo nano /etc/nginx/sites-available/nginx-restapi.conf
```
Find the server configuration block, and comment out the current listen
line, and uncomment the listen line 3 below.

```
server {
    error_log  /var/log/nginx/error_restserver.log warn;
    limit_req zone=python_cumulus_restapi_one burst=100 nodelay;
    
    # All interactions with restserver must be secure.
    ssl_protocols TLSv1.2;
    ssl_ciphers AES256-SHA256:AES256-SHA:AES128-SHA:GCM:ECDHE;
    
    # Some NCLU commands require a long time to complete.  If these numbers change,
    # be sure to change to related parameters for Gunicorn in the associated service.
    keepalive_timeout 5m 5m;
    # Comment the line below, then
    # listen localhost:8080 ssl;
    # uncomment the listen line below to listen on port 8080 for all
    # IPv4 and IPv6 addresses, including IPv6 link local.
    listen [::]:8080 ipv6only=off ssl;
    # then run the command below so nginx uses the new config
    #     sudo systemctl restart nginx
```

Save the file and exit from the editor.  
Restart the nginx service to apply the configuration change.

```
cumulus@leaf01:~$ ​sudo systemctl restart nginx
```

13. **On leaf01:**  
    _Exit leaf01_ and test the NCLU API.  
    This command is long and prone to human error.  
    It is recommended to copy/paste this into the CLI to lessen the possibility of
    transcribing it incorrectly.

```
cumulus@leaf01:~$​ exit
logout
Connection to leaf01 closed.
cumulus@oob-mgmt-server:~$ ​curl -X POST -k -u 'cumulus:CumulusLinux!' -H "Content-Type: application/json" -d '{"cmd": "show interface"}' https://192.168.0.11:8080/nclu/v1/rpc
State  Name  Spd  MTU    Mode      LLDP                    Summary
-----  ----  ---  -----  --------  ----------------------  -------------------------
UP     lo    N/A  65536  Loopback                          IP: 127.0.0.1/8
       lo                                                  IP: ::1/128
UP     eth0  1G   1500   Mgmt      oob-mgmt-switch (swp6)  IP: 192.168.0.11/24(DHCP)
```

14. **On oob-mgmt-server:**  
    Use the API to collect JSON output instead of standard text by changing the  
    command statement to `show interface json`

```
cumulus@oob-mgmt-server:~$ ​curl -X POST -k -u 'cumulus:CumulusLinux!' -H "Content-Type: application/json" -d '{"cmd": "show interfac json"}' https://192.168.0.11:8080/nclu/v1/rpc | json_pp
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1768  100  1738  100    30   7764    134 --:--:-- --:--:-- --:--:--  7793
{
   "lo" : {
      "mode" : "Loopback",
      "linkstate" : "UP",
      "speed" : "N/A",
      "iface_obj" : {
         "description" : "",
         "min_links" : "",
         "members" : {},
         "dhcp_enabled" : false,
         "lldp" : null,
         "ip_address" : {
            "allentries" : [
               "127.0.0.1/8",
               "::1/128"
            ]
         },
         "native_vlan" : null,
         "asic" : null,
         "mac" : "00:00:00:00:00:00",
         "vlan" : null,
         "mtu" : 65536,
         "vlan_list" : [],
         "lacp" : {
            "rate" : "",
            "sys_priority" : "",
            "partner_mac" : "",
            "bypass" : ""
         },
<...output not shown for brevity...>

cumulus@oob-mgmt-server:~$
```

**This concludes Lab 2.**


<!-- AIR:page -->
### Lab 3: Interface Configuration

**Objective:**  

This lab will configure various types of interfaces. First, a bond will be configured between leaf01 and leaf02.  
Then the bond will be configured as a trunk to pass vlan100 and vlan200. Finally, the connections between leafs and servers will be configured as access ports.  
This will create a Layer 2 environment where server01 will be able to communicate with server02 in the same subnet.

By the end of this lab, we'll have the following topology implemented:

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image6.png)

**Dependencies on other Labs:**
- None

**Goals:**
- Configure loopback addresses for leaf01 and leaf02
- Configure a bond between leaf01 and leaf02
- Configure a bridge
- Create a trunk port and access port


**Procedure:**

#### Configure loopback addresses on leaf01 and leaf02


> Interface Configuration Details

| Interface↓ \\ Switch→ | leaf01 | leaf02 |
|-----------------------|:------:|:------:|
| Loopback IP           | 10.255.255.1/32 | 10.255.255.2/32 |


1.  **On leaf01:**  
    Assign an ip address to the loopback interface. 

```
cumulus@leaf01:~$ ​net add loopback lo ip address 10.255.255.1/32 
cumulus@leaf01:~$ ​net commit
```

2.  **On leaf02:**  
    Assign an ip address to the loopback interface. 

```
cumulus@leaf02:~$ ​net add loopback lo ip address 10.255.255.2/32 
cumulus@leaf02:~$ ​net commit
```

#### Verify loopback IP address configuration

3.  **On leaf01:**  
    Check that the address has been applied.

```
cumulus@leaf01:~$ ​net show interface lo
    Name  MAC                Speed  MTU    Mode
--  ----  -----------------  -----  -----  --------
UP  lo    00:00:00:00:00:00  N/A    65536  Loopback

IP Details
------------------------- --------------- 
IP:                       127.0.0.1/8 
IP:                       ​10.255.255.1/32 
IP:                       ::1/128
IP Neighbor(ARP) Entries: 0
```

4.  **On leaf02:**  
    Check that the address has been applied.

```
cumulus@leaf02:~$ ​net show interface lo
    Name  MAC                Speed  MTU    Mode
--  ----  -----------------  -----  -----  --------
UP  lo    00:00:00:00:00:00  N/A    65536  Loopback

IP Details
------------------------- --------------- 
IP:                       127.0.0.1/8 
IP:                       ​10.255.255.2/32 
IP:                       ::1/128
IP Neighbor(ARP) Entries: 0
```


| Important things to observe: |
|------------------------------|
| - Loopback has user-defined IP address as well as home address assigned to |
| - Loopback has a predefined default configuration on Cumulus Linux. Make sure not to delete it. |



#### Configure bond on leaf01 and leaf02

>Bond Configuration Details

| Bond↓   \\   Switch→ | leaf01 | leaf02 |
|----------------------|:------:|:------:|
| Bond name | BOND0 | BOND0 |
| Bond members | swp49,swp50 | swp49,swp50 |


5.  **On leaf01:**  
    Create a bond with members swp49 and swp50.

```
cumulus@leaf01:~$ ​net add bond BOND0 bond slaves swp49-50 
cumulus@leaf01:~$​ ​net commit
```

6.  **On leaf02:**  
    Create a bond with members swp49 and swp50.

```
cumulus@leaf02:~$ ​net add bond BOND0 bond slaves swp49-50 
cumulus@leaf02:~$​ ​net commit
```

7.  **On leaf01:**  
    Check status of the bond between two switches.  
    Verify that the bond is operational by checking the status of the bond and its members.  
    See the highlighted output below to verify that your lab output matches. 

```
cumulus@leaf01:~$ ​net show interface bonds 
   Name  Speed  MTU Mode    Summary
-- ----- ----- ---- ------- ---------------------------------- 
UP​ BOND0 2G    1500 802.3ad Bond Members: ​swp49(UP), swp50(UP)

cumulus@leaf01:~$ ​net show interface bondmems 
   Name  Speed  MTU Mode    Summary
-- ----- ----- ---- ------- ----------------- 
UP​ swp49 1G    1500 LACP-UP Master: ​BOND0(UP) 
UP​ swp50 1G    1500 LACP-UP Master: ​BOND0(UP)
```

8.  **On leaf02:**  
    Check status of the bond between two switches.  
    Verify that the bond is operational by checking the status of the bond and its members.  
    See the highlighted output below to verify that your lab output matches.

```
cumulus@leaf02:~$ ​net show interface bonds 
   Name  Speed  MTU Mode    Summary
-- ----- ----- ---- ------- ---------------------------------- 
UP​ BOND0 2G    1500 802.3ad Bond Members: ​swp49(UP), swp50(UP)

cumulus@leaf02:~$ ​net show interface bondmems 
   Name  Speed  MTU Mode    Summary
-- ----- ----- ---- ------- ----------------- 
UP​ swp49 1G    1500 LACP-UP Master: ​BOND0(UP) 
UP​ swp50 1G    1500 LACP-UP Master: ​BOND0(UP)
```


| Important things to observe: |
|------------------------------|
| -   The speed of the bond is the cumulative speed of all member interfaces |
| -   Bond member interface status and bond interface status are displayed in output |
| -   LLDP remote port information is included in bond member status output |



#### Configure bridge and access ports on leaf01 and leaf02

>Bridge Configuration Details

| Bridge↓   \\   Switch→ | leaf01 | leaf02 |
|------------------------|:------:|:------:|
| Bridge vlans | 10,20 | 10,20 |
| Bridge members | BOND0,swp1 | BOND0,swp2 |
| Bridge access port | swp1 | swp2 |
| Bridge access vlan | 10 | 10 |


9.  **On leaf01:**  
    Create a bridge with vlans 10 and 20.

```
cumulus@leaf01:~$ ​net add bridge bridge vids 10,20
```

10. **On leaf01:**  
    Add swp1 and BOND0 as a member to the bridge.  
    _Note: The name BOND0 is case sensitive in all places._

```
cumulus@leaf01:~$ ​net add bridge bridge ports BOND0
```

11. **On leaf01:**  
    Make swp1 an access port for vlan 10.

```
cumulus@leaf01:~$ ​net add interface swp1 bridge access 10
```

12. **On leaf01:**  
    Commit the changes. 

```
cumulus@leaf01:~$ ​net commit
```

13. **On leaf02:**  
    Repeat the same steps but use swp2 as the access port towards the server. 

```
cumulus@leaf02:~$ ​net add bridge bridge vids 10,20 
cumulus@leaf02:~$ ​net add bridge bridge ports BOND0 
cumulus@leaf02:~$ ​net add interface swp2 bridge access 10 
cumulus@leaf02:~$​ ​net commit
```

_Note: The section below is provided for easier copying and pasting._

**leaf01**
```
net add bridge bridge vids 10,20
net add bridge bridge ports BOND0
net add interface swp1 bridge access 10
net commit
```

**leaf02**
```
net add bridge bridge vids 10,20
net add bridge bridge ports BOND0
net add interface swp2 bridge access 10
net commit
```


#### Verify bridge configuration on leaf01 and leaf02

14. **On leaf01:**  
    Verify the configuration on leaf01 by checking that swp1 and BOND0 are part of the bridge. 

```
cumulus@leaf01$ ​net show bridge vlan

Interface      VLAN  Flags
-----------  ------  ---------------------
swp1             10  PVID, Egress Untagged
BOND0             1  PVID, Egress Untagged
                 10 
                 20
```

15. **On leaf02:**  
    Verify the same configuration on leaf02 by checking that swp2 and BOND0 are part of the bridge.

```
cumulus@leaf02$ ​net show bridge vlan 

Interface      VLAN  Flags
-----------  ------  ---------------------
swp2             10  PVID, Egress Untagged
BOND0             1  PVID, Egress Untagged
                 10 
                 20
```

| Important things to observe: |
|------------------------------|
| -   Access ports are only a single line with the VLAN associated with the port |
| -   Trunk ports are multiple lines with each VLAN associated with the trunk listed |

| On leaf01: |
|------------|
| -   swp1 should be an access port in vlan 10 |
| -   BOND0 should be a trunk for vlan10 and vlan20, with a native vlan of 1 (PVID) |


| On leaf02: |
|------------|
| -   swp2 should be an access port in vlan 10 |
| -   BOND0 should be a trunk for vlan10 and vlan20, with a native vlan of 1 (PVID) |



#### Configure server01 and server02 network interfaces

_Note: All configuration done on server01 is done with the ifupdown2 package installed._

>Server Configuration Details

| Configuration↓ \\  Switch→ | server01 | server02 |
|----------------------------|:--------:|:--------:|
| Interface | eth1 | eth2 |
| IP address | 10.0.10.101/24 | 10.0.10.102/24 |


16. **On oob-mgmt-server:**  
    Run the server configuration script to configure interfaces on the Ubuntu servers. 

```
cumulus@oob-mgmt-server:~$ ​config server lab3
INFO: Changing into /home/cumulus/BootcampAutomation...


 ### Configuring Servers for Lab3...
 
 
TASK [Gathering Facts] ************************************************************
TASK [Copy Interfaces Configuration File] *****************************************
TASK [Restart Networking] *********************************************************


 ### Server Configuration Successful! ###
 
 
cumulus@oob-mgmt-server:~$
```

_The following four steps are verification that is done on server01 and server02.  
Please SSH to server01 and server02 to complete these steps._



17. **On server01:**  
    Confirm the eth1 interface is configured with 10.0.10.101/24 as expected. 

```
cumulus@server01:~$ ​ifquery eth1 
auto eth1
iface eth1
      address ​10.0.10.101/24
      
cumulus@server01:~$ ​ip address show eth1
4: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:03:00:11:11:01 brd ff:ff:ff:ff:ff:ff 
    inet ​10.0.10.101/24​ scope global eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::203:ff:fe11:1101/64 scope link
       valid_lft forever preferred_lft forever
cumulus@server01:~$
```

18. **On server02:**  
    Confirm the eth2 interface is configured with 10.0.10.102/24 as expected. 

```
cumulus@server02:~$ ​ifquery eth2 
auto eth2
iface eth2
       address ​10.0.10.102/24

cumulus@server02:~$ ​ip addr show eth2
5: eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:03:00:22:22:02 brd ff:ff:ff:ff:ff:ff 
    inet ​10.0.10.102/24​ scope global eth2
       valid_lft forever preferred_lft forever
    inet6 fe80::203:ff:fe22:2202/64 scope link
       valid_lft forever preferred_lft forever
cumulus@server02:~$
```

#### Verify connectivity from server01 to server02

19. **On server01:**  
    Test connectivity from server01 to server02.

```
cumulus@server01:~$ ​ping 10.0.10.102
PING 10.0.10.102 (10.0.10.102) 56(84) bytes of data.
64 bytes from 10.0.10.102: icmp_seq=1 ttl=64 time=2.02 ms
64 bytes from 10.0.10.102: icmp_seq=2 ttl=64 time=2.28 ms
^C
--- 10.0.10.102 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 2.028/2.156/2.284/0.128 ms
```

20. **On server02:**  
    Test connectivity from server02 to server01.

```
cumulus@server02:~$​ ​ping 10.0.10.101
PING 10.0.10.101 (10.0.10.101) 56(84) bytes of data.
64 bytes from 10.0.10.101: icmp_seq=1 ttl=64 time=1.26 ms
64 bytes from 10.0.10.101: icmp_seq=2 ttl=64 time=1.76 ms
^C
--- 10.0.10.101 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 1.265/1.512/1.760/0.250 ms
```

#### Verify MAC address table on leaf01 and leaf02


21. **On leaf01 and leaf02:**  
    Check to verify that the MAC addresses are learned correctly.

```
cumulus@leaf01:~$​ net show bridge macs
VLAN      Master  Interface  MAC                TunnelDest  State      Flags  LastSeen
--------  ------  ---------  -----------------  ----------  ---------  -----  --------
1         bridge  BOND0      44:38:39:00:00:3e                                00:00:11
1         bridge  BOND0      44:38:39:00:00:40                                00:00:11
10        bridge  BOND0      00:03:00:22:22:02                                00:00:05
10        bridge  swp1       00:03:00:11:11:01                                00:00:05
untagged  bridge  BOND0      44:38:39:00:00:3d              permanent         00:07:36
untagged  bridge  swp1       44:38:39:00:00:54              permanent         00:07:36
```

```
cumulus@leaf02:~$ ​net show bridge macs
VLAN      Master  Interface  MAC                TunnelDest  State      Flags  LastSeen
--------  ------  ---------  -----------------  ----------  ---------  -----  --------
1         bridge  BOND0 44:38:39:00:00:3d                                     00:00:01
1         bridge  BOND0 44:38:39:00:00:3f                                     00:00:12
10        bridge  BOND0 00:03:00:11:11:01                                     00:00:44
10        bridge  swp2  00:03:00:22:22:02                                     00:00:17
untagged  bridge  BOND0 44:38:39:00:00:3e                   permanent         00:07:54
untagged  bridge  swp2  44:38:39:00:00:5b                   permanent         00:07:54
```

| Important things to observe: |
|------------------------------|
| -   MAC addresses of servers should be learned on BOND0 and swp interface of switch |
| -   Server01 and server02 are in same subnet, so all traffic will be switched via layer 2 |


**This concludes Lab 3.**




<!-- AIR:page -->
### Lab 4a: MLAG Configuration 


**Objective:**

This lab will configure leaf01 and leaf02 as CLAG peers.  
Implementing CLAG (a.k.a. MLAG) allows server01 and server02 to have more than one uplink configured in a bond.  
CLAG is commonly deployed in scenarios where highly-available host connectivity is desired.  
The first steps in this lab will be to remove all configuration performed in Lab 3.

**By the end of this lab, the following topology will be deployed:**

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image8.png)

**Dependencies on other Labs:**
-   None

**Goals:**
-   Configure a bond between leaf01 and leaf02
-   Configure a bridge
-   Create a trunk port and access port

**Procedure:**

#### Remove all previous configuration

1.  **On leaf01 and leaf02:**  
    Use NCLU to reset leaf01 and leaf02 to default configuration. 

```
cumulus@leaf01:~$ ​net del all

*************************************************************************** 
*************************************************************************** 
NOTE: 'net del all' will stage a default configuration file for each of the 
packages that are supported by NCLU. This default configuration may be 
different from the configuration that you would get should you remove and 
re-install each of these packages. Please review the staged changes via 
"net pending". 
*************************************************************************** 
*************************************************************************** 
cumulus@leaf01:~$​ ​net commit
```

```
cumulus@leaf02:~$ ​net del all

*************************************************************************** 
*************************************************************************** 
NOTE: 'net del all' will stage a default configuration file for each of the 
packages that are supported by NCLU. This default configuration may be 
different from the configuration that you would get should you remove and 
re-install each of these packages. Please review the staged changes via 
"net pending". 
*************************************************************************** 
*************************************************************************** 
cumulus@leaf02:~$​ ​net commit
```

2.  **On leaf01 and leaf02:**  
    Restore the NTP configuration to include the workbench NTP server.

```
cumulus@leaf01:~$ ​net add time ntp server 192.168.0.254 iburst 
cumulus@leaf01:~$ ​net del time ntp server 0.cumulusnetworks.pool.ntp.org 
cumulus@leaf01:~$ ​net del time ntp server 1.cumulusnetworks.pool.ntp.org 
cumulus@leaf01:~$ ​net del time ntp server 2.cumulusnetworks.pool.ntp.org 
cumulus@leaf01:~$ ​net del time ntp server 3.cumulusnetworks.pool.ntp.org 
cumulus@leaf01:~$​ ​net commit
```

```
cumulus@leaf02:~$ ​net add time ntp server 192.168.0.254 iburst 
cumulus@leaf02:~$ ​net del time ntp server 0.cumulusnetworks.pool.ntp.org 
cumulus@leaf02:~$ ​net del time ntp server 1.cumulusnetworks.pool.ntp.org 
cumulus@leaf02:~$ ​net del time ntp server 2.cumulusnetworks.pool.ntp.org 
cumulus@leaf02:~$ ​net del time ntp server 3.cumulusnetworks.pool.ntp.org 
cumulus@leaf02:~$​ ​net commit
```

>Note: The section below is provided for easier copying and pasting.

```
net add time ntp server 192.168.0.254 iburst
net del time ntp server 0.cumulusnetworks.pool.ntp.org
net del time ntp server 1.cumulusnetworks.pool.ntp.org
net del time ntp server 2.cumulusnetworks.pool.ntp.org
net del time ntp server 3.cumulusnetworks.pool.ntp.org
net commit
```

3.  **On leaf01:**  
    Validate that NTP is able to connect to the oob-mgmt-server.  
    Your output should not show the word ".INIT." as part of the reference id (refid) field.

```
cumulus@leaf01:~$ ​net show time ntp servers
remote           refid      st t when poll reach   delay   offset  jitter
============================================================================== 
*oob-mgmt-server 10.16.161.1      5 u   13  64     1   1.144   -1.899    0.104
```

#### Configure CLAG on leaf01 and leaf02

>CLAG Configuration Details

| Setting↓   \\   Switch→ | leaf01 | leaf02 |
|-------------------------|:------:|:------:|
| CLAG interface | swp49,swp50 | swp49,swp50 |
| CLAG system MAC | 44:38:39:FF:00:01 | 44:38:39:FF:00:01 |
| CLAG role | PPrimary | Secondary |
| CLAG peer Backup IP | 192.168.0.12 | 192.168.0.11 |


4.  **On leaf01:**  
    Configure CLAG.

```
cumulus@leaf01:~$ ​net add clag peer sys-mac 44:38:39:FF:00:01 interface swp49-50 primary backup-ip 192.168.0.12 
cumulus@leaf01:~$ ​net commit
```

5.  **On leaf02:**  
    Configure CLAG. 

```
cumulus@leaf02:~$ ​net add clag peer sys-mac 44:38:39:FF:00:01 interface swp49-50 secondary backup-ip 192.168.0.11 
cumulus@leaf02:~$ ​net commit
```

#### Verify CLAG relationship

6.  **On leaf01 and leaf02:**  
    Check that clag has peered correctly. 

```
cumulus@leaf01:~$ ​net show clag 
The peer is alive
     Our Priority, ID, and Role: 1000 44:38:39:00:00:3d primary
    Peer Priority, ID, and Role: 2000 44:38:39:00:00:3e secondary
          Peer Interface and IP: peerlink.4094 fe80::4638:39ff:fe00:3e (linklocal) 
                      Backup IP: ​192.168.0.12 (active)
                     System MAC: 44:38:39:ff:00:01
```

```
cumulus@leaf02:~$ ​net show clag 
The peer is alive
     Our Priority, ID, and Role: 2000 44:38:39:00:00:3e secondary
    Peer Priority, ID, and Role: 1000 44:38:39:00:00:3d primary
          Peer Interface and IP: peerlink.4094 fe80::4638:39ff:fe00:3d (linklocal) 
                      Backup IP: ​192.168.0.11 (active)
                     System MAC: 44:38:39:ff:00:01
```

7.  **On leaf01 and leaf02:**  
    Evaluate the interface configuration created by NCLU to support the CLAG relationship.

```
cumulus@leaf01:~$ ​ifquery -a 
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto swp1
iface swp1

auto swp49
iface swp49

auto swp50
iface swp50

auto bridge
iface bridge
        bridge-ports peerlink
        bridge-vlan-aware yes
        
auto peerlink
iface peerlink
        bond-slaves swp49 swp50
        
auto peerlink.4094
iface peerlink.4094
        clagd-backup-ip 192.168.0.12
        clagd-peer-ip linklocal
        clagd-priority 1000
        clagd-sys-mac 44:38:39:FF:00:01
```

```
cumulus@leaf02:~$ ​ifquery -a 
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto swp2
iface swp2

auto swp49
iface swp49

auto swp50
iface swp50

auto bridge
iface bridge
        bridge-ports peerlink
        bridge-vlan-aware yes
        
auto peerlink
iface peerlink
        bond-slaves swp49 swp50
        
auto peerlink.4094
iface peerlink.4094
        clagd-backup-ip 192.168.0.11
        clagd-peer-ip linklocal
        clagd-priority 2000
        clagd-sys-mac 44:38:39:FF:00:01
```

>Important things to observe:
>* Notice how clag configuration automatically creates the following information:
>  - Peerlink bond
>  - CLAG control connection via peerlink subinterface
>  - Bridge with peerlink as member
>* There should be two sets of CLAG verification
>  - Verify peer is alive via control connection
>  - Verify backup link is active via backup IP



#### Create distributed bond between leaf01 and leaf02 to server01 and server02

>Distributed Bond Configuration Details for server01

| Setting↓   \\   Switch→ | leaf01 | leaf02 |
|-------------------------|:------:|:------:|
| server01 BOND name | SERVER01 | SERVER01 |
| server01 BOND member | swp1 | swp1 |
| server01 BOND clag-id | 1 | 1 |
| server01 boND vlan access port | 10 | 10 |

>Distributed Bond Configuration Details for server02

| Setting↓   \\   Switch→ | leaf01 | leaf02 |
|-------------------------|:------:|:------:|
| server02 BOND name | SERVER02 | SERVER02 |
| server02 BOND member | swp2 | swp2 |
| server02 BOND clag-id | 2 | 2 |
| server02 boND vlan access port | 10 | 10 |

_Note: All configuration done on server01 is done with the ifupdown2 package preinstalled._


8.  **On leaf01:**  
    Create a bond to server01 and assign it to clagid 1 

```
cumulus@leaf01:~$ ​net add clag port bond SERVER01 interface swp1 clag-id 1
```

9.  **On leaf01:**  
    Create a bond to server02 and assign it to clagid 2 

```
cumulus@leaf01:~$ ​net add clag port bond SERVER02 interface swp2 clag-id 2
```

10. **On leaf01:**  
    Assign the SERVER01 and SERVER02 bond as an access port on vlan 10 

```
cumulus@leaf01:~$ ​net add bond SERVER01 bridge access 10 
cumulus@leaf01:~$ ​net add bond SERVER02 bridge access 10
```

11. **On leaf01:**  
    Commit all the changes. 

```
cumulus@leaf01:~$ ​net commit
```

12. **On leaf02:**  
    Repeat steps 8-11. 

```
cumulus@leaf02:~$ ​net add clag port bond SERVER01 interface swp1 clag-id 1 
cumulus@leaf02:~$ ​net add clag port bond SERVER02 interface swp2 clag-id 2 
cumulus@leaf02:~$ ​net add bond SERVER01 bridge access 10 
cumulus@leaf02:~$ ​net add bond SERVER02 bridge access 10 
cumulus@leaf02:~$ ​net commit
```

>Note: The section below is provided for easier copying and pasting.

```
net add clag port bond SERVER01 interface swp1 clag-id 1
net add clag port bond SERVER02 interface swp2 clag-id 2
net add bond SERVER01 bridge access 10
net add bond SERVER02 bridge access 10
net commit
```

#### Configure Server01 and Server02 to be bond interfaces

>Note: All configuration done on server01 and server02 is done with the
ifupdown2 package installed which will make the configurations look very
similar to what is required in Cumulus Linux.

**Server Bond Configuration Details**

| Setting↓   \\   Switch→ | server01 | server02 |
|-------------------------|:--------:|:--------:|
| Bond name | uplink | uplink |
| Bond IP address | 10.0.10.101/24 | 10.0.10.102/24 |


13. **On oob-mgmt-server:**  
    Run the server configuration script to configure interfaces on the Ubuntu servers. 

```
cumulus@oob-mgmt-server:~$ ​config server lab4a
 INFO: Changing into /home/cumulus/BootcampAutomation...
 ### Configuring Servers for Lab4a...


TASK [Gathering Facts] ************************************************************
TASK [Copy Interfaces Configuration File] *****************************************
TASK [Restart Networking] *********************************************************
​
 ### Server Configuration Successful! ### 

cumulus@oob-mgmt-server:~$
```

14. **On server01:**  
    Confirm interface IP addresses are allocated to the uplink bond as expected. 

```
cumulus@server01:~$ ​ifquery uplink 
auto uplink
iface uplink
        address ​10.0.10.101/24 bond-mode 802.3ad
        bond-miimon 100 
        bond-lacp-rate 1 
        bond-min-links 1 
        bond-xmit-hash-policy layer3+4 
        bond-slaves eth1 eth2
        post-up ip route add 10.0.0.0/8 dev uplink via 10.0.10.1
        post-up ip link set uplink promisc on

cumulus@server01:~$ ​ip addr show uplink
9: uplink: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 00:03:00:11:11:02 brd ff:ff:ff:ff:ff:ff 
    inet ​10.0.10.101/24​ scope global uplink
       valid_lft forever preferred_lft forever
    inet6 fe80::203:ff:fe11:1102/64 scope link
       valid_lft forever preferred_lft forever
cumulus@server01:~$
```

15. **On server02:**  
    Confirm interface IP addresses are allocated to the Uplink bond as expected.

```
cumulus@server02:~$ ​ifquery uplink
auto uplink
iface uplink
        address ​10.0.10.102/24 bond-mode 802.3ad
        bond-miimon 100 
        bond-lacp-rate 1 
        bond-min-links 1 
        bond-xmit-hash-policy layer3+4 
        bond-slaves eth1 eth2
        post-up ip route add 10.0.0.0/8 dev uplink via 10.0.10.1
        post-up ip link set uplink promisc on

cumulus@server02:~$ ​ip addr show uplink
10: uplink: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 00:03:00:22:22:02 brd ff:ff:ff:ff:ff:ff 
    inet ​10.0.10.102/24​ scope global uplink
       valid_lft forever preferred_lft forever
    inet6 fe80::203:ff:fe22:2202/64 scope link
       valid_lft forever preferred_lft forever
cumulus@server02:~$
```

#### Verify Configuration

16. **On server01:**  
    Test connectivity from server01 to server02. 

```
cumulus@server01:~$ ​ping 10.0.10.102
PING 10.0.10.102 (10.0.10.102) 56(84) bytes of data.
64 bytes from 10.0.10.102: icmp_seq=1 ttl=64 time=2.02 ms
64 bytes from 10.0.10.102: icmp_seq=2 ttl=64 time=2.28 ms
^C
--- 10.0.10.102 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 2.028/2.156/2.284/0.128 ms
```

17. **On server02:** 
    Test connectivity from server02 to server01.

```
cumulus@server02:~$ ​ping 10.0.10.101
PING 10.0.10.101 (10.0.10.101) 56(84) bytes of data.
64 bytes from 10.0.10.101: icmp_seq=1 ttl=64 time=1.26 ms
64 bytes from 10.0.10.101: icmp_seq=2 ttl=64 time=1.76 ms
^C
--- 10.0.10.101 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 1.265/1.512/1.760/0.250 ms
```

18. **On leaf01 and leaf02:**  
    Check clag settings to verify that peer interfaces are seen.

```
cumulus@leaf01:~$ ​net show clag 
The peer is alive
     Our Priority, ID, and Role: 1000 44:38:39:00:00:3d primary
    Peer Priority, ID, and Role: 2000 44:38:39:00:00:3e secondary
          Peer Interface and IP: peerlink.4094 fe80::4638:39ff:fe00:3e (linklocal)
                      Backup IP: 192.168.0.12 (active)
                     System MAC: 44:38:39:ff:00:01

CLAG Interfaces
Our Interface      Peer Interface     CLAG Id   Conflicts              Proto-Down Reason
----------------   ----------------   -------   --------------------   -----------------
SERVER01           SERVER01           1​         -                      -
SERVER02           SERVER02           2 ​        -                      -
```

```
cumulus@leaf02:~$​ ​net show clag 
The peer is alive
     Our Priority, ID, and Role: 2000 44:38:39:00:00:3e secondary
    Peer Priority, ID, and Role: 1000 44:38:39:00:00:3d primary
          Peer Interface and IP: peerlink.4094 fe80::4638:39ff:fe00:3d (linklocal)
                      Backup IP: 192.168.0.11 (active)
                     System MAC: 44:38:39:ff:00:01
CLAG Interfaces
Our Interface      Peer Interface     CLAG Id   Conflicts              Proto-Down Reason
----------------   ----------------   -------   --------------------   -----------------
SERVER01           SERVER01           1​         -                      - ​ 
SERVER02           SERVER02           2 ​        -                      -
```

19. **On leaf01:**  
    Check verbose CLAG configuration on leaf01 to verify MAC address learning.

```
cumulus@leaf01:~$ ​net show clag 
The peer is alive
     Our Priority, ID, and Role: 1000 44:38:39:00:00:3d primary
    Peer Priority, ID, and Role: 2000 44:38:39:00:00:3e secondary
          Peer Interface and IP: peerlink.4094 fe80::4638:39ff:fe00:3e (linklocal)
                      Backup IP: 192.168.0.12 (active)
                     System MAC: 44:38:39:ff:00:01

CLAG Interfaces
Our Interface      Peer Interface     CLAG Id   Conflicts              Proto-Down Reason
----------------   ----------------   -------   --------------------   -----------------
SERVER01           SERVER01           1​         -                      -
SERVER02           SERVER02           2 ​        -                      -

Our LACP Information
Our Interface      Partner MAC         CIST PortId   CLAG Id   Oper St   Flags
----------------   -----------------   -----------   -------   -------   -----
SERVER01           00:03:00:11:11:02​   None          1         None      None
SERVER02           00:03:00:22:22:02   None          2         None      None

Peer LACP Information
Peer Interface     Partner MAC         CIST PortId   CLAG Id   Oper St   Flags
----------------   -----------------   -----------   -------   -------   -----
SERVER01           00:03:00:11:11:02​   None          1         None      None
SERVER02           00:03:00:22:22:02   None          2         None      None

Backup info:
IP: 192.168.0.12; State: active; Role: primary
Peer priority and id: 2000 44:38:39:00:00:3e; Peer role: secondary

Our Interface      Dynamic MAC         VLAN Id
----------------   -----------------   -------
SERVER01           02:03:00:11:11:01   10
SERVER01           00:03:00:11:11:02   10
SERVER02           00:03:00:22:22:02   10
SERVER02           02:03:00:22:22:01   10

Peer Interface     Dynamic MAC         VLAN Id
----------------   -----------------   -------
SERVER02           02:03:00:22:22:02   10
SERVER01           02:03:00:11:11:02   10

Interface          MAC Address         VLAN Id   State
----------------   -----------------   -------   ---------

IP Address                  Our Interface      Dynamic MAC         VLAN Id   Owner
-------------------------   ----------------   -----------------   -------   ----------

Our Multicast Group      Port               VLAN Id   Device             Age
----------------------   ----------------   -------   ----------------   ---

Peer Multicast Group     Port               VLAN Id   Device             Age
----------------------   ----------------   -------   ----------------   ---

Destination                 Our Interface      Dynamic MAC         VLAN Id   Ownership
-------------------------   ----------------   -----------------   -------   ----------

Our Router Port    Device             Age
----------------   ----------------   ---

Peer Router Port   Device             Age
----------------   ----------------   ---

Socket             State
----------------   ----------------
socketToPeer       Connected
socketFromPeer     Connected
serverSocket       Listening

Database           md5 hash
----------------   --------------------------------
neighDB            d41d8cd98f00b204e9800998ecf8427e

Timer Information
Timers             Value     Time remaining
----------------   -------   --------------
sendTimeout        30        -
peerTimeout        20        00:00:20
initDelay          10        00:00:00
reloadTimer        300       00:00:00

Our VLAN Information
Our Interface      VLAN Id
----------------   -------
SERVER01           10
SERVER02           10

Peer VLAN Information
Peer Interface     VLAN Id
----------------   -------
SERVER01           10
SERVER02           10

```

>Important things to observe:
>-   The MAC address for server01 should match between the SERVER01 bonds between leaf01 and leaf02. 
>-   The VLAN configuration for each host interface should match for each of the CLAG peers. 
>


**This concludes Lab 4a.**



<!-- AIR:page -->
### Lab 4b: MLAG + Routing High Availability

**Objective:**

This lab will build on the leaf01 and leaf02 as CLAG peers by configuring a switched virtual interface (SVI) and virtual router redundancy (VRR) on the pair of switches.  
This will allow server01 and server02 to have redundant gateways for their infrastructure.  
This lab will build on the configuration of Lab4a.

**This final configuration will resemble this topology:**

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image3.png)

**Dependencies on other Labs:**
-   Lab 4a is assumed to be complete before the start of this lab.

**Goals:**
-   Configure SVIs on leaf01 and leaf02
-   Configure VRR addresses on leaf01 and leaf02 

**Procedure:**

#### Configure SVI and VRR on leaf01 and leaf02

**VRR Configuration details**

| Setting↓   \\   Switch→ | leaf01 | leaf02 |
|-------------------------|:------:|:------:|
| VLAN10 real IP address | 10.0.10.2/24 | 10.0.10.3/24 |
| VLAN10 VRR IP address | 10.0.10.1/24 | 10.0.10.1/24 |
| VLAN10 VRR MAC address | 00:00:00:00:1a:10 | 00:00:00:00:1a:10 |
| VLAN20 real IP address | 10.0.20.2/24 | 10.0.20.3/24 |
| VLAN20 VRR IP address | 10.0.20.1/24 | 10.0.20.1/24 |
| VLAN20 VRR MAC address | 00:00:00:00:1a:20 | 00:00:00:00:1a:20 |
| SERVER01 bond vlan | 10 | 10 |
| SERVER02 bond vlan | 20 | 20 |


1.  **On leaf01:**  
    Create an SVI for vlan10.

```
cumulus@leaf01:~$ net add vlan 10 ip address 10.0.10.2/24
```

2.  **On leaf01:**  
    Create an SVI for vlan 20. 

```
cumulus@leaf01:~$ net add vlan 20 ip address 10.0.20.2/24
```

3.  **On leaf01:**  
    Apply a VRR address for vlan10. 

```
cumulus@leaf01:~$ net add vlan 10 ip address-virtual 00:00:00:00:1a:10 10.0.10.1/24
```

4.  **On leaf01:**  
    Apply a VRR address for vlan20. 

```
cumulus@leaf01:~$ net add vlan 20 ip address-virtual 00:00:00:00:1a:20 10.0.20.1/24
```

5.  On leaf01: Make the SERVER02 bond an access port for vlan 20. 

```
cumulus@leaf01:~$ net add bond SERVER02 bridge access 20
```

6.  **On leaf01:**  
    Commit the changes. 

```
cumulus@leaf01:~$ net commit
```

7.  **On leaf02:**  
    Repeat steps 1-6. 

```
cumulus@leaf02:~$ net add vlan 10 ip address 10.0.10.3/24
cumulus@leaf02:~$ net add vlan 20 ip address 10.0.20.3/24
cumulus@leaf02:~$ net add vlan 10 ip address-virtual 00:00:00:00:1a:10 10.0.10.1/24
cumulus@leaf02:~$ net add vlan 20 ip address-virtual 00:00:00:00:1a:20 10.0.20.1/24
cumulus@leaf02:~$ net add bond SERVER02 bridge access 20
cumulus@leaf02:~$ net commit
```

>Note: The section below is provided for easier copying and pasting.

**leaf01**

```
net add vlan 10 ip address 10.0.10.2/24
net add vlan 20 ip address 10.0.20.2/24
net add vlan 10 ip address-virtual 00:00:00:00:1a:10 10.0.10.1/24
net add vlan 20 ip address-virtual 00:00:00:00:1a:20 10.0.20.1/24
net add bond SERVER02 bridge access 20
net commit
```

**leaf02**

```
net add vlan 10 ip address 10.0.10.3/24
net add vlan 20 ip address 10.0.20.3/24
net add vlan 10 ip address-virtual 00:00:00:00:1a:10 10.0.10.1/24
net add vlan 20 ip address-virtual 00:00:00:00:1a:20 10.0.20.1/24
net add bond SERVER02 bridge access 20
net commit
```

#### Configure server02 to use new VLAN and SVI

Server Bond Configuration

| Configuration↓   \\   Switch→ | leaf01 | leaf02 |
|-------------------------------|:------:|:------:|
| Uplink bond IP address | 10.0.10.101/24 | 10.0.20.102/24 |

8.  **On oob-mgmt-server:**  
    Run the server configuration script to configure interfaces on the Ubuntu servers. 

```
cumulus@oob-mgmt-server:~$ ​config server lab4b
INFO: Changing into /home/cumulus/BootcampAutomation...


 ### Configuring Servers for Lab4b...
 
 
TASK [Gathering Facts] ************************************************************
TASK [Copy Interfaces Configuration File] *****************************************
TASK [Restart Networking] *********************************************************


 ​### Server Configuration Successful! ### 


cumulus@oob-mgmt-server:~$
```

9.  **On server02:**  
    Verify the interfaces configuration file.
    In this step the ip address is being changed from an IP in vlan 10 to an IP address appropriate for vlan 20.

>Note: The post-up route has changed as well to reflect the gateway IP address of the vlan20 segment.

```
cumulus@server02:~$ ​ifquery uplink
auto uplink
iface uplink
        address ​10.0.20.102/24 
        bond-mode 802.3ad
        bond-miimon 100 
        bond-lacp-rate 1 
        bond-min-links 1 
        bond-xmit-hash-policy layer3+4 
        bond-slaves eth1 eth2
        post-up ip route add 10.0.0.0/8 dev uplink via 10.0.20.1
        post-up ip link set uplink promisc on

cumulus@server02:~$ ​ip addr show uplink
12: uplink: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 00:03:00:22:22:02 brd ff:ff:ff:ff:ff:ff 
    inet ​10.0.20.102/24​ scope global uplink
       valid_lft forever preferred_lft forever
    inet6 fe80::203:ff:fe22:2202/64 scope link
       valid_lft forever preferred_lft forever
cumulus@server02:~$
```

#### Verify Connectivity to VRR address

10. **On server01:**  
    Test connectivity from server01 to the VRR gateway address.

```
cumulus@server01:~$ ​ping 10.0.10.1
PING 10.0.10.1 (10.0.10.1) 56(84) bytes of data.
64 bytes from 10.0.10.1: icmp_seq=1 ttl=64 time=0.686 ms
64 bytes from 10.0.10.1: icmp_seq=2 ttl=64 time=0.922 ms
^C
--- 10.0.10.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 0.686/0.804/0.922/0.118 ms
```

11. **On server01:**  
    Test connectivity from server01 to leaf01 real IP address. 

```
cumulus@server01:~$ ​ping 10.0.10.2
PING 10.0.10.2 (10.0.10.2) 56(84) bytes of data.
64 bytes from 10.0.10.2: icmp_seq=1 ttl=64 time=0.887 ms
64 bytes from 10.0.10.2: icmp_seq=2 ttl=64 time=0.835 ms
^C
--- 10.0.10.2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 0.835/0.861/0.887/0.026 ms
```

12. **On server01:**  
    Test connectivity from server01 to leaf02 real IP address.

```
cumulus@server01:~$ ​ping 10.0.10.3
PING 10.0.10.3 (10.0.10.3) 56(84) bytes of data.
64 bytes from 10.0.10.3: icmp_seq=1 ttl=64 time=0.528 ms
64 bytes from 10.0.10.3: icmp_seq=2 ttl=64 time=0.876 ms
^C
--- 10.0.10.3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 0.528/0.702/0.876/0.174 ms
```

13. **On server01:**  
    Check the IP neighbor table which is similar to the ARP table, to view each MAC address.  
    The arp table could also be evaluated using the "arp" command.

```
cumulus@server01:~$ ​ip neighbor show
192.168.0.254 dev eth0 lladdr 44:38:39:00:00:20 REACHABLE
10.0.10.1 dev uplink lladdr 00:00:00:00:1a:10 STALE
10.0.10.3 dev uplink lladdr 44:38:39:00:00:3e REACHABLE
10.0.10.2 dev uplink lladdr 44:38:39:00:00:3d REACHABLE 
fe80::4638:39ff:fe00:3e dev uplink lladdr 44:38:39:00:00:3e router STALE 
fe80::4638:39ff:fe00:3d dev uplink lladdr 44:38:39:00:00:3d router STALE 
fe80::a200:ff:fe00:61 dev eth0 lladdr a0:00:00:00:00:61 router STALE
```

14. **On server02:**  
    Repeat the same connectivity tests in step 10-13 from server02 to switch IP addresses. 

```
cumulus@server02:~$ ​ping 10.0.20.1
PING 10.0.20.1 (10.0.20.1) 56(84) bytes of data.
64 bytes from 10.0.20.1: icmp_seq=1 ttl=64 time=1.22 ms
64 bytes from 10.0.20.1: icmp_seq=2 ttl=64 time=0.672 ms
^C
--- 10.0.20.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 0.672/0.949/1.226/0.277 ms
```

```
cumulus@server02:~$ ​ping 10.0.20.2
PING 10.0.20.2 (10.0.20.2) 56(84) bytes of data.
64 bytes from 10.0.20.2: icmp_seq=1 ttl=64 time=0.735 ms
64 bytes from 10.0.20.2: icmp_seq=2 ttl=64 time=1.02 ms
^C
--- 10.0.20.2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 0.735/0.882/1.029/0.147 ms
```

```
cumulus@server02:~$ ​ping 10.0.20.3
PING 10.0.20.3 (10.0.20.3) 56(84) bytes of data.
64 bytes from 10.0.20.3: icmp_seq=1 ttl=64 time=0.993 ms
64 bytes from 10.0.20.3: icmp_seq=2 ttl=64 time=1.08 ms
^C
--- 10.0.20.3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1002ms 
rtt min/avg/max/mdev = 0.993/1.040/1.087/0.047 ms
```

```
cumulus@server02:~$ ​ip neighbor show
10.0.20.3 dev uplink lladdr 44:38:39:00:00:3e REACHABLE
10.0.20.1 dev uplink lladdr 00:00:00:00:1a:20 REACHABLE
192.168.0.254 dev eth0 lladdr 44:38:39:00:00:20 DELAY
10.0.20.2 dev uplink lladdr 44:38:39:00:00:3d REACHABLE 
fe80::a200:ff:fe00:61 dev eth0 lladdr a0:00:00:00:00:61 router STALE 
fe80::4638:39ff:fe00:3d dev uplink lladdr 44:38:39:00:00:3d router STALE 
fe80::4638:39ff:fe00:3e dev uplink lladdr 44:38:39:00:00:3e router STALE
```

**Important things to observe:**
-   Pings to the VRR and real IP addresses should complete (both).
-   Both peer switches need to have the same vlan configuration for ports which face a dual-connected host.



20. **On server01 and server02:**  
    Verify connectivity between server01 and server02.

```
cumulus@server01:~$ ​ping 10.0.20.102
PING 10.0.20.102 (10.0.20.102) 56(84) bytes of data.
64 bytes from 10.0.20.102: icmp_seq=1 ttl=63 time=0.790 ms
64 bytes from 10.0.20.102: icmp_seq=2 ttl=63 time=1.35 ms
^C
--- 10.0.20.102 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms 
rtt min/avg/max/mdev = 0.790/1.070/1.351/0.282 ms
```

```
cumulus@server02:~$​ ​ping 10.0.10.101
PING 10.0.10.101 (10.0.10.101) 56(84) bytes of data.
64 bytes from 10.0.10.101: icmp_seq=1 ttl=63 time=1.08 ms
64 bytes from 10.0.10.101: icmp_seq=2 ttl=63 time=1.36 ms
^C
--- 10.0.10.101 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss​, time 1001ms rtt min/avg/max/mdev = 1.089/1.225/1.361/0.136 ms
```


**This concludes Lab 4b.**



<!-- AIR:page -->
### Lab 5a: OSPF

**Objective:**

This lab will configure OSPF unnumbered between the leaf01/leaf02 to spine01.  
This configuration will share the ip addresses of the loopback interfaces on each device.

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image1.png)

**Dependencies on other Labs:**
-   This lab will build from a default configuration

**Goals:**
-   Revert to default configurations
-   Configure OSPF unnumbered on spine01
-   Configure OSPF unnumbered on leaf01/leaf02
-   Advertise loopback addresses into OSPF
-   Verify OSPF neighbors
-   Verify OSPF route advertisements

**Procedure:**

#### Remove all previous configuration

**On leaf01 and leaf02:**  
Use NCLU to reset to default configurations.

```
cumulus@leaf01:~$ ​net del all
*************************************************************************** 
*************************************************************************** 
NOTE: 'net del all' will stage a default configuration file for each of the 
packages that are supported by NCLU. This default configuration may be 
different from the configuration that you would get should you remove and 
re-install each of these packages. Please review the staged changes via 
"net pending". 
*************************************************************************** 
*************************************************************************** 
cumulus@leaf01:~$​ ​net commit
```

```
cumulus@leaf02:~$ ​net del all
*************************************************************************** 
*************************************************************************** 
NOTE: 'net del all' will stage a default configuration file for each of the 
packages that are supported by NCLU. This default configuration may be 
different from the configuration that you would get should you remove and 
re-install each of these packages. Please review the staged changes via 
"net pending". 
*************************************************************************** 
*************************************************************************** 
cumulus@leaf02:~$​ ​net commit
```

#### Apply loopback address to leaf01, leaf02 and spine01

**Loopback Configuration**

| Configuration↓   \\   Switch→ | leaf01 | leaf02 | spine01 |
|-------------------------------|:------:|:------:|:-------:|
| Loopback IP address | 10.255.255.1/32 | 10.255.255.2/32 | 10.255.255.101/32 |


>NOTE: The `net del all` above removed the loopback configuration from leaf01 and leaf02.

1.  **On leaf01:**  
    Configure the loopback interface. 

```
cumulus@leaf01:~$ net add loopback lo ip address 10.255.255.1/32
cumulus@leaf01:~$ net commit
```

2.  **On leaf02:** 
    Configure the loopback.

```
cumulus@leaf02:~$ net add loopback lo ip address 10.255.255.2/32
cumulus@leaf02:~$ net commit
```

3.  **On spine01:**  
    Configure the loopback. 

```
cumulus@spine01:~$net add loopback lo ip address 10.255.255.101/32
cumulus@spine01:~$net commit
```

#### Configure OSPF unnumbered on spine01, leaf01 and leaf02

4.  **On spine01:**  
    Configure the loopback IP address on the interfaces for OSPF unnumbered and set the router-id.

```
cumulus@spine01:~$ net add interface swp1-2 ip address 10.255.255.101/32
cumulus@spine01:~$ net add ospf router-id 10.255.255.101
```

5.  **On spine01:**  
    Assign the interfaces to OSPF Area 0 and set the network type to point-to-point. 

```
cumulus@spine01:~$ ​net add interface swp1-2 ospf area 0 
cumulus@spine01:~$ ​net add interface swp1-2 ospf network point-to-point
```

6.  **On spine01:**  
    Commit the changes.

```
cumulus@spine01:~$ ​net commit
```

7.  **On leaf01:**  
    Configure the loopback IP address on the interfaces for OSPF unnumbered and set the router-id.

```
cumulus@leaf01:~$ ​net add interface swp51 ip address 10.255.255.1/32 
cumulus@leaf01:~$ ​net add ospf router-id 10.255.255.1
```

8.  **On leaf01:**  
    Assign the interfaces to OSPF Area 0 and set the network type to point-to-point.

```
cumulus@leaf01:~$ ​net add interface swp51 ospf area 0 
cumulus@leaf01:~$ ​net add interface swp51 ospf network point-to-point 
cumulus@leaf01:~$ ​net commit
```

9.  **On leaf02:**  
    Configure the loopback IP address on the interfaces for OSPF unnumbered and set the router-id.

```
cumulus@leaf02:~$ ​net add interface swp51 ip address 10.255.255.2/32 
cumulus@leaf02:~$ ​net add ospf router-id 10.255.255.2
```

10. **On leaf02:**  
    Assign the interfaces to OSPF Area 0 and set the network type to point-to-point.

```
cumulus@leaf02:~$ ​net add interface swp51 ospf area 0 
cumulus@leaf02:~$ ​net add interface swp51 ospf network point-to-point 
cumulus@leaf02:~$ ​net commit
```

#### Verify OSPF connectivity between fabric nodes

11. **On spine01:**  
    Verify OSPF neighbors between spine and leafs.

```
cumulus@spine01:~$ ​net show ospf neighbor
Neighbor ID     Pri State           Dead Time Address         Interface             RXmtL RqstL DBsmL
10.255.255.1      1 Full/DROther      39.347s 10.255.255.1    swp1:10.255.255.101       0     0     0
10.255.255.2      1 Full/DROther      38.391s 10.255.255.2    swp2:10.255.255.101       1     0     0
```

12. **On spine01:**  
    Show OSPF interface details.

```
cumulus@spine01:~$ ​net show ospf interface 
swp1 is up
  ifindex 4, MTU 1500 bytes, BW 1000 Mbit <UP,BROADCAST,RUNNING,MULTICAST> 
  This interface is ​UNNUMBERED​, Area 0.0.0.0
  MTU mismatch detection: enabled
  Router ID 10.255.255.101, Network Type POINTOPOINT, Cost: 100
  Transmit Delay is 1 sec, State Point-To-Point, Priority 1
  No backup designated router on this network
  Multicast group memberships: OSPFAllRouters
  Timer intervals configured, Hello 10s, Dead 40s, Wait 40s, Retransmit 5
    Hello due in 1.429s
  Neighbor Count is 1, Adjacent neighbor count is 1
swp2 is up
  ifindex 5, MTU 1500 bytes, BW 1000 Mbit <UP,BROADCAST,RUNNING,MULTICAST> 
  This interface is ​UNNUMBERED​, Area 0.0.0.0
  MTU mismatch detection: enabled
  Router ID 10.255.255.101, Network Type POINTOPOINT, Cost: 100
  Transmit Delay is 1 sec, State Point-To-Point, Priority 1
  No backup designated router on this network
  Multicast group memberships: OSPFAllRouters
  Timer intervals configured, Hello 10s, Dead 40s, Wait 40s, Retransmit 5
    Hello due in 1.430s
  Neighbor Count is 1, Adjacent neighbor count is 1
```

13. **On leaf01:**  
    Verify OSPF neighbors between spine and leafs.

```
cumulus@leaf01:~$ net show ospf neighbor
Neighbor ID     Pri State           Dead Time Address         Interface             RXmtL RqstL DBsmL 
10.255.255.101    1 Full/DROther      34.961s 10.255.255.101  swp51:10.255.255.1        0     0     0
```

>Important things to observe:
>-   OSPF neighbors established in Full/DROther
>-   OSPF interface details show "UNNUMBERED"
>-   The OSPF neighbor identifier uses the loopback address

#### Advertise Loopbacks from leaf01, leaf02 and spine01 into fabric

**Routing Advertisement Configuration**

| Routes↓   \\   Switch→ | leaf01 | leaf02 | spine01 |
|------------------------|:------:|:------:|:-------:|
| Subnets to be advertised | 10.255.255.1/32 | 10.255.255.2/32 | 10.255.255.101/32

14. **On spine01:**  
    Advertise loopback lo address into OSPF by enabling OSPF on the loopback interface.

```
cumulus@spine01:~$ net add loopback lo ospf area 0 
cumulus@spine01:~$ net commit
```

15. **On leaf01:**  
    Advertise loopback lo address into OSPF by enabling OSPF on the loopback interface.

```
cumulus@leaf01:~$ net add loopback lo ospf area 0 
cumulus@leaf01:~$ net commit
```

16. **On leaf02:**  
    Advertise loopback lo address into OSPF by enabling OSPF on the loopback interface.

```
cumulus@leaf02:~$ ​net add loopback lo ospf area 0 
cumulus@leaf02:~$ ​net commit
```

#### Verify that OSPF is advertising the loopback routes

17. **On spine01:**  
    Check that routes are being learned.

```
cumulus@spine01:~$ ​net show route ospf
RIB entry for ospf
==================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route
O>* 10.255.255.1/32 [110/100] via 10.255.255.1, swp1 onlink, 00:00:17
O>* 10.255.255.2/32 [110/100] via 10.255.255.2, swp2 onlink, 00:00:10
O   10.255.255.101/32 [110/0] is directly connected, lo, 00:00:27
```


**This concludes Lab 5a.**


<!-- AIR:page -->
### Lab 5b: iBGP deployment

**Objective:**
This lab will configure iBGP between the leaf01/leaf02 to spine01.

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image7.png)

**Dependencies on other Labs:**
-   Lab 5a is assumed to be complete before the start of this lab.

**Goals:**
-   Configure iBGP route-reflector on spine01
-   Configure iBGP on leaf01/leaf02
-   Verify BGP peering
-   Verify BGP route advertisements

**Procedure:**

#### Configure iBGP on spine01, leaf01 and leaf02

1.  **On spine01:**  
    Configure a BGP Autonomous System (AS) number. 

>Note: the router ID will automatically be assigned from the loopback lo interfaces. 

```
cumulus@spine01:~$ net add bgp autonomous-system 65100
```

2.  On spine01: Configure iBGP peering to the loopbacks on leaf01 and leaf02. 

```
cumulus@spine01:~$ net add bgp neighbor 10.255.255.1 remote-as internal 
cumulus@spine01:~$ net add bgp neighbor 10.255.255.1 update-source lo 
cumulus@spine01:~$ net add bgp neighbor 10.255.255.2 remote-as internal 
cumulus@spine01:~$ net add bgp neighbor 10.255.255.2 update-source lo
```

3.  **On spine01:**  
    Commit the changes.
    
```
cumulus@spine01:~$ net commit
```

4.  **On leaf01:**  
    Configure a BGP Autonomous System (AS) number.

>Note the router ID will automatically be assigned from the loopback lo interfaces. 

```
cumulus@leaf01:~$ net add bgp autonomous-system 65100
cumulus@leaf01:~$ net add bgp neighbor 10.255.255.101 remote-as internal
cumulus@leaf01:~$ net add bgp neighbor 10.255.255.101 update-source lo
cumulus@leaf01:~$ net commit
```

5.  On leaf02: Configure a BGP Autonomous System (AS) number.  

>Note the router ID will automatically be assigned from the loopback lo interfaces.

```
cumulus@leaf02:~$ net add bgp autonomous-system 65100
cumulus@leaf02:~$ net add bgp neighbor 10.255.255.101 remote-as internal
cumulus@leaf02:~$ net add bgp neighbor 10.255.255.101 update-source lo
cumulus@leaf02:~$ net commit
```

#### Verify BGP connectivity between fabric nodes

6.  **On spine01:**  
    Verify BGP peering between spine and leafs. 

```
cumulus@spine01:~$ net show bgp summary
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.255.255.101, local AS number 65100 vrf-id 0 
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 2, using 39 KiB of memory

Neighbor             V           AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(10.255.255.1)​ 4        65100      15      15        0    0    0 00:00:37            0
leaf02(10.255.255.2)​ 4        65100      12      12        0    0    0 00:00:27            0

Total number of neighbors 2


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
% No BGP neighbors found
```

7.  **On leaf01/leaf02:**  
    Verify BGP peering between leafs and spine

```
cumulus@leaf01:~$ net show bgp summary
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.255.255.1, local AS number 65100 vrf-id 0 
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 1, using 19 KiB of memory

Neighbor                V        AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd 
spine01(10.255.255.101)​ 4     65100      33      33        0    0    0 ​00:01:33​            0

Total number of neighbors 1


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
% No BGP neighbors found
```

```
cumulus@leaf02:~$ ​net show bgp summary
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.255.255.2, local AS number 65100 vrf-id 0 
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 1, using 19 KiB of memory

Neighbor                V        AS MsgRcvd MsgSent   Tblver  InQ OutQ  Up/Down State/PfxRcd
spine01(10.255.255.101)​ 4     65100      41      41        0    0    0 00:01:57            0

Total number of neighbors 1


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found

show bgp l2vpn evpn summary
===========================
% No BGP neighbors found
```

>Important things to observe:
>-   The BGP neighbor shows the hostname of the BGP peer and loopback IP address
>-   Only the peer is up, no routes are being advertised yet
>-   The BGP router identifier uses the loopback address


**This concludes Lab 5b.**


<!-- AIR:page -->
### Lab 6: EVPN

**Objective:**

This lab will configure EVPN for layer 2 extension between leaf01 and leaf02.

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image11.png)

**Dependencies on other Labs:**
-   Lab 5a and 5b are assumed to be complete before the start of this lab.

**Goals:**
-   Configure EVPN address family support on spine01 and leaf01/leaf02
-   Configure L2 VNIs for VLAN 10 and 20 on leaf01/leaf02
-   Verify EVPN route advertisement
-   Verify layer2 reachability between server01/server02

**Procedure:**

#### Configure EVPN on spine01, leaf01, and leaf02

1.  **On spine01:**  
    Configure the l2vpn evpn address family and activate the leaf neighbors.

```
cumulus@spine01:~$ net add bgp l2vpn evpn neighbor 10.255.255.1 activate 
cumulus@spine01:~$ net add bgp l2vpn evpn neighbor 10.255.255.2 activate 
cumulus@spine01:~$ net add bgp l2vpn evpn neighbor 10.255.255.1 route-reflector client 
cumulus@spine01:~$ net add bgp l2vpn evpn neighbor 10.255.255.2 route-reflector-client 
cumulus@spine01:~$ net commit
```

2.  **On leaf01:**  
    Configure the l2vpn evpn address family and activate the spine neighbor.

```
cumulus@leaf01:~$ net add bgp l2vpn evpn neighbor 10.255.255.101 activate 
cumulus@leaf01:~$ net commit
```

3.  **On leaf02:**  
    Configure the l2vpn evpn address family and activate the spine neighbor.

```
cumulus@leaf02:~$ ​net add bgp l2vpn evpn neighbor 10.255.255.101 activate 
cumulus@leaf02:~$ ​net commit
```

#### Verify BGP connectivity between fabric nodes

4.  **On spine01:**  
    Verify BGP neighbors have the l2vpn evpn capability correctly exchanged.   
    
>Note: the `net show bgp summary` command will display the summary for all the supported address families.

```
cumulus@spine01:~$ net show bgp summary
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.255.255.101, local AS number 65100 vrf-id 0
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 2, using 39 KiB of memory

Neighbor             V          AS MsgRcvd MsgSent    TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(10.255.255.1) 4       65100      91      91         0    0    0 00:00:13            0
leaf02(10.255.255.2) 4       65100      87      87         0    0    0 00:00:05            0

Total number of neighbors 2


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
BGP router identifier 10.255.255.101, local AS number 65100 vrf-id 0
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 2, using 39 KiB of memory

Neighbor             V          AS MsgRcvd MsgSent    TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(10.255.255.1) 4       65100      91      91         0    0    0 00:00:13            0
leaf02(10.255.255.2) 4       65100      87      87         0    0    0 00:00:05            0

Total number of neighbors 2
```

5.  **On leaf01/leaf02:**  
    Verify BGP peering between leafs and spine

```
cumulus@leaf01:~$ net show bgp summary
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.255.255.1, local AS number 65100 vrf-id 0 BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 1, using 19 KiB of memory

Neighbor                V        AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
spine01(10.255.255.101) 4     65100     109     115        0    0    0 00:01:09            0

Total number of neighbors 1


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
BGP router identifier 10.255.255.1, local AS number 65100 vrf-id 0
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 1, using 19 KiB of memory

Neighbor                V       AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd 
spine01(10.255.255.101)​ 4    65100     110     116        0    0    0 ​00:01:09​            0

Total number of neighbors 1
```

```
cumulus@leaf02:~$ net show bgp summary
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.255.255.2, local AS number 65100 vrf-id 0 
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 1, using 19 KiB of memory

Neighbor                V       AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
spine01(10.255.255.101)​ 4    65100     118     124        0    0    0 ​00:01:37​            0

Total number of neighbors 1


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
BGP router identifier 10.255.255.2, local AS number 65100 vrf-id 0
BGP table version 0
RIB entries 0, using 0 bytes of memory
Peers 1, using 19 KiB of memory

Neighbor                V       AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
spine01(10.255.255.101)​ 4    65100     118     124        0    0    0 ​00:01:37​            0

Total number of neighbors 1
```

>Important things to observe:
>-   The neighbors should be listed and up under ipv4 unicast as well as l2vpn evpn
>-   Only the peer is up, no routes are being advertised yet
>-   The BGP router identifier uses the loopback address

#### Configure SVI and VRR on leaf01 and leaf02

**VRR Configuration details**

| Setting↓   \\   Switch→ | leaf01 | leaf02 |
|-------------------------|:------:|:------:|
| VLAN10 real IP address | 10.0.10.2/24 | 10.0.10.3/24 |
| VLAN10 VRR IP address | 10.0.10.1/24 | 10.0.10.1/24 |
| VLAN10 VRR MAC address | 00:00:5e:00:01:10 | 00:00:5e:00:01:10 |
| VLAN20 real IP address | 10.0.20.2/24 | 10.0.20.3/24 |
| VLAN20 VRR IP address | 10.0.20.1/24 | 10.0.20.1/24 |
| VLAN20 VRR MAC address | 00:00:5e:00:01:20 | 00:00:5e:00:01:20 |
| SERVER01 bond vlan | 10 | 10 |
| SERVER02 bond vlan | 20 | 20 |

6.  **On leaf01:**  
    Create an SVI for vlan10.

```
cumulus@leaf01:~$ ​net add vlan 10 ip address 10.0.10.2/24
```

7.  **On leaf01:**  
    Create an SVI for vlan 20.

```
cumulus@leaf01:~$ ​net add vlan 20 ip address 10.0.20.2/24
```

8.  **On leaf01:**  
    Apply a VRR address for vlan10.

```
cumulus@leaf01:~$ ​net add vlan 10 ip address-virtual 00:00:5e:00:01:10 10.0.10.1/24
```

9.  **On leaf01:**  
    Apply a VRR address for vlan20.

```
cumulus@leaf01:~$ ​net add vlan 20 ip address-virtual 00:00:5e:00:01:20 10.0.20.1/24
```

10. **On leaf01:**  
    Make the server01 interface swp1 a trunk port with vlans 10 and 20.

```
cumulus@leaf01:~$ ​net add interface swp1 bridge trunk vlans 10,20
```

11. **On leaf01:**  
    Commit the changes.

```
cumulus@leaf01:~$ ​net commit
```

12. **On leaf02:**  
    Repeat steps 6-11. 

```
cumulus@leaf02:~$ ​net add vlan 10 ip address 10.0.10.3/24
cumulus@leaf02:~$ ​net add vlan 20 ip address 10.0.20.3/24
cumulus@leaf02:~$ ​net add vlan 10 ip address-virtual 00:00:5e:00:01:10 10.0.10.1/24 
cumulus@leaf02:~$ ​net add vlan 20 ip address-virtual 00:00:5e:00:01:20 10.0.20.1/24 
cumulus@leaf02:~$ ​net add interface swp2 bridge trunk vlans 10,20 
cumulus@leaf02:~$ ​net commit
```

>Note: The section below is provided for easier copying and pasting.

**leaf01**
```
net add vlan 10 ip address 10.0.10.2/24
net add vlan 20 ip address 10.0.20.2/24
net add vlan 10 ip address-virtual 00:00:5e:00:01:10 10.0.10.1/24
net add vlan 20 ip address-virtual 00:00:5e:00:01:20 10.0.20.1/24
net add interface swp1 bridge trunk vlans 10,20
net commit
```

**leaf02**
```
net add vlan 10 ip address 10.0.10.3/24
net add vlan 20 ip address 10.0.20.3/24
net add vlan 10 ip address-virtual 00:00:5e:00:01:10 10.0.10.1/24
net add vlan 20 ip address-virtual 00:00:5e:00:01:20 10.0.20.1/24
net add interface swp2 bridge trunk vlans 10,20
net commit
```

#### Create VLAN to VNI mappings

**VLAN to VNI mapping**

[VLAN↓   \\  VNI→ ]{.c8}

[leaf01]{.c8}

[leaf02]{.c8}

[spine01]{.c8}

[VLAN interface and associated VNI]{.c11}

[VLAN 10 / VNI 10]{.c11}

[VLAN 20 / VNI 20]{.c11}

[VLAN 10 / VNI 10]{.c11}

[VLAN 20 / VNI 20]{.c11}

[N/A]{.c11}

[]{.c69 .c54}

13. [On leaf01]{.c6}[: Configure the VXLAN local tunnel IP under the
    loopback.]{.c49 .c157}

[]{#t.3b089980b40bc8f346a5bf112e817c1fa4670dd8}[]{#t.159}

  -----------------------------------------------------------------------------------------------------
  [cumulus\@leaf01:\~\$]{.c7}[ net add loopback lo vxlan local-tunnelip 10.255.255.1]{.c57 .c61 .c51}
  -----------------------------------------------------------------------------------------------------

[]{.c49 .c157}

14. [On leaf01]{.c6}[: Create VNI 10 and map VLAN 10 to VNI 10.  Disable
    bridge learning on vxlan interfaces as EVPN will manage the MAC
    learning.]{.c15}

[]{#t.821cf9292f4f6becc4244f1d6208ae535b460e96}[]{#t.160}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add vxlan vni10 vxlan id 10]{.c17}   |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add vxlan vni10 bridge access        |
| 10]{.c17}                                                             |
|                                                                       |
| [cumulus\@leaf01:\~\$]{.c7}[ net add vxlan vni10 bridge learning      |
| off]{.c17}                                                            |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

15. [On leaf01]{.c6}[: Create VNI 20 and map VLAN 20 to VNI 20.  Disable
    bridge learning on vxlan interfaces as EVPN will manage the MAC
    learning. ]{.c15}

[]{#t.afe48e7b1e8c2980cd2f94f76557c6641bf86c47}[]{#t.161}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add vxlan vni20 vxlan id 20]{.c17}   |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add vxlan vni20 bridge access        |
| 20]{.c17}                                                             |
|                                                                       |
| [cumulus\@leaf01:\~\$]{.c7}[ net add vxlan vni20 bridge learning      |
| off]{.c57 .c61 .c51}                                                  |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

16. [On leaf01]{.c6}[: Commit the changes. ]{.c15}

[]{#t.1ece2b3ebea614be9e6b582505a5a6cabe3dd681}[]{#t.162}

  ------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c17}
  ------------------------------------------------

[]{.c15}

17. [On leaf02]{.c57 .c47}[: Configure the VXLAN local tunnel IP under
    the loopback.]{.c49 .c157}

[]{#t.7eb38cb9310885fb780543b504f41bd57ae96b25}[]{#t.163}

  -----------------------------------------------------------------------------------------------------
  [cumulus\@leaf02:\~\$]{.c7}[ net add loopback lo vxlan local-tunnelip 10.255.255.2]{.c57 .c61 .c51}
  -----------------------------------------------------------------------------------------------------

[]{.c49 .c47}

18. [On leaf02:]{.c57 .c47}[ Create VNI 10 and map VLAN 10 to VNI 10.
     Disable bridge learning on vxlan interfaces as EVPN will manage the
    MAC learning.]{.c15}

[]{#t.57f32550aa69e19d4b3482941851d08efe08ff66}[]{#t.164}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add vxlan vni10 vxlan id 10]{.c17}   |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net add vxlan vni10 bridge access        |
| 10]{.c17}                                                             |
|                                                                       |
| [cumulus\@leaf02:\~\$]{.c7}[ net add vxlan vni10 bridge learning      |
| off]{.c57 .c61 .c51}                                                  |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

19. [On leaf02:]{.c57 .c47}[ Create VNI 20 and map VLAN 20 to VNI 20.
     Disable bridge learning on vxlan interfaces as EVPN will manage the
    MAC learning.]{.c15}

[]{#t.b192a32373f5c0b2cceca92e08a3f9529091eeea}[]{#t.165}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add vxlan vni20 vxlan id 20]{.c17}   |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net add vxlan vni20 bridge access        |
| 20]{.c17}                                                             |
|                                                                       |
| [cumulus\@leaf02:\~\$]{.c7}[ net add vxlan vni20 bridge learning      |
| off]{.c57 .c61 .c51}                                                  |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

20. [On leaf02:]{.c57 .c47}[ Commit the changes]{.c15}

[]{#t.19245a90cc63d18141c55fb2eb02844f5d730259}[]{#t.166}

  ------------------------------------------------
  [cumulus\@leaf02:\~\$ ]{.c7}[net commit]{.c17}
  ------------------------------------------------

[]{.c15}

[Advertise the VNIs and hosts on VLAN 10 and VLAN 20]{.c4} {#h.94h17lcyhols .c23}
----------------------------------------------------------

21. [On leaf01:]{.c6}[ Advertise all VNIs]{.c15}

[]{#t.7731533c325551c55ef39afcacdfcb2579241821}[]{#t.167}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp l2vpn evpn                   |
| advertise-all-vni]{.c17}                                              |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}              |
+-----------------------------------------------------------------------+

[]{.c49 .c58}

22. [On leaf01:]{.c6}[ Verify the EVPN VNI configuration]{.c15}

[]{#t.7b20dc5c5fdeb996190932861aebc2910fef599f}[]{#t.168}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net show evpn vni]{.c17}                 |
|                                                                       |
| [VNI        Type VxLAN IF              \# MACs   \# ARPs   \# Remote  |
| VTEPs  Tenant VRF                           ]{.c1}                    |
|                                                                       |
| [20         L2   vni20                 1        2        0            |
|     default                              ]{.c1}                       |
|                                                                       |
| [10         L2   vni10                 1        2        0            |
|     default]{.c1}                                                     |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[]{.c49 .c47}

23. [On leaf02:]{.c57 .c47}[ Advertise all VNIs]{.c15}

[]{#t.72765d9ae31668317806352ecb16e1f7fe4df9f0}[]{#t.169}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp l2vpn evpn                   |
| advertise-all-vni]{.c17}                                              |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}              |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

24. [On leaf02:]{.c57 .c47}[ Verify the EVPN VNI configuration]{.c15}

[]{#t.e5f19e6a56741da14f3c366953b9cac1dfea1205}[]{#t.170}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net show evpn vni]{.c17}                 |
|                                                                       |
| [VNI        Type VxLAN IF              \# MACs   \# ARPs   \# Remote  |
| VTEPs  Tenant VRF                           ]{.c1}                    |
|                                                                       |
| [20         L2   vni20                 1        2        1            |
|     default                              ]{.c1}                       |
|                                                                       |
| [10         L2   vni10                 1        2        1            |
|     default]{.c1}                                                     |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[]{.c15}

[]{.c4} {#h.y9peozqeg5ti .c23 .c137}
-------

[Configure server01 and server02 trunk links]{.c4} {#h.t7v5z3y3b3el .c23}
--------------------------------------------------

[]{#t.78fcbf8509522435924653e3f6bac28f39ac90a1}[]{#t.171}

[Server Bond Configuration]{.c8}

[Configuration↓   \\   Switch→ ]{.c8}

[leaf01]{.c8}

[leaf02]{.c8}

[VLAN 10 ip/mac]{.c11}

[10.0.10.101/24]{.c11}

[00:03:00:11:11:01]{.c11}

[10.0.10.102/24]{.c11}

[00:03:00:22:22:02]{.c11}

[VLAN 20 ip/mac]{.c11}

[10.0.20.101/24]{.c11}

[00:03:00:11:11:01]{.c11}

[10.0.20.102/24]{.c11}

[00:03:00:22:22:02]{.c11}

[]{.c69 .c54}

25. [On oob-mgmt-server: Run the server configuration script to
    configure interfaces on the Ubuntu servers. ]{.c15}

[]{#t.fa644813de9b0dcefa0cd60e0197260269f90db8}[]{#t.172}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[config server lab6]{.c17}       |
|                                                                       |
| [ INFO: Changing into /home/cumulus/BootcampAutomation\...]{.c1}      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ \#\#\# Configuring Servers for Lab6\... ]{.c1}                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [TASK \[Gathering Facts\]                                             |
| \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\             |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c1} |
|                                                                       |
| [TASK \[Copy Interfaces Configuration File\]                          |
| \*\*\*\*\*\*\*\*\*\                                                   |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c1} |
|                                                                       |
| [TASK \[Restart Networking\]                                          |
| \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\                   |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c1} |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ \#\#\# Server Configuration Successful! \#\#\#]{.c29 .c12}          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[]{.c15}

26. [On server01 and server02: Verify connectivity between server01 and
    server02.]{.c15}

[]{#t.1e97f360c9c08f7c2a640e204b53b7c68f53b4a8}[]{#t.173}

+-----------------------------------------------------------------------+
| [cumulus\@server01:\~\$ ]{.c7}[ping 10.0.10.102]{.c17}                |
|                                                                       |
| [PING 10.0.10.102 (10.0.10.102) 56(84) bytes of data.]{.c1}           |
|                                                                       |
| [64 bytes from 10.0.10.102: icmp\_seq=1 ttl=63 time=0.790 ms]{.c1}    |
|                                                                       |
| [64 bytes from 10.0.10.102: icmp\_seq=2 ttl=63 time=1.35 ms]{.c1}     |
|                                                                       |
| [\^C]{.c1}                                                            |
|                                                                       |
| [\-\-- 10.0.10.102 ping statistics \-\--]{.c1}                        |
|                                                                       |
| [2 packets transmitted, 2 received, 0% packet loss]{.c40 .c12}[, time |
| 1001ms]{.c1}                                                          |
|                                                                       |
| [rtt min/avg/max/mdev = 0.790/1.070/1.351/0.282 ms]{.c1}              |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.2978f578adea81b1cf5d6ffb35ae2746bb2f0d42}[]{#t.174}

+-----------------------------------------------------------------------+
| [cumulus\@server02:\~\$]{.c7}[ ]{.c40 .c51}[ping 10.0.10.101]{.c17}   |
|                                                                       |
| [PING 10.0.10.101 (10.0.10.101) 56(84) bytes of data.]{.c1}           |
|                                                                       |
| [64 bytes from 10.0.10.101: icmp\_seq=1 ttl=63 time=1.08 ms]{.c1}     |
|                                                                       |
| [64 bytes from 10.0.10.101: icmp\_seq=2 ttl=63 time=1.36 ms]{.c1}     |
|                                                                       |
| [\^C]{.c1}                                                            |
|                                                                       |
| [\-\-- 10.0.10.101 ping statistics \-\--]{.c1}                        |
|                                                                       |
| [2 packets transmitted, 2 received, 0% packet loss]{.c40 .c12}[, time |
| 1001ms]{.c1}                                                          |
|                                                                       |
| [rtt min/avg/max/mdev = 1.089/1.225/1.361/0.136 ms]{.c1}              |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[Verify that BGP is advertising the routes]{.c4} {#h.8kgv19dnzxpr .c23}
------------------------------------------------

27. [On spine01:]{.c42}[ Verify the BGP l2vpn evpn VNI type-3 routes for
    BUM are advertised]{.c15}

[]{#t.6bb506020e8108279495bee78e77491955547080}[]{#t.175}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp l2vpn evpn route type      |
| multicast]{.c17}                                                      |
|                                                                       |
| [BGP table version is 3, local router ID is 10.255.255.101]{.c1}      |
|                                                                       |
| [Status codes: s suppressed, d damped, h history, \* valid, \> best,  |
| i - internal]{.c1}                                                    |
|                                                                       |
| [Origin codes: i - IGP, e - EGP, ? - incomplete]{.c1}                 |
|                                                                       |
| [EVPN type-2 prefix:                                                  |
| \[2\]:\[ESI\]:\[EthTag\]:\[MAClen\]:\[MAC\]:\[IPlen\]:\[IP\]]{.c1}    |
|                                                                       |
| [EVPN type-3 prefix: \[3\]:\[EthTag\]:\[IPlen\]:\[OrigIP\]]{.c1}      |
|                                                                       |
| [EVPN type-5 prefix: \[5\]:\[ESI\]:\[EthTag\]:\[IPlen\]:\[IP\]]{.c1}  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [   Network          Next Hop            Metric LocPrf Weight         |
| Path]{.c1}                                                            |
|                                                                       |
| [                    Extended Community]{.c1}                         |
|                                                                       |
| [Route Distinguisher: 10.255.255.1:2]{.c1}                            |
|                                                                       |
| [\*\>i\[3\]:\[0\]:\[32\]:\[10.255.255.1\]]{.c1}                       |
|                                                                       |
| [                    10.255.255.1                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:20 ET:8]{.c1}                           |
|                                                                       |
| [Route Distinguisher: 10.255.255.1:3]{.c1}                            |
|                                                                       |
| [\*\>i\[3\]:\[0\]:\[32\]:\[10.255.255.1\]]{.c1}                       |
|                                                                       |
| [                    10.255.255.1                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:10 ET:8]{.c1}                           |
|                                                                       |
| [Route Distinguisher: 10.255.255.2:2]{.c1}                            |
|                                                                       |
| [\*\>i\[3\]:\[0\]:\[32\]:\[10.255.255.2\]]{.c1}                       |
|                                                                       |
| [                    10.255.255.2                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:20 ET:8]{.c1}                           |
|                                                                       |
| [Route Distinguisher: 10.255.255.2:3]{.c1}                            |
|                                                                       |
| [\*\>i\[3\]:\[0\]:\[32\]:\[10.255.255.2\]]{.c1}                       |
|                                                                       |
| [                    10.255.255.2                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:10 ET:8]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Displayed 4 prefixes (4 paths) (of requested type)]{.c1}             |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.4462cb74eaa0abc4dfeb544084427bec62e5339b}[]{#t.176}

[Important things to observe:]{.c15}

-   [The RD is auto-derived from the router-id and a local instance
    number]{.c15}
-   [The spine will have EVPN type 3 routes for the VTEP IP address on
    each leaf.  There should be 4 total 2 from each leaf 1 for each
    VNI]{.c15}

[]{.c69 .c54}

[]{.c69 .c54}

28. [On spine01:]{.c42}[ Verify the BGP l2vpn evpn VNI type-2 routes for
    host MACs are advertised]{.c15}

[]{#t.e32d952d40b748ed480f4e9ce6dd02b7146696bc}[]{#t.177}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp l2vpn evpn route type      |
| macip]{.c17}                                                          |
|                                                                       |
| [BGP table version is 3, local router ID is 10.255.255.101]{.c1}      |
|                                                                       |
| [Status codes: s suppressed, d damped, h history, \* valid, \> best,  |
| i - internal]{.c1}                                                    |
|                                                                       |
| [Origin codes: i - IGP, e - EGP, ? - incomplete]{.c1}                 |
|                                                                       |
| [EVPN type-2 prefix:                                                  |
| \[2\]:\[ESI\]:\[EthTag\]:\[MAClen\]:\[MAC\]:\[IPlen\]:\[IP\]]{.c1}    |
|                                                                       |
| [EVPN type-3 prefix: \[3\]:\[EthTag\]:\[IPlen\]:\[OrigIP\]]{.c1}      |
|                                                                       |
| [EVPN type-5 prefix: \[5\]:\[ESI\]:\[EthTag\]:\[IPlen\]:\[IP\]]{.c1}  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [   Network          Next Hop            Metric LocPrf Weight         |
| Path]{.c1}                                                            |
|                                                                       |
| [                    Extended Community]{.c1}                         |
|                                                                       |
| [Route Distinguisher: 10.255.255.1:2]{.c1}                            |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:\[48\]:\[00:03:00:11:11:01\]]{.c1}            |
|                                                                       |
| [                    10.255.255.1                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:20 ET:8]{.c1}                           |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:                                              |
| \[48\]:\[00:03:00:11:11:01\]:\[128\]:\[fe80::203:ff:fe11:1101\]]{.c1} |
|                                                                       |
| [                    10.255.255.1                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:20 ET:8]{.c1}                           |
|                                                                       |
| [Route Distinguisher: 10.255.255.1:3]{.c1}                            |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:\[48\]:\[00:03:00:11:11:01\]]{.c29 .c12}      |
|                                                                       |
| [                    10.255.255.1]{.c40 .c12}[                  100   |
|    0 i]{.c1}                                                          |
|                                                                       |
| [                    RT:65100:10 ET:8]{.c1}                           |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:                                              |
| \[48\]:\[00:03:00:11:11:01\]:\[128\]:\[fe80::203:ff:fe11:1101\]]{.c1} |
|                                                                       |
| [                    10.255.255.1                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:10 ET:8]{.c1}                           |
|                                                                       |
| [Route Distinguisher: 10.255.255.2:2]{.c1}                            |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:\[48\]:\[00:03:00:22:22:02\]]{.c1}            |
|                                                                       |
| [                    10.255.255.2                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:20 ET:8]{.c1}                           |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:                                              |
| \[48\]:\[00:03:00:22:22:02\]:\[128\]:\[fe80::203:ff:fe22:2202\]]{.c1} |
|                                                                       |
| [                    10.255.255.2                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:20 ET:8]{.c1}                           |
|                                                                       |
| [Route Distinguisher: 10.255.255.2:3]{.c1}                            |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:\[48\]:\[00:03:00:22:22:02\]]{.c29 .c12}      |
|                                                                       |
| [                    10.255.255.2]{.c40 .c12}[                  100   |
|    0 i]{.c1}                                                          |
|                                                                       |
| [                    RT:65100:10 ET:8]{.c1}                           |
|                                                                       |
| [\*\>i\[2\]:\[0\]:\[0\]:                                              |
| \[48\]:\[00:03:00:22:22:02\]:\[128\]:\[fe80::203:ff:fe22:2202\]]{.c1} |
|                                                                       |
| [                    10.255.255.2                  100      0 i]{.c1} |
|                                                                       |
| [                    RT:65100:10 ET:8]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Displayed 8 prefixes (8 paths) (of requested type)]{.c1}             |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.06e2e8b5a0d03ad17cc05df4d837558cb4fc05d8}[]{#t.178}

[Important things to observe:]{.c15}

-   [There should be a type-2 MAC entry for each host and vor each VNI.
     Above highlighted is the entry for VLAN 10 from each leaf]{.c15}
-   [There will also be type-2 IPv6/MAC and IP/MAC entries because we
    have a default gw on each leaf.  The GW is optional but is]{.c15}

configured for testing in the lab.

**This concludes Lab 6.**


<!-- AIR:page -->
### Lab 7: Automation with Ansible

**Objective:**

This module will teach students how to use Ansible with the NCLU module to deploy a network wide configuration.  
In this lab we will configure both halves of the topology: spine01, leaf01, leaf02, server01, server02 and the other half, spine02, leaf03, leaf04, server03, server04.

**The completed topology will look like this:**

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image2.png)

**Dependencies on other Labs:**
-   None

**Goals:**
-   Check that Zero Touch Provisioning (ZTP) completed successfully
-   Learn how to clone an Ansible configuration repo from a centralized Git server 
-   Use Ansible to deploy networking configuration

**Procedure:**

#### Check on ZTP in the Environment


1.  **On leaf01:**  
    Confirm that Zero Touch Provisioning (ZTP) completed successfully.

[]{#t.58bb0f563dcf4c0ffdefbc03127af0fddb18bb6d}[]{#t.179}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net show system ztp]{.c17}               |
|                                                                       |
| [ZTP INFO:]{.c1}                                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [State                            disabled                            |
| ]{.c1}                                                                |
|                                                                       |
| [Version                          1.0                                 |
|  ]{.c1}                                                               |
|                                                                       |
| [Result                           success]{.c40 .c58}[                |
|           ]{.c1}                                                      |
|                                                                       |
| [Date                             Tue Dec 17 14:06:16 2019 UTC        |
| ]{.c1}                                                                |
|                                                                       |
| [Method                           ZTP DHCP                            |
| ]{.c1}                                                                |
|                                                                       |
| [URL                                                                  |
|  http://192.168.0.254/ztp\_oob.sh]{.c1}                               |
+-----------------------------------------------------------------------+

[]{.c15}

[Note the URL of the ZTP script which was executed.  ]{.c15}

[]{.c15}

2.  [(Optional/Advanced)]{.c50}[ On leaf01]{.c6}[: Download the ZTP
    script and inspect the contents.]{.c50}

[]{#t.c3d3672bb2ad30e26bc718802d7e075a5d4de3dd}[]{#t.180}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[wget                                     |
| http://192.168.0.254/ztp\_oob.sh]{.c57 .c61 .c51}                     |
|                                                                       |
| [\--2017-10-03 20:10:41\--  http://192.168.0.254/ztp\_oob.sh]{.c1}    |
|                                                                       |
| [Connecting to 192.168.0.254:80\... connected.]{.c1}                  |
|                                                                       |
| [HTTP request sent, awaiting response\... 200 OK]{.c1}                |
|                                                                       |
| [Length: 2073 (2.0K) \[text/x-sh\]]{.c1}                              |
|                                                                       |
| [Saving to: 'ztp\_oob.sh']{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ztp\_oob.sh         100%\[=================\>\]   2.02K  \--.-KB/s   |
| in 0s     ]{.c1}                                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [2017-10-03 20:10:41 (124 MB/s) - 'ztp\_oob.sh' saved                 |
| \[2073/2073\]]{.c1}                                                   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[cat ztp\_oob.sh]{.c57 .c61 .c51}         |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

Setup the Automation Environment {#h.s2swt97am472 .c23}
--------------------------------

1.  [On oob-mgmt-server: Change directories to the Bootcamp Automation
    git repository. ]{.c15}

[]{#t.245d824f3470fca848e11ce681438ae19387fb5e}[]{#t.181}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[cd BootcampAutomation/]{.c17}   |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$]{.c1}               |
+-----------------------------------------------------------------------+

[]{.c15}

2.  [On oob-mgmt-server: Examine the contents of the playbook which is
    to be executed. ]{.c15}

[]{#t.ba168516accd1d5b58b43946f14bd352acd25703}[]{#t.182}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$ ]{.c7}[cat          |
| lab7.yml]{.c17}                                                       |
|                                                                       |
| [- name: Wipe Configuration]{.c1}                                     |
|                                                                       |
| [  hosts: spine,leaf]{.c1}                                            |
|                                                                       |
| [  become: yes]{.c1}                                                  |
|                                                                       |
| [  gather\_facts: no]{.c1}                                            |
|                                                                       |
| [  roles:]{.c1}                                                       |
|                                                                       |
| [  - reset]{.c1}                                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [- name: Lab 1 \-- Setup PTM]{.c1}                                    |
|                                                                       |
| [  hosts: spine,leaf]{.c1}                                            |
|                                                                       |
| [  become: yes]{.c1}                                                  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.10276994c5a1f8416cada25e9040dcfbf2c4e0d7}[]{#t.183}

[Important things to observe:]{.c15}

-   [Playbook uses NCLU module
    ]{.c50}[[https://docs.ansible.com/ansible/latest/nclu\_module.html](https://www.google.com/url?q=https://docs.ansible.com/ansible/latest/nclu_module.html&sa=D&ust=1578375996140000){.c18}]{.c56
    .c57 .c111}
-   [Single Ansible playbook deploys leaf, spine and server
    configuration]{.c15}

[]{.c15}

Run the Ansible playbook {#h.35mhyqtkm0tn .c23 .c22}
------------------------

3.  [On oob-mgmt-server: Run the Ansible playbook. ]{.c50}[This playbook
    will wipe the configurations back to a starting point and then apply
    all the configuration up to this point for leaf01, leaf02, server01,
    server02 and spine01. The automation will also configure several
    previously untouched devices: leaf03, leaf04, spine02, server03,
    server04 to complete the configuration for the entire fabric.
    ]{.c75}

[]{#t.e38d830b8e2848e8893447456741abe28a581421}[]{#t.184}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$                     |
| ]{.c7}[ansible-playbook lab7.yml]{.c89 .c102 .c57 .c61}               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\...]{.c1}                                                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PLAY RECAP                                                           |
| \*\*\*                                                                |
| \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\ |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c1} |
|                                                                       |
| [leaf01                     : ok=12   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf02                     : ok=12   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf03                     : ok=12   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf04                     : ok=12   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server01                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server02                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server03                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server04                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [spine01                    : ok=12   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [spine02                    : ok=12   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Tuesday 17 December 2019  17:41:36 +0000 (0:00:07.134)               |
| 0:02:00.397 \*\*\*\*\*\*]{.c1}                                        |
|                                                                       |
| [================                                                     |
| ===============================================================]{.c1} |
|                                                                       |
| [reset : Clear config                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 32.65s]{.c1}                                                          |
|                                                                       |
| [Lab 7 \-- Deploy Configuration To All Leafs                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--             |
| 16.09s]{.c1}                                                          |
|                                                                       |
| [Lab 7 \-- Deploy Configuration to All Spines                         |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 12.62s]{.c1}  |
|                                                                       |
| [reset : Wait 5 seconds for port 22 to become open and contain        |
| \"OpenSSH\" \-- 11.78s]{.c1}                                          |
|                                                                       |
| [reset : Stop FRR                                                     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 10.55s]{.c1}                                                          |
|                                                                       |
| [reset : Restore NTP                                                  |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                  |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 8.17s]{.c1}                                                           |
|                                                                       |
| [Restart Networking                                                   |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 7.14s]{.c1}                                                           |
|                                                                       |
| [reset : Apply Default Interface Configuration                        |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 3.54s]{.c1}     |
|                                                                       |
| [reset : Restart PTM Daemon to Apply New PTM Config                   |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 3.30s]{.c1}               |
|                                                                       |
| [reset : Copy the Default Interface Configuration in Place            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-- 2.56s]{.c1}                             |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 2.48s]{.c1}                                                           |
|                                                                       |
| [Restart PTM Daemon to Apply new Topology.dot file                    |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 2.45s]{.c1}             |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.63s]{.c1}                                                           |
|                                                                       |
| [Download the topology.dot file from the OOB-MGMT-SERVER              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 1.52s]{.c1}                         |
|                                                                       |
| [reset : Remove PTM file                                              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-                                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.43s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.25s]{.c1}                                                           |
|                                                                       |
| [Copy Interfaces Configuration File                                   |
| \-\-\-                                                                |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.19s]{.c1}                                                           |
+-----------------------------------------------------------------------+

[]{.c15}

4.  [(Optional)]{.c50}[ On oob-mgmt-server: Use the other playbooks in
    the BootcampAutomation directory to move around to put the fabric in
    the same state as it was at the completion of various
    labs.]{.c50}[ ]{.c15}

[]{#t.6cf247dfb09d3fd7958d39bb87b0c46db2894210}[]{#t.185}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$                     |
| ]{.c7}[ansible-playbook lab2.yml]{.c89 .c102 .c57 .c61}               |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$                     |
| ]{.c7}[ansible-playbook lab5a.yml]{.c89 .c57 .c61 .c102}              |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
+-----------------------------------------------------------------------+


Note: If you have performed any of the options in step 4 above,  
make sure to run `ansible-playbook lab7.yml` over again before moving on to the next lab.



>Important things to observe:
>-   Each play has three possible outputs: changed, unreachable, failed
>-   All plays executed successfully
>-   Time for each task completed


**This concludes Lab 7.**


<!-- AIR:page -->
### Lab 8: Troubleshooting via Traditional Methods

**Objective:**
This lab will walk through how to validate and troubleshoot various components on an individual switch level.

**Dependencies on other Labs:**
-   Lab 7 is assumed to be complete before the start of this lab.

**Goals:**
-   Verify PTM is correct on all nodes
-   Check, clear, and unclear counters
-   View Environmental Sensor Information
-   Generate and inspect a cl-support
-   Test monitoring tools by failing a sensor in VX

**Procedure:**

#### Setup Prescriptive Topology Manager

**On leaf01:** 
Download a pre-built topology file which represents all the links in the topology.  
Install that file in the PTM directory and restart the PTM service.

```
| [cumulus\@leaf01:\~\$ ]{.c7}[wget                                     |
| http://192.168.0.254/topology.dot]{.c17}                              |
|                                                                       |
| [\--2017-10-03 20:46:38\--  http://192.168.0.254/topology.dot]{.c1}   |
|                                                                       |
| [Connecting to 192.168.0.254:80\... connected.]{.c1}                  |
|                                                                       |
| [HTTP request sent, awaiting response\... 200 OK]{.c1}                |
|                                                                       |
| [Length: 2638 (2.6K) \[application/msword\]]{.c1}                     |
|                                                                       |
| [Saving to: 'topology.dot']{.c1}                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [topology.dot       100%\[=================\>\]   2.58K  \--.-KB/s    |
| in 0s     ]{.c1}                                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [2017-10-03 20:46:38 (47.7 MB/s) - 'topology.dot' saved               |
| \[2638/2638\]]{.c1}                                                   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo]{.c57 .c61 .c51}[ cp ]{.c57 .c61    |
| .c51}[topology.dot /etc/ptm.d/topology.dot]{.c17}                     |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo systemctl restart                   |
| ptmd.service]{.c57 .c61 .c51}                                         |
```

**On leaf01:**  
Look at ptmctl output to confirm links are setup correctly.  
At this point all ports have been enabled on leaf01.  
These ports show to be correctly connected.

>Note: When using Cumulus In The Cloud (CitC), the output will be different from what is shown below as LLDP is not possible in CitC.

```
| [cumulus\@leaf01:\~]{.c7}[\$]{.c7}[ ]{.c40 .c51}[ptmctl]{.c57 .c61    |
| .c51}                                                                 |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [port   cbl     BFD     BFD   BFD    BFD   ]{.c1}                     |
|                                                                       |
| [       status  status  peer  local  type  ]{.c1}                     |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [swp46  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp48  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp51  pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp49  pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp50  pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp52  pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [eth0   pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp1   pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp2   pass    N/A     N/A   N/A    N/A]{.c7}[ ]{.c1}                |
```

>Note: This output will change as we move through the labs and configure more interfaces.

**On leaf01:**  
Evaluate the expected and actual LLDP neighbors.
By changing the arguments to the ptmctl command we can see more information about who PTM was expecting to see for an LLDP neighbor versus which neighbor is actually connected.

Note: When using Cumulus In The Cloud (CitC), the output will be different from what is shown below as LLDP is not possible in CitC.

```
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo ptmctl -dl]{.c17}                   |
|                                                                       |
| [\-\-\-\-\-\-\-\-                                                     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\ |
| -\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\- |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [port   cbl     exp                   act                   sysname   |
|        portID  portDescr  match   last]{.c1}                          |
|                                                                       |
| [       status  nbr                   nbr                             |
|                           on      upd]{.c1}                           |
|                                                                       |
| [\-\-\-\-\-\-\-\-                                                     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\ |
| -\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\- |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [eth0   pass    oob-mgmt-switch:swp6  oob-mgmt-switch:swp6            |
|  oob-mgmt-switch  swp6    swp6       IfName  45s]{.c1}                |
|                                                                       |
| [swp1   pass    server01:eth1         server01:eth1         server01  |
|         eth1    eth1       IfName  45s]{.c1}                          |
|                                                                       |
| [swp2   pass    server02:eth1         server02:eth1         server02  |
|         eth1    eth1       IfName  45s]{.c1}                          |
|                                                                       |
| [swp49  pass    leaf02:swp49          leaf02:swp49          leaf02    |
|         swp49   swp49      IfName  45s]{.c1}                          |
|                                                                       |
| [swp50  pass    leaf02:swp50          leaf02:swp50          leaf02    |
|         swp50   swp50      IfName  45s]{.c1}                          |
|                                                                       |
| [swp51  pass    spine01:swp1          spine01:swp1          spine01   |
|        swp1    swp1       IfName  45s]{.c1}                           |
|                                                                       |
| [swp52  pass    spine02:swp1          spine02:swp1          spine02   |
|        swp1    swp1       IfName  45s]{.c7}                           |
```

Note: This output will change as we move through the labs and configure more interfaces.

#### Check PTM status

1.  **On oob-mgmt-server:**  
    Use Ansible to check the state of PTM across the network.  
    At this point everything in the environment should show in the 'pass' state which means the entire environment is cabled correctly.

```
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[cd BootcampAutomation]{.c17}    |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ansible -a \'ptmctl -l\'        |
| spine,leaf]{.c17}                                                     |
|                                                                       |
| [leaf01 \| SUCCESS \| rc=0 \>\>]{.c29 .c12}                           |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c29 .c12}                            |
|                                                                       |
| [port   cbl     ]{.c29 .c12}                                          |
|                                                                       |
| [       status  ]{.c29 .c12}                                          |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c29 .c12}                            |
|                                                                       |
| [eth0   pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp1   pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp51  pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp2   pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp52  pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp50  pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp49  pass    ]{.c29 .c12}                                          |
|                                                                       |
| []{.c29 .c12}                                                         |
|                                                                       |
| [leaf03 \| SUCCESS \| rc=0 \>\>]{.c29 .c12}                           |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c29 .c12}                            |
|                                                                       |
| [port   cbl     ]{.c29 .c12}                                          |
|                                                                       |
| [       status  ]{.c29 .c12}                                          |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c29 .c12}                            |
|                                                                       |
| [eth0   pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp1   pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp2   pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp51  pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp52  pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp50  pass    ]{.c29 .c12}                                          |
|                                                                       |
| [swp49  pass    ]{.c29 .c12}                                          |
|                                                                       |
| []{.c29 .c12}                                                         |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
```

#### Check and Clear Counters 

2.  **On spine01:**  
    Check some of the counters 

```
| [cumulus\@spine01:\~\$ ]{.c7}[net show counters]{.c17}                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Kernel Interface table]{.c1}                                         |
|                                                                       |
| [Iface      MTU    Met    RX\_OK    RX\_ERR    RX\_DRP    RX\_OVR     |
|  TX\_OK    TX\_ERR    TX\_DRP    TX\_OVR  Flg]{.c1}                   |
|                                                                       |
| [\-\-\-\-\-\--  \-\-\-\--  \-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--  |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--     |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\--]{.c1}                    |
|                                                                       |
| [eth0      1500      0    12120         0         1         0         |
| 3639         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [lo       65536      0        0         0         0         0         |
|  0         0         0         0  LRU]{.c1}                           |
|                                                                       |
| [swp1      1500      0     2214         0         2         0         |
| 2144         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [swp2      1500      0     2021         0         2         0         |
| 2219         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [swp3      1500      0      781         0         2         0         |
|  942         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [swp4      1500      0     1000         0         2         0         |
| 1028         0         0         0  BMRU]{.c1}                        |
```

3.  **On spine01:**  
    Watch the counters iterate in real time.  
    The `watch` command will run any command over and over again every 2 seconds.  
    This combination of `watch` with the `net show counters` command is very useful  
    to watch traffic moving through the box in real time and for watching interface  
    or drop counters iterate during testing

```
| [cumulus\@spine01:\~\$ ]{.c7}[watch -d net show counters]{.c57 .c61   |
| .c51}                                                                 |
|                                                                       |
| [Every 2.0s: net show counters                                        |
|               Wed Oct 11 19:57:44 2017]{.c1}                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Kernel Interface table]{.c1}                                         |
|                                                                       |
| [Iface      MTU    Met    RX\_OK    RX\_ERR    RX\_DRP    RX\_OVR     |
|  TX\_OK    TX\_ERR    TX\_DRP    TX\_OVR  Flg]{.c1}                   |
|                                                                       |
| [\-\-\-\-\-\--  \-\-\-\--  \-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--  |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--     |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\--]{.c1}                    |
|                                                                       |
| [eth0      1500      0   1102]{.c7}[93]{.c62 .c40}[         0         |
| 1         0    6965]{.c7}[5]{.c62 .c40}[         0         0          |
| 0  BMRU]{.c1}                                                         |
|                                                                       |
| [lo       65536      0    15494         0         0         0         |
|  15494         0         0         0  LRU]{.c1}                       |
|                                                                       |
| [swp1      1500      0    7820]{.c7}[1]{.c62 .c40}[         0         |
| 5         0    81337         0         0         0  BMRU]{.c1}        |
|                                                                       |
| [swp2      1500      0    79734         0         5         0         |
|  832]{.c7}[26]{.c62 .c40}[         0         0         0  BMRU]{.c1}  |
|                                                                       |
| [swp3      1500      0     333]{.c7}[8]{.c62 .c40}[         0         |
| 3         0     4371         0         0         0  BMRU]{.c1}        |
|                                                                       |
| [swp4      1500      0     369]{.c7}[3]{.c40 .c62}[         0         |
| 3         0     4252         0         0         0  BMRU]{.c1}        |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c45 .c7}                                                          |
```

Note: Use `<CTRL> + c` to exit this screen and return to the CLI. 

4.  **On spine01:**  
    Clear the counters.  
    Clearing counters is a common technique to troubleshoot a problem as it is occuring.
    Creating a common reference point at the beginning of a troubleshooting session to  
    understand what has changed since the start of the session. 

```
| [cumulus\@spine01:\~\$ ]{.c7}[net clear counters ]{.c57 .c61 .c51}    |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net show counters]{.c17}                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Kernel Interface table]{.c1}                                         |
|                                                                       |
| [Iface      MTU    Met    RX\_OK    RX\_ERR    RX\_DRP    RX\_OVR     |
|  TX\_OK    TX\_ERR    TX\_DRP    TX\_OVR  Flg]{.c1}                   |
|                                                                       |
| [\-\-\-\-\-\--  \-\-\-\--  \-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--  |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--     |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\--]{.c1}                    |
|                                                                       |
| [eth0      1500      0       38         0         0         0         |
| 19         0         0         0  BMRU]{.c1}                          |
|                                                                       |
| [lo       65536      0        0         0         0         0         |
|  0         0         0         0  LRU]{.c1}                           |
|                                                                       |
| [swp1      1500      0        2         0         0         0         |
|  3         0         0         0  BMRU]{.c1}                          |
|                                                                       |
| [swp2      1500      0        0         0         0         0         |
|  1         0         0         0  BMRU]{.c1}                          |
|                                                                       |
| [swp3      1500      0        0         0         0         0         |
|  1         0         0         0  BMRU]{.c1}                          |
|                                                                       |
| [swp4      1500      0        0         0         0         0         |
|  1         0         0         0  BMRU ]{.c1}                         |
```

5.  **On spine01:**  
    Unclear the counters.  
    One of the interesting benefits of Cumulus Linux that some other vendors do not  
    have is the ability to "unclear" the counters back to the default state.  
    This is possible because Cumulus wrote a special wrappered version of the  
    standard Linux `netstat` utility called `cl-netstat`.  
    When net show counters is called, NCLU is really calling `cl-netstat` in the background.  
    One of the driving factors behind the creation of the `cl-netstat` utility was the  
    need for network engineers to be able to "clear the counters" which was not  
    possible with standard Linux utilities.

```
| [cumulus\@spine01:\~\$ ]{.c7}[sudo cl-netstat -D]{.c57 .c61 .c51}     |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net show counters]{.c17}                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Kernel Interface table]{.c1}                                         |
|                                                                       |
| [Iface      MTU    Met    RX\_OK    RX\_ERR    RX\_DRP    RX\_OVR     |
|  TX\_OK    TX\_ERR    TX\_DRP    TX\_OVR  Flg]{.c1}                   |
|                                                                       |
| [\-\-\-\-\-\--  \-\-\-\--  \-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--  |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\-\-\--  \-\-\-\-\-\-\--     |
|  \-\-\-\-\-\-\--  \-\-\-\-\-\-\--  \-\-\-\--]{.c1}                    |
|                                                                       |
| [eth0      1500      0    12386         0         1         0         |
| 3772         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [lo       65536      0        0         0         0         0         |
|  0         0         0         0  LRU]{.c1}                           |
|                                                                       |
| [swp1      1500      0     2279         0         2         0         |
| 2210         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [swp2      1500      0     2087         0         2         0         |
| 2259         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [swp3      1500      0      829         0         2         0         |
| 1006         0         0         0  BMRU]{.c1}                        |
|                                                                       |
| [swp4      1500      0     1042         0         2         0         |
| 1091         0         0         0  BMRU ]{.c1}                       |
```

Note: The counter values have increased back to their normal values and should be higher than those seen in step 4. 


#### View Environmental Information

6.  **On spine01:**  
    Look at the high-level sensor output.  
    Vx devices like those used for these labs also have environmental sensor  
    output which can be observed and used to test your monitoring tools.

```
| [cumulus\@spine01:\~\$ ]{.c7}[smonctl]{.c17}                          |
|                                                                       |
| [Fan1      (Fan Tray 1, Fan 1                     ):  OK]{.c1}        |
|                                                                       |
| [Fan2      (Fan Tray 1, Fan 2                     ):  OK]{.c1}        |
|                                                                       |
| [Fan3      (Fan Tray 2, Fan 1                     ):  OK]{.c1}        |
|                                                                       |
| [Fan4      (Fan Tray 2, Fan 2                     ):  OK]{.c1}        |
|                                                                       |
| [Fan5      (Fan Tray 3, Fan 1                     ):  OK]{.c1}        |
|                                                                       |
| [Fan6      (Fan Tray 3, Fan 2                     ):  OK]{.c1}        |
|                                                                       |
| [PSU1                                              :  OK]{.c1}        |
|                                                                       |
| [PSU2                                              :  OK]{.c1}        |
|                                                                       |
| [PSU1Fan1  (PSU1 Fan                              ):  OK]{.c1}        |
|                                                                       |
| [PSU1Temp1 (PSU1 Temp Sensor                      ):  OK]{.c1}        |
|                                                                       |
| [PSU2Fan1  (PSU2 Fan                              ):  OK]{.c1}        |
|                                                                       |
| [PSU2Temp1 (PSU2 Temp Sensor                      ):  OK]{.c1}        |
|                                                                       |
| [Temp1     (Board Sensor near CPU                 ):  OK]{.c1}        |
|                                                                       |
| [Temp2     (Board Sensor Near Virtual Switch      ):  OK]{.c1}        |
|                                                                       |
| [Temp3     (Board Sensor at Front Left Corner     ):  OK]{.c1}        |
|                                                                       |
| [Temp4     (Board Sensor at Front Right Corner    ):  OK]{.c1}        |
|                                                                       |
| [Temp5     (Board Sensor near Fan                 ):  OK]{.c1}        |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c1}                                         |
```

7.  **On spine01:**  
    Look at the detailed sensor output.  
    You can see here the specifics of these sensors like the individual RPM  
    values for fan speeds as well as the specific thresholds for alerts for  
    each individual sensor. On a real piece of hardware the specific sensors  
    that are available will vary from platform to platform.

```
| [cumulus\@spine01:\~\$ ]{.c7}[smonctl -v]{.c17}                       |
|                                                                       |
| [Fan1(Fan Tray 1, Fan 1):  OK]{.c1}                                   |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Fan2(Fan Tray 1, Fan 2):  OK]{.c1}                                   |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Fan3(Fan Tray 2, Fan 1):  OK]{.c1}                                   |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Fan4(Fan Tray 2, Fan 2):  OK]{.c1}                                   |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Fan5(Fan Tray 3, Fan 1):  OK]{.c1}                                   |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Fan6(Fan Tray 3, Fan 2):  OK]{.c1}                                   |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PSU1:  OK]{.c1}                                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PSU2:  OK]{.c1}                                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PSU1Fan1(PSU1 Fan):  OK]{.c1}                                        |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PSU1Temp1(PSU1 Temp Sensor):  OK]{.c1}                               |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PSU2Fan1(PSU2 Fan):  OK]{.c1}                                        |
|                                                                       |
| [fan:2500 RPM   (max = 29000 RPM, min = 2500 RPM, limit\_variance =   |
| 15%)]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PSU2Temp1(PSU2 Temp Sensor):  OK]{.c1}                               |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Temp1(Board Sensor near CPU):  OK]{.c1}                              |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Temp2(]{.c7}[Board Sensor Near Virtual Switch]{.c40 .c12}[):         |
|  OK]{.c1}                                                             |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Temp3(Board Sensor at Front Left Corner):  OK]{.c1}                  |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Temp4(Board Sensor at Front Right Corner):  OK]{.c1}                 |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Temp5(Board Sensor near Fan):  OK]{.c1}                              |
|                                                                       |
| [temp:25.0 C (lcrit = 0 C, fan\_max = 80 C, fan\_min = 25 C, min = 5  |
| C, max = 80 C, crit = 85 C)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c1}                                         |
```

Note: The sense of humor possessed by our development engineers.  
On a real hardware switch this would typically be the temperature sensor next  
to the switching ASIC.

#### Fail a Sensor in Vx to Observe the Result

8.  **On spine01:**  
    Read the current sensor value.  
    The first command shows the sensor output for just the Temp2 sensor.  
    The second command shows the actual place where the value is read from  
    to populate the smonctl output, 25000 → 25.000 degrees celsius.

```
| [cumulus\@spine01:\~\$ ]{.c7}[smonctl -v -s Temp2]{.c17}              |
|                                                                       |
| [Temp2(Board Sensor Near Virtual Switch):  OK]{.c1}                   |
|                                                                       |
| [temp:]{.c7}[25.0 C]{.c40 .c12}[ (lcrit = 0 C, fan\_max = 80 C,       |
| fan\_min = 25 C, min = 5 C, max = 80 C, crit = 85 C)]{.c1}            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[cat                                     |
| /sys/class/hwmon/hwmon0/temp2\_input ]{.c17}                          |
|                                                                       |
| [25000]{.c29 .c12}                                                    |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c1}                                         |
```

9.  **On spine01:**  
    Write a new sensor value.  
    On Vx these artificial sensor values will never change due to the changing  
    environmental conditions like you would find in a real data center.  
    In the virtual platform these normally read-only values are writable too.  
    In this step we're changing the Temp2 sensor value to 80.0 degrees celsius.  
    The new values are read-in by the smonctl daemon every ~10 seconds.

```
| [cumulus\@spine01:\~\$ ]{.c7}[echo \"80000\" \| sudo tee              |
| /sys/class/hwmon/hwmon0/temp2\_input ]{.c17}                          |
|                                                                       |
| [80000]{.c29 .c12}                                                    |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[smonctl -v -s Temp2]{.c17}              |
|                                                                       |
| [Temp2(Board Sensor Near Virtual Switch):  OK]{.c1}                   |
|                                                                       |
| [temp:]{.c7}[25.0 C]{.c40 .c12}[ (lcrit = 0 C, fan\_max = 80 C,       |
| fan\_min = 25 C, min = 5 C, max = 80 C, crit = 85 C)]{.c1}            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[smonctl -v -s Temp2]{.c17}              |
|                                                                       |
| [Temp2(Board Sensor Near Virtual Switch):  OK]{.c1}                   |
|                                                                       |
| [temp:]{.c7}[80.0 C]{.c40 .c12}[ (lcrit = 0 C, fan\_max = 80 C,       |
| fan\_min = 25 C, min = 5 C, max = 80 C, crit = 85 C)]{.c1}            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c1}                                         |
```

10. **On spine01:**  
    Check the new fan speed.  
    Looking back at the output above from step 7 you can see the fan speeds  
    for all fans was 2500 RPMs. Every 30 seconds the `pwmd` program checks the  
    temperatures of the various sensors on the switch and if the temperatures  
    have gone up, pwmd will increase the fan speeds linearly to compensate.  
    In this case the fan speeds have increased from 2500 RPMs to 29000 RPMs.

```
| [cumulus\@spine01:\~\$ ]{.c7}[smonctl -v \| grep -A1 Fan]{.c17}       |
|                                                                       |
| [Fan1(Fan Tray 1, Fan 1):  OK]{.c1}                                   |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c12 .c40}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [Fan2(Fan Tray 1, Fan 2):  OK]{.c1}                                   |
|                                                                       |
| [fan:]{.c7}[29000 RPM ]{.c40 .c12}[  (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [Fan3(Fan Tray 2, Fan 1):  OK]{.c1}                                   |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c40 .c12}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [Fan4(Fan Tray 2, Fan 2):  OK]{.c1}                                   |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c40 .c12}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [Fan5(Fan Tray 3, Fan 1):  OK]{.c1}                                   |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c40 .c12}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [Fan6(Fan Tray 3, Fan 2):  OK]{.c1}                                   |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c40 .c12}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [PSU1Fan1(PSU1 Fan):  OK]{.c1}                                        |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c40 .c12}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c1}                                                            |
|                                                                       |
| [PSU2Fan1(PSU2 Fan):  OK]{.c1}                                        |
|                                                                       |
| [fan:]{.c7}[29000 RPM]{.c40 .c12}[   (max = 29000 RPM, min = 2500     |
| RPM, limit\_variance = 15%)]{.c1}                                     |
|                                                                       |
| [\--]{.c7}                                                            |
```

#### Generate and Inspect a cl-support file

11. **On spine01:**  
    See the options available to generate a cl-support.

```
| [cumulus\@spine01:\~\$ ]{.c7}[sudo cl-support -h]{.c17}               |
|                                                                       |
| [Usage: \[-h (help)\] \[-cDjlMsv\] \[-d m1,m2,\...\] \[-e             |
| m1,m2,\...\]]{.c1}                                                    |
|                                                                       |
| [  \[-p prefix\] \[-r reason\] \[-S dir\] \[-T Timeout\_seconds\]     |
| \[-t tag\]]{.c1}                                                      |
|                                                                       |
| [    -h: Display this help message]{.c1}                              |
|                                                                       |
| [    -c: Run only modules matching any core files, if no -e           |
| modules]{.c1}                                                         |
|                                                                       |
| [    -D: Display debugging information]{.c1}                          |
|                                                                       |
| [    -d: Disable (do not run) modules in this comma separated         |
| list]{.c1}                                                            |
|                                                                       |
| [    -e: Enable (only run) modules in this comma separated list; \"-e |
| all\" runs]{.c1}                                                      |
|                                                                       |
| [        all modules and sub-modules, including all optional          |
| modules]{.c1}                                                         |
|                                                                       |
| [    -j: Modules will produce json output files, where                |
| supported.]{.c1}                                                      |
|                                                                       |
| [    -l: List available modules, then exit]{.c1}                      |
|                                                                       |
| [    -M: Do not timeout modules, use with -T]{.c1}                    |
|                                                                       |
| [    -p: Use prefix as leading part of archive name]{.c1}             |
|                                                                       |
| [    -r: Reason for running cl-support (quoted string)]{.c1}          |
|                                                                       |
| [    -S: Use an alternate output directory (default                   |
| /var/support)]{.c1}                                                   |
|                                                                       |
| [    -s: Include security sensitive information such as sudoers       |
| file]{.c29 .c12}                                                      |
|                                                                       |
| [    -T: Timeout in seconds for creating support, 0 disables]{.c1}    |
|                                                                       |
| [    -t: tag string as part of archive name]{.c1}                     |
|                                                                       |
| [    -v: Verbose; display status messages]{.c1}                       |
```

12. **On spine01:**  
    Generate a cl-support with the default options. 

Note: Your file will have a different name based on the date and time it was generated.

```
| [cumulus\@spine01:\~\$]{.c7}[ ]{.c40 .c51}[sudo cl-support]{.c17}     |
|                                                                       |
| [Please send /var/support/cl\_support\_spine01\_20191217\_175320.txz  |
| to Cumulus support.]{.c1}                                             |
```

13. **On spine01:**  
    Untar the cl-support.

```
| [cumulus\@spine01:\~\$ ]{.c7}[tar -xvf                                |
| /var/support/cl\_support\_spine01\_20191217\_175320.txz]{.c17}        |
|                                                                       |
| [cl\_support\_spine01\_20191217\_175320/                              |
|                                                                       |
|                     ]{.c1}                                            |
|                                                                       |
| [cl\_support\_spine01\_20191217\_175320/Support/]{.c1}                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
```

14. **On spine01:***  
    Look at the Control file to the arguments cl-support was called with.  
    This can usually provide insight as to the cause for which the cl-support  
    was automatically generated for.

```
| [cumulus\@spine01:\~\$ ]{.c7}[cd                                      |
| cl\_support\_spine01\_20191217\_175320/]{.c17}                        |
|                                                                       |
| [cumulus\@spine01:\~/cl\_support\_spine01\_20171012\_164637\$         |
| ]{.c7}[cat Control]{.c17}                                             |
|                                                                       |
| [Command line: cl-support \'\' ]{.c29 .c12}                           |
|                                                                       |
| [cl-support Started collecting data at 2019-12-17T17:53:20.244283075  |
| UTC]{.c1}                                                             |
|                                                                       |
| [Support timeout=180]{.c1}                                            |
|                                                                       |
| [Modules to be run:]{.c1}                                             |
|                                                                       |
| [  dot1x:]{.c1}                                                       |
|                                                                       |
| [  frr:]{.c1}                                                         |
|                                                                       |
| [  vxfld:]{.c1}                                                       |
|                                                                       |
| [  system:]{.c1}                                                      |
|                                                                       |
| [  network:]{.c1}                                                     |
|                                                                       |
| [  lldp:]{.c1}                                                        |
|                                                                       |
| [  switchd:]{.c1}                                                     |
|                                                                       |
| [  clag:]{.c1}                                                        |
|                                                                       |
| [  nclu:]{.c1}                                                        |
|                                                                       |
| [  gdb:]{.c1}                                                         |
|                                                                       |
| [  openvswitch:]{.c1}                                                 |
|                                                                       |
| [  ptmd:]{.c1}                                                        |
|                                                                       |
| [cl-support Finished collecting data at 2019-12-17T17:53:37.840776763 |
| UTC]{.c1}                                                             |
```

15. **On spine01:**  
    Log out of the switch and log back in to observe the notification that a cl-support file is present. 

```
| [cumulus\@spine01:\~\$ ]{.c7}[exit]{.c17}                             |
|                                                                       |
| [logout]{.c1}                                                         |
|                                                                       |
| [Connection to spine01 closed.]{.c1}                                  |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ssh spine01]{.c17}              |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Welcome to Cumulus VX (TM)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Cumulus VX (TM) is a community supported virtual appliance designed  |
| for]{.c1}                                                             |
|                                                                       |
| [experiencing, testing and prototyping Cumulus Networks\' latest      |
| technology.]{.c1}                                                     |
|                                                                       |
| [For any questions or technical support, visit our community site     |
| at:]{.c1}                                                             |
|                                                                       |
| [http://community.cumulusnetworks.com]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [The registered trademark Linux (R) is used pursuant to a sublicense  |
| from LMI,]{.c1}                                                       |
|                                                                       |
| [the exclusive licensee of Linus Torvalds, owner of the mark on a     |
| world-wide]{.c1}                                                      |
|                                                                       |
| [basis.]{.c1}                                                         |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\  |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c29 |
| .c12}                                                                 |
|                                                                       |
| [Please send these support file(s) to                                 |
| support\@cumulusnetworks.com:]{.c29 .c12}                             |
|                                                                       |
| [ /var/support/cl\_support\_spine01\_20191217\_175320.txz]{.c29 .c12} |
|                                                                       |
| [\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\  |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c29 |
| .c12}                                                                 |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c1}                                         |
```

>Important things to observe:
>-   Whenever a Cl-support file exists on the box a notification will be shown in the Message of the Day (MOTD) upon login.
>-   Secure information is not transmitted in a cl-support
>-   Cl-support files will include the reason for creation
>-   Cl-support files gather the entire /etc and /var directories
>-   The cl-support process issues many commands and stores them in the `/Support` directory


**This concludes Lab 8.**

<!-- AIR:page -->
### Lab 9: Network Services

**Objective:**
This lab will configure several common network services including DNS, syslog and SNMP.

**Dependencies on other Labs:**
-   None

**Goals:**
-   Configure DNS
-   Configure Syslog (sending to remote destination)
-   Configure a Pre and Post login message
-   Enable SNMP

**Procedure:**

#### Configure a DNS Server

1.  [On spine01: Configure a DNS server.]{.c50}[ ]{.c89 .c21 .c54}

[]{#t.a025335966daafccb9fb4716adf5e2ca39a132c8}[]{#t.206}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add dns nameserver ipv4             |
| 192.168.0.254]{.c17}                                                  |
|                                                                       |
| [The IP is already present.]{.c7}                                     |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c17}                       |
+-----------------------------------------------------------------------+

[]{.c15}

[Note: This DNS server is already configured so NCLU will notify the IP
is already present. ]{.c89 .c21 .c54}

[]{.c89 .c21 .c54}

[Configure Remote Syslog Transmission]{.c4} {#h.3bnvxmki4uzn .c23 .c22}
-------------------------------------------

2.  [On spine01: Configure a remote syslog target that is the
    oob-mgmt-server. ]{.c15}

[]{#t.bc06ab1e1da10c9f3e3f2d2b75315dab3c4e4bce}[]{#t.207}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add syslog host ipv4 192.168.0.254  |
| port udp 514]{.c17}                                                   |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c17}                       |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

3.  [On oob-mgmt-server: Configure the syslog daemon to receive syslog
    messages from other machines. ]{.c50}[Uncomment the two lines in red
    below and save the file with ]{.c75}[\<CTRL\>+x]{.c40
    .c75}[.]{.c75}[ ]{.c15}

[]{#t.083d911ac21cd52a371b64a56c51f2ceae01277e}[]{#t.208}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo nano                       |
| /etc/rsyslog.conf]{.c57 .c61 .c51}                                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/rsyslog.conf                |
|       Modified]{.c37 .c89 .c106}                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\#  /etc/rsyslog.conf    Configuration file for rsyslog.]{.c1}       |
|                                                                       |
| [\#]{.c1}                                                             |
|                                                                       |
| [\#                       For more information see]{.c1}              |
|                                                                       |
| [\#                                                                   |
| /usr/share/doc/rsyslog-doc/html/rsyslog\_conf.\$]{.c1}                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c1}                             |
|                                                                       |
| [\#\#\#\# MODULES \#\#\#\#]{.c1}                                      |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c1}                             |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\$ModLoad imuxsock \# provides support for local system              |
| logging]{.c1}                                                         |
|                                                                       |
| [\$ModLoad imklog   \# provides kernel logging support]{.c1}          |
|                                                                       |
| [\#\$ModLoad immark  \# provides \--MARK\-- message capability]{.c1}  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\# provides UDP syslog reception]{.c1}                               |
|                                                                       |
| [\$ModLoad imudp]{.c29 .c90}                                          |
|                                                                       |
| [\$UDPServerRun 514]{.c29 .c90}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\# provides TCP syslog reception]{.c1}                               |
|                                                                       |
| [\#\$ModLoad imtcp]{.c1}                                              |
|                                                                       |
| [\#\$InputTCPServerRun 514]{.c1}                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [                               ]{.c7}[\[ Read 79 lines \]]{.c37 .c89 |
| .c106}                                                                |
|                                                                       |
| [\^G]{.c37}[ Get Help  ]{.c7}[\^O]{.c37}[ WriteOut                    |
|  ]{.c7}[\^R]{.c37}[ Read File ]{.c7}[\^Y]{.c37}[ Prev Page            |
| ]{.c7}[\^K]{.c37}[ Cut Text  ]{.c7}[\^C]{.c37}[ Cur Pos]{.c1}         |
|                                                                       |
| [\^X]{.c37}[ Exit      ]{.c7}[\^J]{.c37}[ Justify                     |
| ]{.c7}[\^W]{.c37}[ Where Is  ]{.c7}[\^V]{.c37}[ Next Page             |
| ]{.c7}[\^U]{.c37}[ UnCut Text]{.c7}[\^T]{.c37}[ To Spell]{.c1}        |
+-----------------------------------------------------------------------+

[]{.c15}

4.  [On oob-mgmt-server: Restart the syslog daemon. ]{.c15}

[]{#t.bd6fb695d112dc458a9fd44769dcb533ea3bf968}[]{#t.209}

  ------------------------------------------------------------------------------------------------
  [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo systemctl restart rsyslog.service ]{.c57 .c61 .c51}
  ------------------------------------------------------------------------------------------------

[]{.c15}

5.  [On spine01: Logout and login to generate a syslog message which
    will be sent to the oob-mgmt-server. ]{.c15}

[]{#t.671a9a4d5de4814e8b6a3a47eeb79cf40f198c1f}[]{#t.210}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[exit]{.c17}                             |
|                                                                       |
| [logout]{.c1}                                                         |
|                                                                       |
| [Connection to spine01 closed.]{.c1}                                  |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ssh spine01]{.c17}              |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Welcome to Cumulus VX (TM)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Cumulus VX (TM) is a community supported virtual appliance designed  |
| for]{.c1}                                                             |
|                                                                       |
| [experiencing, testing and prototyping Cumulus Networks\' latest      |
| technology.]{.c1}                                                     |
|                                                                       |
| [For any questions or technical support, visit our community site     |
| at:]{.c1}                                                             |
|                                                                       |
| [http://community.cumulusnetworks.com]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [The registered trademark Linux (R) is used pursuant to a sublicense  |
| from LMI,]{.c1}                                                       |
|                                                                       |
| [the exclusive licensee of Linus Torvalds, owner of the mark on a     |
| world-wide]{.c1}                                                      |
|                                                                       |
| [basis.]{.c1}                                                         |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c1}                                         |
|                                                                       |
| []{.c1}                                                               |
+-----------------------------------------------------------------------+

[]{.c15}

6.  [On oob-mgmt-server: View the syslog for messages from spine01.
    ]{.c15}

[]{#t.15a3b855a92fbba8d9d31265ec28b969f596a2a8}[]{#t.211}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo cat /var/log/syslog \|     |
| grep spine01]{.c17}                                                   |
|                                                                       |
| [2019-12-17T17:59:40+00:00 spine01 sshd\[24708\]: Received disconnect |
| from 192.168.0.254: 11: disconnected by user]{.c1}                    |
|                                                                       |
| [2019-12-17T17:59:40+00:00 spine01 sshd\[24690\]:                     |
| pam\_unix(sshd:session): session closed for user cumulus]{.c1}        |
|                                                                       |
| [2019-12-17T17:59:40+00:00 spine01 sshd\[28903\]: Accepted publickey  |
| for cumulus from 192.168.0.254 port 50398 ssh2: RSA                   |
| 38:e6:3b:cc:04:ac:41:5e:c9:e3:93:9d:cc:9e:48:25]{.c1}                 |
|                                                                       |
| [2019-12-17T17:59:41+00:00 spine01 sshd\[28903\]:                     |
| pam\_unix(sshd:session): session opened for user cumulus by           |
| (uid=0)]{.c1}                                                         |
|                                                                       |
| [2019-12-17T17:59:41+00:00 spine01 sshd\[28903\]: lastlog\_openseek:  |
| Couldn\'t stat /var/log/lastlog: No such file or directory]{.c1}      |
|                                                                       |
| [2019-12-17T17:59:41+00:00 spine01 sshd\[28903\]: lastlog\_openseek:  |
| Couldn\'t stat /var/log/lastlog: No such file or directory]{.c1}      |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c15}

Configure a Pre Login Message {#h.i6rbowfr1y5i .c23 .c22}
-----------------------------

7.  [On leaf01]{.c6}[: Create a pre login message file. ]{.c50}[Replace
    the existing content "Debian GNU/Linux 8" with a new message. ]{.c64
    .c54}

[]{#t.5b17d116573ca845901ec579f60ee25cb4320b6f}[]{#t.212}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo nano /etc/issue.net]{.c57 .c61      |
| .c51}                                                                 |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/issue.net                   |
|      Modified]{.c37 .c89 .c106}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c1}   |
|                                                                       |
| [\#  Authorized Users Only!!!  \#]{.c1}                               |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c1}   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c37 .c89 .c106}                                                   |
|                                                                       |
| [\^G]{.c37}[ Get Help  ]{.c7}[\^O]{.c37}[ WriteOut                    |
|  ]{.c7}[\^R]{.c37}[ Read File ]{.c7}[\^Y]{.c37}[ Prev Page            |
| ]{.c7}[\^K]{.c37}[ Cut Text  ]{.c7}[\^C]{.c37}[ Cur Pos]{.c1}         |
|                                                                       |
| [\^X]{.c37}[ Exit      ]{.c7}[\^J]{.c37}[ Justify                     |
| ]{.c7}[\^W]{.c37}[ Where Is  ]{.c7}[\^V]{.c37}[ Next Page             |
| ]{.c7}[\^U]{.c37}[ UnCut Text]{.c7}[\^T]{.c37}[ To Spell]{.c1}        |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

8.  [On leaf01]{.c6}[: Modify the SSHd configuration to use the pre
    login message file. ]{.c50}[Uncomment the line beginning with
    "\#Banner" . You can see that any text file could potentially be
    used as a pre login message.]{.c75}[ ]{.c15}

[]{#t.0a817e8ce35eecb4b7c09c89a9fd648da97b7b6f}[]{#t.213}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo nano /etc/ssh/sshd\_config]{.c57    |
| .c61 .c51}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/ssh/sshd\_config            |
|        Modified]{.c37 .c89 .c106}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [X11Forwarding yes]{.c1}                                              |
|                                                                       |
| [X11DisplayOffset 10]{.c1}                                            |
|                                                                       |
| [PrintMotd no]{.c1}                                                   |
|                                                                       |
| [PrintLastLog yes]{.c1}                                               |
|                                                                       |
| [TCPKeepAlive yes]{.c1}                                               |
|                                                                       |
| [\#UseLogin no]{.c1}                                                  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\#MaxStartups 10:30:60]{.c1}                                         |
|                                                                       |
| [Banner /etc/issue.net]{.c29 .c12}                                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\# Allow client to pass locale environment variables]{.c1}           |
|                                                                       |
| [AcceptEnv LANG LC\_\*]{.c1}                                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Subsystem sftp /usr/lib/openssh/sftp-server]{.c1}                    |
|                                                                       |
| []{.c37 .c89 .c106}                                                   |
|                                                                       |
| [\^G]{.c37}[ Get Help  ]{.c7}[\^O]{.c37}[ WriteOut                    |
|  ]{.c7}[\^R]{.c37}[ Read File ]{.c7}[\^Y]{.c37}[ Prev Page            |
| ]{.c7}[\^K]{.c37}[ Cut Text  ]{.c7}[\^C]{.c37}[ Cur Pos]{.c1}         |
|                                                                       |
| [\^X]{.c37}[ Exit      ]{.c7}[\^J]{.c37}[ Justify                     |
| ]{.c7}[\^W]{.c37}[ Where Is  ]{.c7}[\^V]{.c37}[ Next Page             |
| ]{.c7}[\^U]{.c37}[ UnCut Text]{.c7}[\^T]{.c37}[ To Spell]{.c1}        |
+-----------------------------------------------------------------------+

[]{.c15}

9.  [On leaf01]{.c6}[: Restart the SSH service and logout then login to
    see the new pre login message. ]{.c50}[Since passwordless logins
    have been setup in our lab environment we would need to login as
    another user to see that the pre login message comes before the
    password prompt; to accomplish that we'll try to login as another
    nonexistent user.]{.c75}[ ]{.c15}

[]{#t.d7592784c3853cd70b355cab9b7fc9823c93531a}[]{#t.214}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo systemctl restart sshd]{.c17}       |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[exit]{.c17}                              |
|                                                                       |
| [logout]{.c1}                                                         |
|                                                                       |
| [Connection to leaf01 closed.]{.c1}                                   |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ssh leaf01]{.c57 .c61 .c51}     |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c29   |
| .c12}                                                                 |
|                                                                       |
| [\#  Authorized Users Only!!!  \#]{.c29 .c12}                         |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c29   |
| .c12}                                                                 |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Welcome to Cumulus VX (TM)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Cumulus VX (TM) is a community supported virtual appliance designed  |
| for]{.c1}                                                             |
|                                                                       |
| [experiencing, testing and prototyping Cumulus Networks\' latest      |
| technology.]{.c1}                                                     |
|                                                                       |
| [For any questions or technical support, visit our community site     |
| at:]{.c1}                                                             |
|                                                                       |
| [http://community.cumulusnetworks.com]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [The registered trademark Linux (R) is used pursuant to a sublicense  |
| from LMI,]{.c1}                                                       |
|                                                                       |
| [the exclusive licensee of Linus Torvalds, owner of the mark on a     |
| world-wide]{.c1}                                                      |
|                                                                       |
| [basis.]{.c1}                                                         |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[exit]{.c57 .c61 .c51}                    |
|                                                                       |
| [logout]{.c1}                                                         |
|                                                                       |
| [Connection to leaf01 closed.]{.c1}                                   |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ssh bootcamp\@leaf01]{.c57 .c61 |
| .c51}                                                                 |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c29   |
| .c12}                                                                 |
|                                                                       |
| [\#  Authorized Users Only!!!  \#]{.c29 .c12}                         |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c29   |
| .c12}                                                                 |
|                                                                       |
| [bootcamp\@leaf01\'s password: ]{.c7}[\<CTRL\>\<c\>]{.c17}            |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c15}

Configure a Post Login Message {#h.hm9b45nyvw8i .c23 .c22}
------------------------------

10. [On leaf01]{.c6}[: Open the Message Of The Day (MOTD) file using the
    "nano" text editor. ]{.c50}[The MOTD file provides a post login
    message for your system. ]{.c64 .c54}

[]{#t.4b1c94b97a632e9dd907aecebdf370f68f8a4238}[]{#t.215}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo nano /etc/motd]{.c57 .c61 .c51}     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/motd                        |
|             \_]{.c37 .c89 .c106}                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Welcome to Cumulus VX (TM)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Cumulus VX (TM) is a community supported virtual appliance designed  |
| for]{.c1}                                                             |
|                                                                       |
| [experiencing, testing and prototyping Cumulus Networks\' latest      |
| technology.]{.c1}                                                     |
|                                                                       |
| [For any questions or technical support, visit our community site     |
| at:]{.c1}                                                             |
|                                                                       |
| [http://community.cumulusnetworks.com]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [The registered trademark Linux (R) is used pursuant to a sublicense  |
| from LMI,]{.c1}                                                       |
|                                                                       |
| [the exclusive licensee of Linus Torvalds, owner of the mark on a     |
| world-wide]{.c1}                                                      |
|                                                                       |
| [basis.]{.c1}                                                         |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [                               ]{.c7}[\[ Read 11 lines \]]{.c37 .c89 |
| .c106}                                                                |
|                                                                       |
| [\^G]{.c37}[ Get Help  ]{.c7}[\^O]{.c37}[ WriteOut                    |
|  ]{.c7}[\^R]{.c37}[ Read File ]{.c7}[\^Y]{.c37}[ Prev Page            |
| ]{.c7}[\^K]{.c37}[ Cut Text  ]{.c7}[\^C]{.c37}[ Cur Pos]{.c1}         |
|                                                                       |
| [\^X]{.c37}[ Exit      ]{.c7}[\^J]{.c37}[ Justify                     |
| ]{.c7}[\^W]{.c37}[ Where Is  ]{.c7}[\^V]{.c37}[ Next Page             |
| ]{.c7}[\^U]{.c37}[ UnCut Text]{.c7}[\^T]{.c37}[ To Spell]{.c1}        |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

11. [On leaf01]{.c6}[: Edit the MOTD to create a post login message of
    your choosing.  ]{.c50}[Note: The command \<CTRL\>\<k\> can be used
    to cut (or remove) entire lines of text.  ]{.c21}[ When complete,
    your edits might look like this:]{.c64 .c54}

[]{#t.67c8c29dee2b8d2a91142479fe1fed4e713fdb3e}[]{#t.216}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo nano /etc/motd]{.c57 .c61 .c51}     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/motd                        |
|       Modified]{.c37 .c89 .c106}                                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Credentials accepted, welcome to leaf01!]{.c1}                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [                               ]{.c7}[\[ Read 11 lines \]]{.c37 .c89 |
| .c106}                                                                |
|                                                                       |
| [\^G]{.c37}[ Get Help  ]{.c7}[\^O]{.c37}[ WriteOut                    |
|  ]{.c7}[\^R]{.c37}[ Read File ]{.c7}[\^Y]{.c37}[ Prev Page            |
| ]{.c7}[\^K]{.c37}[ Cut Text  ]{.c7}[\^C]{.c37}[ Cur Pos]{.c1}         |
|                                                                       |
| [\^X]{.c37}[ Exit      ]{.c7}[\^J]{.c37}[ Justify                     |
| ]{.c7}[\^W]{.c37}[ Where Is  ]{.c7}[\^V]{.c37}[ Next Page             |
| ]{.c7}[\^U]{.c37}[ UnCut Text]{.c7}[\^T]{.c37}[ To Spell]{.c1}        |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

12. [On leaf01]{.c6}[: Save the MOTD file using the \<Ctrl\>\<x\>
    command.  ]{.c50}[Answer "y" to save the file and hit "enter" to
    overwrite the existing file by saving the current file in the
    existing location. ]{.c64 .c54}

[]{#t.2a55282d4139895a9da6fb8ddadc59e9d732da3f}[]{#t.217}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo nano /etc/motd]{.c57 .c61 .c51}     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/motd                        |
|       Modified  ]{.c37 .c89 .c106}                                    |
|                                                                       |
| []{.c37 .c89 .c106}                                                   |
|                                                                       |
| [Credentials accepted, welcome to leaf01!]{.c7}                       |
|                                                                       |
| []{.c37 .c89 .c106}                                                   |
|                                                                       |
| []{.c37 .c89 .c106}                                                   |
|                                                                       |
| [Save modified buffer (ANSWERING \"No\" WILL DESTROY CHANGES) ?       |
|              ]{.c37 .c89 .c106}                                       |
|                                                                       |
| [ Y]{.c37}[ Yes]{.c89 .c57 .c106 .c61 .c66}                           |
|                                                                       |
| [ N]{.c37}[ No           ]{.c57 .c61 .c66}[\^C]{.c37}[ Cancel]{.c57   |
| .c61 .c66}                                                            |
+-----------------------------------------------------------------------+

[]{.c15}

13. [On leaf01]{.c6}[: Exit the SSH session to leaf01 and login again to
    view the new MOTD message. ]{.c15}

[]{#t.97a803950a1f612f3ac20700a06d08cc9a9f5a3b}[]{#t.218}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[exit ]{.c57 .c61 .c51}[logout]{.c1}      |
|                                                                       |
| [Connection to leaf01 closed.]{.c1}                                   |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ssh leaf01]{.c17}               |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c1}   |
|                                                                       |
| [\#  Authorized Users Only!!!  \#]{.c1}                               |
|                                                                       |
| [\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#]{.c1}   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Credentials accepted, welcome to leaf01!]{.c29 .c12}                 |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c1}                                          |
|                                                                       |
| []{.c1}                                                               |
+-----------------------------------------------------------------------+

[]{.c15}

Enable SNMP {#h.qjxpsq6g10gb .c23 .c22}
-----------

14. [On leaf01]{.c6}[: Enable SNMP polling. ]{.c15}

[]{#t.b6b561e0df003ed84452701cab658d1a38887725}[]{#t.219}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server listening-address    |
| all]{.c17}                                                            |
|                                                                       |
| [Warning: configuring all removes other listening addresses]{.c1}     |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server readonly-community   |
| public access any]{.c17}                                              |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c15}

15. [On oob-mgmt-server: Test that polling is working by polling the
    sysname OID on leaf01. ]{.c15}

[]{#t.1561ba5f98394a8b1d7dfaa2792dac920ec9f665}[]{#t.220}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo snmpget -v2c -c public     |
| 192.168.0.11 ]{.c57 .c61 .c51}[SNMPv2-MIB::sysName.0]{.c57 .c61 .c51} |
|                                                                       |
| [Created directory: /var/lib/snmp/mib\_indexes]{.c7}                  |
|                                                                       |
| [SNMPv2-MIB::sysName.0 = STRING: ]{.c7}[leaf01]{.c29 .c12}            |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c15}

16. [(Optional) ]{.c50}[On leaf01]{.c6}[: Enable SNMP traps. ]{.c15}

[]{#t.88f5d2c94f4b76d4be80fba65e4b6165a457665b}[]{#t.221}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server trap-destination     |
| 192.168.0.254 community-password public version 2c]{.c17}             |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server trap-link-up         |
| check-frequency 15]{.c57 .c61 .c51}                                   |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server trap-link-down       |
| check-frequency 10]{.c17}                                             |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server                      |
| trap-snmp-auth-failures]{.c17}                                        |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server system-contact       |
| Rocket ]{.c57 .c61 .c51}[Turtle]{.c17}                                |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add snmp-server system-location      |
| Bootcamp Lab]{.c57 .c61 .c51}                                         |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}              |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

[Verify traps are being sent by leaf01 using the TCPdump utility.]{.c15}

[Here it is useful to have two terminals open, one for leaf01 and
another terminal for oob-mgmt-server. What we're doing is starting a
tcpdump session on oob-mgmt-server to listen for SNMP traps then while
watching that output, we create events which will send the traps from
leaf01.]{.c64 .c54}

[]{.c64 .c54}

[On oob-mgmt-server: In the first terminal start the TCPdump session.
]{.c15}

[]{.c15}

[]{#t.35f097cb05ee817667d1c834f7add0fc8df19df2}[]{#t.222}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo tcpdump -c2 -i eth1 port   |
| 162]{.c17}                                                            |
|                                                                       |
| [tcpdump: verbose output suppressed, use -v or -vv for full protocol  |
| decode]{.c1}                                                          |
|                                                                       |
| [listening on eth1, link-type EN10MB (Ethernet), capture size 262144  |
| bytes]{.c1}                                                           |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

[On leaf01]{.c6}[: In the second terminal create a SNMP trap generating
event for the linkdown event by shutting a port. ]{.c15}

[]{#t.c39de19f3db515f0ad65637220bb5ba02d95c863}[]{#t.223}

  -----------------------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[sudo ifdown swp51]{.c57 .c61 .c51}
  -----------------------------------------------------------------

[]{.c15}

[On oob-mgmt-server: In the first terminal view the TCPdump session
which is already running. ]{.c50}[This trap will take at most 10 seconds
to be sent based in the "check-frequency" value configured above.]{.c64
.c54}

[]{#t.38634decbf4f0502ffde3cff17dcc4cbfbc941c5}[]{#t.224}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo tcpdump -c2 -i eth1 port   |
| 162]{.c17}                                                            |
|                                                                       |
| [tcpdump: verbose output suppressed, use -v or -vv for full protocol  |
| decode]{.c1}                                                          |
|                                                                       |
| [listening on eth1, link-type EN10MB (Ethernet), capture size 262144  |
| bytes]{.c1}                                                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [15:18:27.072635 IP leaf01.34345 \> oob-mgmt-server.snmp-trap:        |
|  V2Trap(236)  system.sysUpTime.0=246073 S:1.1.4.1.0=88.2.0.1          |
| 88.2.1.1.0=\"]{.c7}[CumulusLinkDOWN]{.c40 .c12}[\" 88.2.1.2.0=\"\"    |
| 88.2.1.3.0=\"\" 88.2.1.4.0=interfaces.ifTable.ifEntry.ifOperStatus.12 |
| 88.2.1.5.0=2 31.1.1.1.1.12=\"swp51\"                                  |
| interfaces.ifTable.ifEntry.ifIndex.12=12                              |
| interfaces.ifTable.ifEntry.ifAdminStatus.12=2                         |
| interfaces.ifTable.ifEntry.ifOperStatus.12=2]{.c1}                    |
|                                                                       |
| []{.c1}                                                               |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[On leaf01]{.c6}[: In the second terminal create a SNMP trap generating
event for the linkup event by turning the port up again. ]{.c15}

[]{#t.1f58ed499f3709e382fa8772d7eb41439058a6c8}[]{#t.225}

  ---------------------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[sudo ifup swp51]{.c57 .c61 .c51}
  ---------------------------------------------------------------

[]{.c15}

[On oob-mgmt-server: In the first terminal view the TCPdump session
which is already running. ]{.c50}[This trap will take at most 15 seconds
to be sent based in the "check-frequency" value configured
above.]{.c75}[ ]{.c15}

[]{#t.177ac962fed18397e4d8f97195fac64ac40734dd}[]{#t.226}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[sudo tcpdump -c2 -i eth1 port   |
| 162]{.c17}                                                            |
|                                                                       |
| [tcpdump: verbose output suppressed, use -v or -vv for full protocol  |
| decode]{.c1}                                                          |
|                                                                       |
| [listening on eth1, link-type EN10MB (Ethernet), capture size 262144  |
| bytes]{.c1}                                                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [15:18:27.072635 IP leaf01.34345 \> oob-mgmt-server.snmp-trap:        |
|  V2Trap(236)  system.sysUpTime.0=246073 S:1.1.4.1.0=88.2.0.1          |
| 88.2.1.1.0=\"CumulusLinkDOWN\" 88.2.1.2.0=\"\" 88.2.1.3.0=\"\"        |
| 88.2.1.4.0=interfaces.ifTable.ifEntry.ifOperStatus.12 88.2.1.5.0=2    |
| 31.1.1.1.1.12=\"swp51\" interfaces.ifTable.ifEntry.ifIndex.12=12      |
| interfaces.ifTable.ifEntry.ifAdminStatus.12=2                         |
| interfaces.ifTable.ifEntry.ifOperStatus.12=2]{.c1}                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [15:31:27.108768 IP leaf01.34345 \> oob-mgmt-server.snmp-trap:        |
|  V2Trap(234)  system.sysUpTime.0=324076 S:1.1.4.1.0=88.2.0.1          |
| 88.2.1.1.0=\"]{.c7}[CumulusLinkUP]{.c40 .c12}[\" 88.2.1.2.0=\"\"      |
| 88.2.1.3.0=\"\" 88.2.1.4.0=interfaces.ifTable.ifEntry.ifOperStatus.12 |
| 88.2.1.5.0=1 31.1.1.1.1.12=\"swp51\"                                  |
| interfaces.ifTable.ifEntry.ifIndex.12=12                              |
| interfaces.ifTable.ifEntry.ifAdminStatus.12=1                         |
| interfaces.ifTable.ifEntry.ifOperStatus.12=1]{.c1}                    |
+-----------------------------------------------------------------------+

[]{.c49 .c127}

[]{.c49 .c127}

[]{.c49 .c127}

**This concludes Lab 9.**


<!-- AIR:page -->
### Lab 10: eBGP Unnumbered (Optional)

[Objective:]{.c32 .c12}

[This lab will configure eBGP unnumbered between the leaf01/leaf02 to
spine01. This configuration will share the ip addresses of the loopback
interfaces on each device as well as the vlan10 and vlan20 subnets on
the leaf01 and leaf02 devices.]{.c45 .c54 .c75 .c85}

[]{.c69 .c54}

![](https://gitlab.com/cumulus-consulting/education/air-bc-lab-guide/raw/master/images/image4.png)

**Dependencies on other Labs:**

-   [Lab 4b must be completed before the start of this lab, or
    configurations need to be restored via playbook.]{.c45 .c50 .c54}

[]{.c45 .c50 .c54}

[Goals:]{.c32 .c12}

-   [Configure BGP unnumbered on spine01]{.c45 .c50 .c54}
-   [Configure BGP unnumbered on leaf01/leaf02]{.c45 .c50 .c54}
-   [Advertise loopback addresses into BGP ]{.c45 .c50 .c54}
-   [Advertise SVI subnets of leafs into BGP]{.c45 .c50 .c54}
-   [Verify BGP peering]{.c45 .c50 .c54}
-   [Verify BGP route advertisements]{.c50 .c100}

[]{.c32 .c127}

[Procedure:]{.c57 .c96 .c12}

[Apply loopback address to leaf01, leaf02 and spine01]{.c4} {#h.4jy27gh5c0ko .c23}
-----------------------------------------------------------

[]{.c69 .c54}

[]{#t.73da5ff81aacad71b92c824db59b9638634e28d1}[]{#t.227}

[Loopback Configuration]{.c8}

[]{.c8}

[Configuration↓   \\   Switch→ ]{.c8}

[leaf01]{.c8}

[leaf02]{.c8}

[spine01]{.c8}

[Loopback IP address]{.c11}

[10.255.255.1/32]{.c11}

[10.255.255.2/32]{.c11}

[10.255.255.101/32]{.c11}

[]{.c15}

[Remove all previous configuration]{.c4} {#h.wsikq5mjq085 .c23}
----------------------------------------

1.  [On the oob-mgmt-server, reset the switch configurations to the
    state in lab 1. ]{.c6}[ ]{.c15}

[]{#t.bc837de36553ba443174f257dd5915c2dadf37ad}[]{#t.228}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[cd BootcampAutomation]{.c17}    |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$                     |
| ]{.c7}[ansible-playbook lab4b.yml]{.c17}                              |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [...]{.c1}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PLAY RECAP                                                           |
| \*\*\*                                                                |
| \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\ |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c1} |
|                                                                       |
| [leaf01                     : ok=16   changed=12   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf02                     : ok=16   changed=11   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf03                     : ok=9    changed=4    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf04                     : ok=9    changed=4    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server01                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server02                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [spine01                    : ok=14   changed=10   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [spine02                    : ok=9    changed=4    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Tuesday 17 December 2019  18:29:34 +0000 (0:00:06.495)               |
| 0:01:20.035 \*\*\*\*\*\*]{.c1}                                        |
|                                                                       |
| [================                                                     |
| ===============================================================]{.c1} |
|                                                                       |
| [reset : Clear config                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 15.60s]{.c1}                                                          |
|                                                                       |
| [reset : Wait 5 seconds for port 22 to become open and contain        |
| \"OpenSSH\" \-- 11.81s]{.c1}                                          |
|                                                                       |
| [reset : Restore NTP                                                  |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                  |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 7.61s]{.c1}                                                           |
|                                                                       |
| [Restart Networking                                                   |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 6.50s]{.c1}                                                           |
|                                                                       |
| [Lab 4b \-- Deploy configuration                                      |
| \-\-\-\-\-\-\-                                                        |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 5.89s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 5.74s]{.c1}                                                           |
|                                                                       |
| [reset : Stop FRR                                                     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 5.29s]{.c1}                                                           |
|                                                                       |
| [Test PTM                                                             |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 3.66s]{.c1}                                                           |
|                                                                       |
| [reset : Apply Default Interface Configuration                        |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 3.37s]{.c1}     |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 2.49s]{.c1}                                                           |
|                                                                       |
| [reset : Copy the Default Interface Configuration in Place            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-- 2.31s]{.c1}                             |
|                                                                       |
| [reset : Restart PTM Daemon to Apply New PTM Config                   |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 1.95s]{.c1}               |
|                                                                       |
| [reset : Remove PTM file                                              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-                                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.80s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.48s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.34s]{.c1}                                                           |
|                                                                       |
| [Download the topology.dot file from the OOB-MGMT-SERVER              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 1.06s]{.c1}                         |
|                                                                       |
| [Copy Interfaces Configuration File                                   |
| \-\-\-                                                                |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 0.75s]{.c1}                                                           |
|                                                                       |
| [Install License                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 0.74s]{.c1}                                                           |
|                                                                       |
| [Restart PTM Daemon to Apply new Topology.dot file                    |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 0.62s]{.c1}             |
+-----------------------------------------------------------------------+

[]{.c15}

2.  [On leaf01]{.c6}[: Configure the loopback.]{.c15}

[]{#t.66fbf764f7bb5eecd790dc8b49a30807d95945d2}[]{#t.229}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add loopback lo ip address           |
| 10.255.255.1/32]{.c17}                                                |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c15}

3.  [On leaf02]{.c57 .c47}[: Configure the loopback. ]{.c15}

[]{#t.64484e9ffb2f98a845c9e1ba6aac1a982e2475e2}[]{#t.230}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add loopback lo ip address           |
| 10.255.255.2/32]{.c17}                                                |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c15}

4.  [On spine01: Configure the loopback. ]{.c15}

[]{#t.7ff679bbbae7c797dc455292347c75e5cdf659a1}[]{#t.231}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$]{.c7}[ ]{.c40 .c51}[net add loopback lo ip     |
| address 10.255.255.101/32]{.c17}                                      |
|                                                                       |
| [cumulus\@spine01:\~\$]{.c7}[ ]{.c40 .c51}[net commit]{.c17}          |
+-----------------------------------------------------------------------+

[]{.c15}

[Configure BGP unnumbered on spine01, leaf01 and leaf02]{.c4} {#h.m0vc04xtdwj .c23 .c35}
-------------------------------------------------------------

5.  [On spine01: Configure a BGP Autonomous System (AS) number for the
    routing instance. ]{.c15}

[]{#t.a9173feec32bdcf4c50ec509dab1ab4214b4771f}[]{#t.232}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp autonomous-system           |
| 65201]{.c17}                                                          |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp bestpath as-path            |
| multipath-relax]{.c17}                                                |
+-----------------------------------------------------------------------+

[]{.c15}

6.  [On spine01: Configure BGP peering on swp1 towards leaf01 and swp2
    towards leaf02. ]{.c15}

[]{#t.c4c16cb6f3b49cd4a8b90b1421c75b4771d724f5}[]{#t.233}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp neighbor swp1 interface     |
| remote-as external]{.c17}                                             |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp neighbor swp2 interface     |
| remote-as external]{.c17}                                             |
+-----------------------------------------------------------------------+

[]{.c15}

7.  [On spine01: Commit the changes. ]{.c15}

[]{#t.c876caaf02f05adeb65e21a894df3a2baea61feb}[]{#t.234}

  -------------------------------------------------
  [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c17}
  -------------------------------------------------

[]{.c49 .c58}

8.  [On leaf01]{.c6}[: Repeat steps 4-6. ]{.c15}

[]{#t.855edb5cca3e153e3a922fd383e1cedd2d31112a}[]{#t.235}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp autonomous-system            |
| 65101]{.c17}                                                          |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp bestpath as-path             |
| multipath-relax]{.c17}                                                |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp neighbor swp51 interface     |
| remote-as external]{.c17}                                             |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c49 .c58}

9.  [On leaf02]{.c57 .c47}[: Repeat steps 4-6. ]{.c15}

[]{#t.c557b00f6c9daf622ddb3f9d57c111f363a09a68}[]{#t.236}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp autonomous-system            |
| 65102]{.c17}                                                          |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp bestpath as-path             |
| multipath-relax]{.c17}                                                |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp neighbor swp51 interface     |
| remote-as external]{.c17}                                             |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[Verify BGP connectivity between fabric nodes]{.c4} {#h.iskkf3ye7kj0 .c23 .c35}
---------------------------------------------------

10. [On spine01: Verify BGP peering between spine and leafs. ]{.c15}

[]{#t.da733ad4204fe86037c3a18d0b1c8674bc728b84}[]{#t.237}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp summary]{.c17}             |
|                                                                       |
| [show bgp ipv4 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [BGP router identifier 10.255.255.101, local AS number 65201 vrf-id   |
| 0]{.c1}                                                               |
|                                                                       |
| [BGP table version 0]{.c1}                                            |
|                                                                       |
| [RIB entries 0, using 0 bytes of memory]{.c1}                         |
|                                                                       |
| [Peers 2, using 39 KiB of memory]{.c1}                                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ      |
|  Up/Down State/PfxRcd]{.c1}                                           |
|                                                                       |
| [leaf01(swp1)]{.c40 .c12}[    4      65101      12      13        0   |
|  0    0 ]{.c7}[00:00:30]{.c40 .c12}[            0]{.c1}               |
|                                                                       |
| [leaf02(swp2)]{.c40 .c12}[    4      65102       5       6        0   |
|  0    0 ]{.c7}[00:00:09]{.c40 .c12}[            0]{.c1}               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Total number of neighbors 2]{.c1}                                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp ipv6 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp l2vpn evpn summary]{.c1}                                    |
|                                                                       |
| [===========================]{.c1}                                    |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
+-----------------------------------------------------------------------+

[]{.c15}

[]{.c15}

[]{.c15}

11. [On leaf01]{.c6}[: Verify BGP peering between leafs and
    spine.]{.c15}

[]{#t.23a22b0453c47e1b2f4a8f0e5dd532ed16e32c4a}[]{#t.238}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net show bgp summary]{.c17}              |
|                                                                       |
| [show bgp ipv4 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [BGP router identifier 10.255.255.1, local AS number 65101 vrf-id     |
| 0]{.c1}                                                               |
|                                                                       |
| [BGP table version 0]{.c1}                                            |
|                                                                       |
| [RIB entries 0, using 0 bytes of memory]{.c1}                         |
|                                                                       |
| [Peers 1, using 19 KiB of memory]{.c1}                                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ      |
|  Up/Down State/PfxRcd]{.c1}                                           |
|                                                                       |
| [spine01(swp51)]{.c40 .c58}[  4      65201      37      37        0   |
|  0    0 ]{.c7}[00:01:43]{.c40 .c58}[            0]{.c1}               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Total number of neighbors 1]{.c1}                                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp ipv6 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp l2vpn evpn summary]{.c1}                                    |
|                                                                       |
| [===========================]{.c1}                                    |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
+-----------------------------------------------------------------------+

[]{.c49 .c58}

12. [On leaf02]{.c57 .c47}[: Verify BGP peering between leafs and
    spine]{.c15}

[]{#t.277799e6343e113a6ded22db0ad7abafab803b46}[]{#t.239}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net show bgp sum]{.c17}                  |
|                                                                       |
| [show bgp ipv4 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [BGP router identifier 10.255.255.2, local AS number 65102 vrf-id     |
| 0]{.c1}                                                               |
|                                                                       |
| [BGP table version 0]{.c1}                                            |
|                                                                       |
| [RIB entries 0, using 0 bytes of memory]{.c1}                         |
|                                                                       |
| [Peers 1, using 19 KiB of memory]{.c1}                                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ      |
|  Up/Down State/PfxRcd]{.c1}                                           |
|                                                                       |
| [spine01(swp51)]{.c40 .c47}[  4      65201      41      41        0   |
|  0    0 ]{.c7}[00:01:55]{.c40 .c47}[            0]{.c1}               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Total number of neighbors 1]{.c1}                                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp ipv6 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp l2vpn evpn summary]{.c1}                                    |
|                                                                       |
| [===========================]{.c1}                                    |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
+-----------------------------------------------------------------------+

[]{.c49 .c58}

[]{.c15}

[]{#t.33437534f1d8bcf94324d0d27e800a332b4ede4e}[]{#t.240}

[Important things to observe:]{.c15}

-   [The BGP neighbor shows the hostname of the BGP peer]{.c15}
-   [Only the peer is up, no routes are being advertised yet]{.c15}
-   [The BGP router identifier uses the loopback address]{.c15}

[]{.c69 .c54}

[]{#t.05be81d6f851ec4e8ee7740276d657ed5a8591c6}[]{#t.241}

[]{.c8}

[]{.c8}

[Routes↓   \\   Switch→ ]{.c8}

[leaf01]{.c8}

[leaf02]{.c8}

[spine01]{.c8}

[Subnets to be advertised]{.c11}

[10.255.255.1/32]{.c11}

[10.0.10.0/24]{.c11}

[10.0.20.0/24]{.c11}

[10.255.255.2/32]{.c11}

[10.0.10.0/24]{.c11}

[10.0.20.0/24]{.c11}

[10.255.255.101/32]{.c11}

[]{.c49 .c58}

13. [On spine01: Advertise loopback address into BGP. ]{.c15}

[]{#t.403ccb28defa839a0fd90ea017f3cb36a92fb139}[]{#t.242}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp network                     |
| 10.255.255.101/32]{.c17}                                              |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c17}                       |
+-----------------------------------------------------------------------+

[]{.c15}

14. [On leaf01]{.c6}[: Advertise loopback address into BGP. ]{.c15}

[]{#t.048f66e5c18807a2deb35ac63a5191360e43aa7f}[]{#t.243}

  -------------------------------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp network 10.255.255.1/32]{.c17}
  -------------------------------------------------------------------------

[]{.c15}

15. [On leaf01]{.c6}[: Advertise subnet for VLAN10. ]{.c15}

[]{#t.f7e170b255e5d907679a5d2a0d4f9109d9e118dd}[]{#t.244}

  ----------------------------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp network 10.0.10.0/24]{.c17}
  ----------------------------------------------------------------------

[]{.c15}

16. [On leaf01]{.c6}[: Advertise subnet for VLAN20. ]{.c15}

[]{#t.d09535777a85250f6def1b1b32a7dcc2e1834be8}[]{#t.245}

  ----------------------------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp network 10.0.20.0/24]{.c17}
  ----------------------------------------------------------------------

[]{.c15}

17. [On leaf01]{.c6}[: Commit the changes. ]{.c15}

[]{#t.1ece2b3ebea614be9e6b582505a5a6cabe3dd681}[]{#t.246}

  ------------------------------------------------
  [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c17}
  ------------------------------------------------

[]{.c49 .c58}

18. [On leaf02]{.c57 .c47}[: Repeat steps 13-15. Notice the different
    loopback IP that is advertised. ]{.c15}

[]{#t.1299fb493c3be3d439da77a65bb2e031d57f1c5f}[]{#t.247}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp network                      |
| 10.255.255.2/32]{.c17}                                                |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp network 10.0.10.0/24]{.c17}  |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp network 10.0.20.0/24]{.c17}  |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

Verify that BGP is advertising the routes {#h.jzbt466p5vbv .c23}
-----------------------------------------

19. [On spine01: Check that routes are being learned. ]{.c15}

[]{#t.b6502af553583e89c87214342f8ed5c9473f893b}[]{#t.248}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp]{.c17}                     |
|                                                                       |
| [show bgp ipv4 unicast]{.c1}                                          |
|                                                                       |
| [=====================]{.c1}                                          |
|                                                                       |
| [BGP table version is 7, local router ID is 10.255.255.101]{.c1}      |
|                                                                       |
| [Status codes: s suppressed, d damped, h history, \* valid, \> best,  |
| = multipath,]{.c1}                                                    |
|                                                                       |
| [              i internal, r RIB-failure, S Stale, R Removed]{.c1}    |
|                                                                       |
| [Origin codes: i - IGP, e - EGP, ? - incomplete]{.c1}                 |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [   Network          Next Hop            Metric LocPrf Weight         |
| Path]{.c1}                                                            |
|                                                                       |
| [\*= 10.0.10.0/24     swp2                     0             0 65102  |
| i]{.c29 .c12}                                                         |
|                                                                       |
| [\*\>                  swp1                     0             0 65101 |
| i]{.c29 .c12}                                                         |
|                                                                       |
| [\*= 10.0.20.0/24     swp2                     0             0 65102  |
| i]{.c29 .c12}                                                         |
|                                                                       |
| [\*\>                  swp1                     0             0 65101 |
| i]{.c29 .c12}                                                         |
|                                                                       |
| [\*\> 10.255.255.1/32  swp1                     0             0 65101 |
| i]{.c1}                                                               |
|                                                                       |
| [\*\> 10.255.255.2/32  swp2                     0             0 65102 |
| i]{.c1}                                                               |
|                                                                       |
| [\*\> 10.255.255.101/32]{.c1}                                         |
|                                                                       |
| [                    0.0.0.0                  0         32768 i]{.c1} |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Displayed  5 routes and 7 total paths]{.c1}                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp ipv6 unicast]{.c1}                                          |
|                                                                       |
| [=====================]{.c1}                                          |
|                                                                       |
| [No BGP prefixes displayed, 0 exist]{.c7}                             |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.5e04fcc483610ac3f20523b5e089fd805433c3d3}[]{#t.249}

[Important things to observe:]{.c15}

-   [The spine will have equal cost routes to both leaf01 and leaf02 for
    the 10.0.10.0/24 subnet since both leafs are advertising those
    networks]{.c50}
-   [AS PATH identifies where routes are originating]{.c15}
-   [NEXT HOP is the interface and not an IP address because of BGP
    unnumbered]{.c50}[ Where the next hop is equal to 0.0.0.0, that
    route is originated locally.]{.c15}


**This concludes Lab 10.**

<!-- AIR:page -->
### Lab 11: Snapshots and Package Management (Optional)

**Objective:**
This lab is designed to highlight the various rollback capabilities
present in Cumulus Linux. We'll install the Apache web server, test that
the switch can serve a text file and then rollback to a state before
Apache was installed. This same process can be extended to a whole
switch upgrade. This lab provides a different kind of rollback
capability from what was seen in Lab 2 with NCLU. NCLU only rolls back a
series of individual files and services. However the rollbacks performed
with the snapper utility shown in this section roll the entire switch
back to an earlier point in time.

**Dependencies on other Labs:**
-   None

**Goals:**
-   Install the Apache2 webserver software
-   Copy created/hosted file from leaf to oob-mgmt-server
-   Evaluate the differences between snapshots
-   Perform a rollback.

**Procedure:**

#### Installing Apache2

1.  [On leaf01]{.c6}[: First we have to update our cached listing
    locally with \`sudo apt-get update\`. ]{.c15}

[]{#t.6807e54acdfe179f1821afae85487c2c17b0c7dc}[]{#t.250}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo apt-get update]{.c17}               |
|                                                                       |
| [Get:1 http://repo3.cumulusnetworks.com CumulusLinux-3 InRelease      |
| \[7,645 B\]]{.c1}                                                     |
|                                                                       |
| [Get:2 http://repo3.cumulusnetworks.com                               |
| CumulusLinux-3-security-updates InRelease \[7,612 B\]]{.c1}           |
|                                                                       |
| [Get:3 http://repo3.cumulusnetworks.com CumulusLinux-3-updates        |
| InRelease \[7,690 B\]]{.c1}                                           |
|                                                                       |
| [Get:4 http://repo3.cumulusnetworks.com CumulusLinux-3/cumulus        |
| Sources \[20 B\]]{.c1}                                                |
|                                                                       |
| [Get:5 http://repo3.cumulusnetworks.com CumulusLinux-3/upstream       |
| Sources \[209 kB\]]{.c1}                                              |
|                                                                       |
| [Get:6 http://repo3.cumulusnetworks.com CumulusLinux-3/cumulus amd64  |
| Packages \[33.7 kB\]]{.c1}                                            |
|                                                                       |
| [Get:7 http://repo3.cumulusnetworks.com CumulusLinux-3/upstream amd64 |
| Packages \[499 kB\]]{.c1}                                             |
|                                                                       |
| [Get:8 http://repo3.cumulusnetworks.com                               |
| CumulusLinux-3-security-updates/cumulus Sources \[20 B\]]{.c1}        |
|                                                                       |
| [Get:9 http://repo3.cumulusnetworks.com                               |
| CumulusLinux-3-security-updates/upstream Sources \[35.7 kB\]]{.c1}    |
|                                                                       |
| [Get:10 http://repo3.cumulusnetworks.com                              |
| CumulusLinux-3-security-updates/cumulus amd64 Packages \[20 B\]]{.c1} |
|                                                                       |
| [Get:11 http://repo3.cumulusnetworks.com                              |
| CumulusLinux-3-security-updates/upstream amd64 Packages \[140         |
| kB\]]{.c1}                                                            |
|                                                                       |
| [Get:12 http://repo3.cumulusnetworks.com                              |
| CumulusLinux-3-updates/cumulus Sources \[23.6 kB\]]{.c1}              |
|                                                                       |
| [Get:13 http://repo3.cumulusnetworks.com                              |
| CumulusLinux-3-updates/upstream Sources \[214 kB\]]{.c1}              |
|                                                                       |
| [Get:14 http://repo3.cumulusnetworks.com                              |
| CumulusLinux-3-updates/cumulus amd64 Packages \[65.4 kB\]]{.c1}       |
|                                                                       |
| [Get:15 http://repo3.cumulusnetworks.com                              |
| CumulusLinux-3-updates/upstream amd64 Packages \[512 kB\]]{.c1}       |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3/cumulus          |
| Translation-en\_US]{.c1}                                              |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3/cumulus          |
| Translation-en                                                        |
|       ]{.c1}                                                          |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3/upstream         |
| Translation-en\_US                                                    |
|       ]{.c1}                                                          |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3/upstream         |
| Translation-en                                                        |
|      ]{.c1}                                                           |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com                                 |
| CumulusLinux-3-security-updates/cumulus Translation-en\_US            |
|                               ]{.c1}                                  |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com                                 |
| CumulusLinux-3-security-updates/cumulus Translation-en                |
|                              ]{.c1}                                   |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com                                 |
| CumulusLinux-3-security-updates/upstream Translation-en\_US           |
|                              ]{.c1}                                   |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com                                 |
| CumulusLinux-3-security-updates/upstream Translation-en               |
|                             ]{.c1}                                    |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3-updates/cumulus  |
| Translation-en\_US                                                    |
|  ]{.c1}                                                               |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3-updates/cumulus  |
| Translation-en                                                        |
| ]{.c1}                                                                |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3-updates/upstream |
| Translation-en\_US                                                    |
| ]{.c1}                                                                |
|                                                                       |
| [Ign http://repo3.cumulusnetworks.com CumulusLinux-3-updates/upstream |
| Translation-en                                                        |
|  ]{.c1}                                                               |
|                                                                       |
| [Fetched 1,756 kB in 6s (252 kB/s)                                    |
|                                                                       |
|  ]{.c1}                                                               |
|                                                                       |
| [Reading package lists\... Done]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c15}

2.  [On leaf01]{.c6}[: Find the Apache2 package. ]{.c50}[If you aren't
    sure of a package's name, use 'apt-cache search \<keyword\>' and the
    APT software will search through the package archives for any
    packages containing the keyword text. ]{.c64 .c54}

[]{#t.9a600673cb834d54d7464923c8d0a0ec32045faa}[]{#t.251}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[apt-cache search apache]{.c17}           |
|                                                                       |
| [apache2 - Apache HTTP Server]{.c29 .c58}                             |
|                                                                       |
| [apache2-bin - Apache HTTP Server (modules and other binary           |
| files)]{.c1}                                                          |
|                                                                       |
| [apache2-data - Apache HTTP Server (common files)]{.c1}               |
|                                                                       |
| [apache2-utils - Apache HTTP Server (utility programs for web         |
| servers)]{.c1}                                                        |
|                                                                       |
| [collectd-core - statistics collection and monitoring daemon (core    |
| system)]{.c1}                                                         |
|                                                                       |
| [javascript-common - Base support for JavaScript library              |
| packages]{.c1}                                                        |
|                                                                       |
| [libapr1 - Apache Portable Runtime Library]{.c1}                      |
|                                                                       |
| [libaprutil1 - Apache Portable Runtime Utility Library]{.c1}          |
|                                                                       |
| [libaprutil1-dbd-sqlite3 - Apache Portable Runtime Utility Library -  |
| SQLite3 Driver]{.c1}                                                  |
|                                                                       |
| [libaprutil1-ldap - Apache Portable Runtime Utility Library - LDAP    |
| Driver]{.c1}                                                          |
|                                                                       |
| [libfile-listing-perl - module to parse directory listings]{.c1}      |
|                                                                       |
| [nano - small, friendly text editor inspired by Pico]{.c1}            |
+-----------------------------------------------------------------------+

[]{.c15}

3.  [On leaf01]{.c6}[: Confirm Apache2 package is not installed.
    ]{.c50}[Apt-cache policy will show which versions of a package are
    available as well as the installed version.]{.c75}[ ]{.c15}

[]{#t.6e8692adf9ec39f4112405c94770b226b9a921c9}[]{#t.252}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[apt-cache policy apache2]{.c17}          |
|                                                                       |
| [apache2:]{.c1}                                                       |
|                                                                       |
| [  Installed: (none)]{.c29 .c58}                                      |
|                                                                       |
| [  Candidate: 2.4.10-10+deb8u16]{.c1}                                 |
|                                                                       |
| [  Version table:]{.c1}                                               |
|                                                                       |
| [     2.4.10-10+deb8u16 0]{.c1}                                       |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3-security-updates/upstream amd64 Packages]{.c1}         |
|                                                                       |
| [     2.4.10-10+deb8u12 0]{.c1}                                       |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3-updates/upstream amd64 Packages]{.c1}                  |
|                                                                       |
| [     2.4.10-10+deb8u7 0]{.c1}                                        |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3/upstream amd64 Packages]{.c1}                          |
+-----------------------------------------------------------------------+

[]{.c49 .c127}

4.  [On leaf01]{.c6}[: Install Apache2. Apache2 is an open-source HTTP
    server. ]{.c15}

[]{#t.564eae61018d7d009e5ca61e534f2ea6da1272fc}[]{#t.253}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo apt-get install apache2]{.c29 .c51} |
|                                                                       |
| [Reading package lists\... Done                                       |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [Building dependency tree                                             |
|                                                                       |
|  ]{.c1}                                                               |
|                                                                       |
| [Reading state information\... Done                                   |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [The following extra packages will be installed:                      |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [  apache2-bin apache2-data apache2-utils libapr1 libaprutil1         |
| libaprutil1-dbd-sqlite3 libaprutil1-ldap liblua5.1-0                  |
|       ]{.c1}                                                          |
|                                                                       |
| [Suggested packages:                                                  |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [  www-browser apache2-doc apache2-suexec-pristine                    |
| apache2-suexec-custom                                                 |
|                 ]{.c1}                                                |
|                                                                       |
| [Recommended packages:                                                |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [  ssl-cert                                                           |
|                                                                       |
|  ]{.c1}                                                               |
|                                                                       |
| [The following NEW packages will be installed:                        |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [  apache2 apache2-bin apache2-data apache2-utils libapr1 libaprutil1 |
| libaprutil1-dbd-sqlite3 libaprutil1-ldap liblua5.1-0                  |
| ]{.c1}                                                                |
|                                                                       |
| [0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.       |
|                                                                       |
|  ]{.c1}                                                               |
|                                                                       |
| [Need to get 1,935 kB of archives.                                    |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [After this operation, 6,575 kB of additional disk space will be      |
| used.                                                                 |
|   ]{.c1}                                                              |
|                                                                       |
| [Do you want to continue? \[Y/n\]                                     |
|                                                                ]{.c1} |
|                                                                       |
| [Get:1 http://repo3.cumulusnetworks.com/repo/ CumulusLinux-3/upstream |
| libapr1 amd64 1.5.1-3 \[95.3 kB\]                                     |
|  ]{.c1}                                                               |
|                                                                       |
| [Get:2 http://repo3.cumulusnetworks.com/repo/ CumulusLinux-3/upstream |
| libaprutil1 amd64 1.5.4-1 \[86.2 kB\]                                 |
|  ]{.c1}                                                               |
|                                                                       |
| [Get:3 http://repo3.cumulusnetworks.com/repo/ CumulusLinux-3/upstream |
| libaprutil1-dbd-sqlite3 amd64 1.5.4-1 \[19.1 kB\]                     |
|  ]{.c1}                                                               |
|                                                                       |
| [Get:4 http://repo3.cumulusnetworks.com/repo/ CumulusLinux-3/upstream |
| libaprutil1-ldap amd64 1.5.4-1 \[17.2 kB\]                            |
| ]{.c1}                                                                |
|                                                                       |
| [Get:5 http://repo3.cumulusnetworks.com/repo/ CumulusLinux-3/upstream |
| liblua5.1-0 amd64 5.1.5-7.1 \[108 kB\]                                |
| ]{.c1}                                                                |
|                                                                       |
| [Get:6 http://repo3.cumulusnetworks.com/repo/                         |
| CumulusLinux-3-security-updates/upstream apache2-bin amd64            |
| 2.4.10-10+deb8u16 \[1,041 kB\]    ]{.c1}                              |
|                                                                       |
| [Get:7 http://repo3.cumulusnetworks.com/repo/                         |
| CumulusLinux-3-security-updates/upstream apache2-utils amd64          |
| 2.4.10-10+deb8u16 \[196 kB\]    ]{.c1}                                |
|                                                                       |
| [Get:8 http://repo3.cumulusnetworks.com/repo/                         |
| CumulusLinux-3-security-updates/upstream apache2-data all             |
| 2.4.10-10+deb8u16 \[163 kB\]       ]{.c1}                             |
|                                                                       |
| [Get:9 http://repo3.cumulusnetworks.com/repo/                         |
| CumulusLinux-3-security-updates/upstream apache2 amd64                |
| 2.4.10-10+deb8u16 \[209 kB\]          ]{.c1}                          |
|                                                                       |
| [Fetched 1,935 kB in 1s (1,043 kB/s)                                  |
|                                                                       |
| ]{.c1}                                                                |
|                                                                       |
| [Creating pre-apt snapshot\... 92 done.]{.c40 .c58}[                  |
|                                                                       |
|            ]{.c1}                                                     |
|                                                                       |
| [Selecting previously unselected package libapr1:amd64.]{.c1}         |
|                                                                       |
| [(Reading database \... 28660 files and directories currently         |
| installed.)]{.c1}                                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c89 .c7 .c43}             |
|                                                                       |
| []{.c89 .c7 .c43}                                                     |
|                                                                       |
| [Enabling conf charset.]{.c1}                                         |
|                                                                       |
| [Enabling conf localized-error-pages.]{.c1}                           |
|                                                                       |
| [Enabling conf other-vhosts-access-log.]{.c1}                         |
|                                                                       |
| [Enabling conf security.]{.c1}                                        |
|                                                                       |
| [Enabling conf serve-cgi-bin.]{.c1}                                   |
|                                                                       |
| [Enabling site 000-default.]{.c1}                                     |
|                                                                       |
| [invoke-rc.d: policy-rc.d denied execution of start.]{.c1}            |
|                                                                       |
| [Processing triggers for libc-bin (2.19-18+deb8u10) \...]{.c1}        |
|                                                                       |
| [Processing triggers for systemd (215-17+deb8u13) \...]{.c1}          |
|                                                                       |
| [Creating post-apt snapshot\... 93 done.]{.c40 .c58}                  |
+-----------------------------------------------------------------------+

[]{.c49 .c127}

[N]{.c21}[ote the ]{.c21}[snapshot numbers]{.c6 .c43}[ in your lab's
output after the Apache2 install finishes.]{.c21}

[]{.c89 .c21 .c54}

5.  [On leaf01]{.c6}[: Verify Apache2 is installed with the command
    'apt-cache policy apache2'. ]{.c50}[You can also check 'dpkg -l' for
    a listing of installed packages.]{.c75}[ ]{.c15}

[]{#t.0a9555d45a8102838dca4ca48c895c33320029ce}[]{#t.254}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[apt-cache policy apache2]{.c57 .c61      |
| .c51}                                                                 |
|                                                                       |
| [apache2:]{.c1}                                                       |
|                                                                       |
| [  Installed: 2.4.10-10+deb8u16]{.c29 .c58}                           |
|                                                                       |
| [  Candidate: 2.4.10-10+deb8u16]{.c1}                                 |
|                                                                       |
| [  Version table:]{.c1}                                               |
|                                                                       |
| [ \*\*\* 2.4.10-10+deb8u16 0]{.c1}                                    |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3-security-updates/upstream amd64 Packages]{.c1}         |
|                                                                       |
| [        100 /var/lib/dpkg/status]{.c1}                               |
|                                                                       |
| [     2.4.10-10+deb8u12 0]{.c1}                                       |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3-updates/upstream amd64 Packages]{.c1}                  |
|                                                                       |
| [     2.4.10-10+deb8u7 0]{.c1}                                        |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3/upstream amd64 Packages]{.c1}                          |
+-----------------------------------------------------------------------+

[]{.c49 .c127}

6.  [On leaf01]{.c6}[: Create a new file in the /var/www/html/
    directory.  ]{.c15}

[]{#t.06705107ce9d86155dfadcf5256edf2aee24a5f8}[]{#t.255}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[echo \"This is leaf01.\" \| sudo tee     |
| /var/www/html/test]{.c17}                                             |
|                                                                       |
| [This is leaf01.]{.c1}                                                |
|                                                                       |
| [cumulus\@leaf01:\~\$]{.c7}[ cat /var/www/html/test ]{.c17}           |
|                                                                       |
| [This is leaf01.]{.c1}                                                |
|                                                                       |
| [cumulus\@leaf01:\~\$]{.c1}                                           |
+-----------------------------------------------------------------------+

[]{.c15}

7.  [On oob-mgmt-server: Access the webserver running on leaf01 with the
    curl command.]{.c50}[ ]{.c69 .c54}

[]{#t.643bbb20c0dc2f30a1e563a697b902ee632b73ed}[]{#t.256}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[curl http://leaf01/test]{.c17}  |
|                                                                       |
| [This is leaf01.]{.c1}                                                |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[Creating a Manual Snapshot]{.c4} {#h.8878eqg5tua2 .c23 .c22}
---------------------------------

8.  [On leaf01]{.c6}[: create a manual snapshot.]{.c50}[ ]{.c75}[Take
    note of \#8 and \#10. \#8 is the snapshot before the Apache2
    install. \#10 is the the snapshot just created.]{.c75}[ Your number
    may be different, so pay attention to the number noted above in the
    apache install step.]{.c75}[ ]{.c69 .c54}

[]{#t.6958c99f9164c2846ff4204b828a0c70492283a8}[]{#t.257}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo snapper create -d \"TEST\"]{.c57    |
| .c61 .c51}                                                            |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo snapper list]{.c57 .c61 .c51}       |
|                                                                       |
| [Type   \| \#  \| Pre \# \| Date                            \| User   |
| \| Cleanup \| Description                      \| Userdata     ]{.c1} |
|                                                                       |
| [\-\-\-\-\-\--+\-\-\--+\-\-                                           |
| \-\-\-\--+\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\ |
| -\-\--+\-\-\-\-\--+\-\-\-\-\-\-\-\--+\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\- |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--+\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [single \| 0  \|       \|                                 \| root \|  |
|         \| current                          \|              ]{.c1}    |
|                                                                       |
| [single \| 1  \|       \| Mon 09 Dec 2019 10:01:42 PM UTC \| root \|  |
|         \| Empty root filesystem            \|              ]{.c1}    |
|                                                                       |
| [pre    \| 2  \|       \| Mon 09 Dec 2019 10:14:16 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 3  \| 2     \| Mon 09 Dec 2019 10:14:30 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 4  \|       \| Mon 09 Dec 2019 10:15:34 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 5  \| 4     \| Mon 09 Dec 2019 10:15:37 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 6  \|       \| Mon 09 Dec 2019 10:15:43 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 7  \| 6     \| Mon 09 Dec 2019 10:15:45 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 8  \|       \| Mon 09 Dec 2019 10:20:53 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 9  \| 8     \| Mon 09 Dec 2019 10:21:04 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 10 \|       \| Mon 09 Dec 2019 10:22:13 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 11 \| 10    \| Mon 09 Dec 2019 10:22:15 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 88 \|       \| Tue 17 Dec 2019 06:32:42 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|              ]{.c1}  |
|                                                                       |
| [post   \| 89 \| 88    \| Tue 17 Dec 2019 06:32:49 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|              ]{.c1}  |
|                                                                       |
| [pre    \| 90 \|       \| Tue 17 Dec 2019 07:02:33 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|              ]{.c1}  |
|                                                                       |
| [post   \| 91 \| 90    \| Tue 17 Dec 2019 07:02:35 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|              ]{.c1}  |
|                                                                       |
| [pre    \| 92 \|       \| Tue 17 Dec 2019 07:14:04 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c29    |
| .c58}                                                                 |
|                                                                       |
| [post   \| 93 \| 92    \| Tue 17 Dec 2019 07:14:57 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c29    |
| .c58}                                                                 |
|                                                                       |
| [single \| 94 \|       \| Tue 17 Dec 2019 07:19:43 PM UTC \| root \|  |
|         \| TEST                             \|]{.c29 .c58}            |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

9.  [On leaf01]{.c6}[: Evaluate the differences between snapshot 93 and
    94 for the test file.]{.c50}

[]{.c69 .c54}

[Note: These index numbers 9 and 10 may be different in your output so
please verify the numbers shown in step 8 and use the appropriate index
numbers for your environment. ]{.c89 .c21 .c54}

[]{.c89 .c21 .c54}

[]{#t.19960931c59004496654a6c91d3af8407fc27ef2}[]{#t.258}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo snapper diff 9..10                  |
| /var/www/html/test]{.c17}                                             |
|                                                                       |
| [\-\-- /.snapshots/93/snapshot/var/www/html/test   1970-01-01         |
| 00:00:00.000000000 +0000]{.c1}                                        |
|                                                                       |
| [+++ /.snapshots/94/snapshot/var/www/html/test   2019-12-17           |
| 19:18:34.552295808 +0000]{.c1}                                        |
|                                                                       |
| [@@ -0,0 +1 @@]{.c1}                                                  |
|                                                                       |
| [+This is leaf01.]{.c1}                                               |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

Restore a Single File from a Previous Snapshot {#h.pwiheuud3ej6 .c23 .c22}
----------------------------------------------

10. [On leaf01]{.c6}[: Restore the '/var/www/html/test' file to the
    state it had in the previous snapshot using snapper. ]{.c50}[ The
    undochange will remove our newly created test file. ]{.c75} [Note:
    These numbers 9 and 10 may be different in your output so please use
    the index numbers shown in your environment in step 8.
    ]{.c21}[ ]{.c69 .c54}

[]{#t.9836f0e3abe7bc55c3ba2db87cac4ad1d2aa64b8}[]{#t.259}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[cat /var/www/html/test]{.c57 .c61 .c51}  |
|                                                                       |
| [This is leaf01.]{.c1}                                                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo snapper undochange 93..94           |
| /var/www/html/test]{.c57 .c61 .c51}                                   |
|                                                                       |
| [create:0 modify:0 ]{.c7}[delete:1]{.c29 .c58}                        |
|                                                                       |
| []{.c29 .c90}                                                         |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[cat /var/www/html/test]{.c89 .c102 .c57  |
| .c61}                                                                 |
|                                                                       |
| [cat: /var/www/html/test: No such file or directory]{.c1}             |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[Perform a Rollback]{.c4} {#h.69kyw66xasmh .c23 .c22}
-------------------------

11. [On leaf01]{.c6}[: Rollback the entire switch to the snapshot before
    Apache2 was installed, and check the snapper listing.]{.c50}[ ]{.c69
    .c54}

[]{#t.a7e143497b6e8bdc5fc66351ecaaa8c8edcbe486}[]{#t.260}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo snapper rollback -d \"PREAPACHE\"   |
| 92]{.c57 .c61 .c120}                                                  |
|                                                                       |
| [Creating read-only snapshot of current system. (Snapshot 95.)]{.c1}  |
|                                                                       |
| [Creating read-write snapshot of snapshot 92. (Snapshot 96.)]{.c1}    |
|                                                                       |
| [Setting default subvolume to snapshot 96.]{.c1}                      |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo snapper list]{.c17}                 |
|                                                                       |
| [Type   \| \#  \| Pre \# \| Date                            \| User   |
| \| Cleanup \| Description                      \| Userdata]{.c1}      |
|                                                                       |
| [\-\-\-\-\-\--+\-\-\--+\-\-                                           |
| \-\-\-\--+\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\ |
| -\-\--+\-\-\-\-\--+\-\-\-\-\-\-\-\--+\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\- |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--+\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [single \| 0  \|       \|                                 \| root \|  |
|         \| current                          \|]{.c1}                  |
|                                                                       |
| [single \| 1  \|       \| Mon 09 Dec 2019 10:01:42 PM UTC \| root \|  |
| number  \| Empty root filesystem            \|]{.c1}                  |
|                                                                       |
| [pre    \| 2  \|       \| Mon 09 Dec 2019 10:14:16 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 3  \| 2     \| Mon 09 Dec 2019 10:14:30 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 4  \|       \| Mon 09 Dec 2019 10:15:34 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 5  \| 4     \| Mon 09 Dec 2019 10:15:37 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 6  \|       \| Mon 09 Dec 2019 10:15:43 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 7  \| 6     \| Mon 09 Dec 2019 10:15:45 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 8  \|       \| Mon 09 Dec 2019 10:20:53 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 9  \| 8     \| Mon 09 Dec 2019 10:21:04 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 10 \|       \| Mon 09 Dec 2019 10:22:13 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 11 \| 10    \| Mon 09 Dec 2019 10:22:15 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [pre    \| 88 \|       \| Tue 17 Dec 2019 06:32:42 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|]{.c1}                |
|                                                                       |
| [post   \| 89 \| 88    \| Tue 17 Dec 2019 06:32:49 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|]{.c1}                |
|                                                                       |
| [pre    \| 90 \|       \| Tue 17 Dec 2019 07:02:33 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|]{.c1}                |
|                                                                       |
| [post   \| 91 \| 90    \| Tue 17 Dec 2019 07:02:35 PM UTC \| root \|  |
| number  \| nclu \"net commit\" (user cumulus) \|]{.c1}                |
|                                                                       |
| [pre    \| 92 \|       \| Tue 17 Dec 2019 07:14:04 PM UTC \| root \|  |
| number  \| pre-apt                          \| important=yes]{.c1}    |
|                                                                       |
| [post   \| 93 \| 92    \| Tue 17 Dec 2019 07:14:57 PM UTC \| root \|  |
| number  \| post-apt                         \| important=yes]{.c1}    |
|                                                                       |
| [single \| 94 \|       \| Tue 17 Dec 2019 07:19:43 PM UTC \| root \|  |
|         \| TEST                             \|]{.c1}                  |
|                                                                       |
| [single \| 95 \|       \| Tue 17 Dec 2019 07:25:41 PM UTC \| root \|  |
| number  \| PREAPACHE                        \| important=yes]{.c29    |
| .c58}                                                                 |
|                                                                       |
| [single \| 96 \|       \| Tue 17 Dec 2019 07:25:41 PM UTC \| root \|  |
|         \| PREAPACHE                        \|]{.c29 .c58}            |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

12. [On leaf01]{.c6}[: Reboot to finish the rollback to before Apache2
    was installed. Login to leaf01.]{.c50}[  ]{.c75}[Note: it will take
    less than a minute for leaf01 to reboot.]{.c21}[ ]{.c69 .c54}

[]{#t.97bf0b749da76b8c33223b1a197871036b4c428a}[]{#t.261}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo reboot]{.c17}                       |
|                                                                       |
| [Connection to leaf01 closed by remote host.]{.c1}                    |
|                                                                       |
| [Connection to leaf01 closed.]{.c1}                                   |
|                                                                       |
| []{.c29 .c51}                                                         |
|                                                                       |
| [cumulus\@oob-mgmt-server:\~\$ ]{.c7}[ssh leaf01]{.c17}               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Welcome to Cumulus VX (TM)]{.c1}                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Cumulus VX (TM) is a community supported virtual appliance designed  |
| for]{.c1}                                                             |
|                                                                       |
| [experiencing, testing and prototyping Cumulus Networks\' latest      |
| technology.]{.c1}                                                     |
|                                                                       |
| [For any questions or technical support, visit our community site     |
| at:]{.c1}                                                             |
|                                                                       |
| [http://community.cumulusnetworks.com]{.c1}                           |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [The registered trademark Linux (R) is used pursuant to a sublicense  |
| from LMI,]{.c1}                                                       |
|                                                                       |
| [the exclusive licensee of Linus Torvalds, owner of the mark on a     |
| world-wide]{.c1}                                                      |
|                                                                       |
| [basis.]{.c1}                                                         |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cumulus\@leaf01:\~\$]{.c7}                                           |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

13. [On leaf01]{.c6}[: Confirm that Apache2 is not installed after the
    rollback.]{.c50}[ ]{.c69 .c54}

[]{#t.056c6e6c45851b1f4df928175dd711102d664360}[]{#t.262}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[apt-cache policy apache2]{.c17}          |
|                                                                       |
| [apache2:]{.c1}                                                       |
|                                                                       |
| [  Installed: (none)]{.c29 .c58}                                      |
|                                                                       |
| [  Candidate: 2.4.10-10+deb8u16]{.c1}                                 |
|                                                                       |
| [  Version table:]{.c1}                                               |
|                                                                       |
| [     2.4.10-10+deb8u16 0]{.c1}                                       |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3-security-updates/upstream amd64 Packages]{.c1}         |
|                                                                       |
| [     2.4.10-10+deb8u12 0]{.c1}                                       |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3-updates/upstream amd64 Packages]{.c1}                  |
|                                                                       |
| [     2.4.10-10+deb8u7 0]{.c1}                                        |
|                                                                       |
| [        991 http://repo3.cumulusnetworks.com/repo/                   |
| CumulusLinux-3/upstream amd64 Packages]{.c1}                          |
|                                                                       |
| [cumulus\@leaf01:\~\$]{.c1}                                           |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[]{.c15}

[]{#t.7fbb468aceb1baf155779b50526c36993faebe2a}[]{#t.263}

[Important things to observe:]{.c15}

-   [Packages can be individually installed or upgraded]{.c15}
-   [System snapshots are created automatically before and after
    software installation or whenever NCLU runs]{.c15}
-   [System rollbacks are different from the rollbacks performed by
    NCLU]{.c15}
-   [System rollbacks require a reboot of the switch to apply]{.c15}

[]{.c49 .c127}

[]{.c49 .c127}

[]{.c49 .c127}

**This concludes Lab 11.**


<!-- AIR:page -->
### Lab 12: Configuring Routing Enhancements (Optional)

**Objective:**

This lab will apply some more common enhancements to the routing
configured in lab 5a. Configuration of route-maps will be shown along
with Bidirectional Forwarding Detection (BFD) to detect link failures.

**Dependencies on other Labs:**
-   Lab 1 is required to perform the PTM link verification in this lab.
-   Lab 5b is assumed to be complete before starting this lab.

**Goals:**
-   Use route maps to advertise loopback addresses
-   Configure BFD
-   Enable PTM link verification to the forming of BGP neighbors

**Procedure:**

#### Use route-maps to advertise loopback addresses

1.  [On the oob-mgmt-server, reset the switch configurations to the
    state in lab lab 5b.]{.c15}

[]{#t.6b370374f3b47172c8a1a19f64c002ad6b047695}[]{#t.264}

+-----------------------------------------------------------------------+
| [cumulus\@oob-mgmt-server:\~/BootcampAutomation\$                     |
| ]{.c7}[ansible-playbook lab5b.yml]{.c17}                              |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [PLAY RECAP                                                           |
| \*\*\*                                                                |
| \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\ |
| *\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*]{.c1} |
|                                                                       |
| [leaf01                     : ok=17   changed=13   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf02                     : ok=17   changed=13   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf03                     : ok=9    changed=4    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [leaf04                     : ok=9    changed=4    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server01                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [server02                   : ok=3    changed=1    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [spine01                    : ok=16   changed=12   unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| [spine02                    : ok=9    changed=4    unreachable=0      |
|  failed=0]{.c1}                                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Tuesday 17 December 2019  19:37:52 +0000 (0:00:06.433)               |
| 0:01:41.915 \*\*\*\*\*\*]{.c1}                                        |
|                                                                       |
| [================                                                     |
| ===============================================================]{.c1} |
|                                                                       |
| [reset : Clear config                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 15.76s]{.c1}                                                          |
|                                                                       |
| [Lab 6 \-- Deploy Leaf FRR configuration                              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--     |
| 12.36s]{.c1}                                                          |
|                                                                       |
| [Lab 6 \-- Deploy Configuration on Spine01                            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--         |
| 12.13s]{.c1}                                                          |
|                                                                       |
| [reset : Wait 5 seconds for port 22 to become open and contain        |
| \"OpenSSH\" \-- 11.80s]{.c1}                                          |
|                                                                       |
| [reset : Restore NTP                                                  |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                  |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 7.15s]{.c1}                                                           |
|                                                                       |
| [Restart Networking                                                   |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                                |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 6.43s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 5.75s]{.c1}                                                           |
|                                                                       |
| [reset : Stop FRR                                                     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 5.30s]{.c1}                                                           |
|                                                                       |
| [Test PTM                                                             |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 3.66s]{.c1}                                                           |
|                                                                       |
| [reset : Apply Default Interface Configuration                        |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 3.35s]{.c1}     |
|                                                                       |
| [Lab 6 \-- Deploy Leaf interface configuration                        |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 3.29s]{.c1}   |
|                                                                       |
| [reset : Copy the Default Interface Configuration in Place            |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-- 2.26s]{.c1}                             |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 2.19s]{.c1}                                                           |
|                                                                       |
| [reset : Restart PTM Daemon to Apply New PTM Config                   |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 1.89s]{.c1}               |
|                                                                       |
| [reset : Remove PTM file                                              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-                                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.77s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.44s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.32s]{.c1}                                                           |
|                                                                       |
| [Gathering Facts                                                      |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-                          |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 1.10s]{.c1}                                                           |
|                                                                       |
| [Download the topology.dot file from the OOB-MGMT-SERVER              |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- 0.89s]{.c1}                         |
|                                                                       |
| [Copy Interfaces Configuration File                                   |
| \-\-\-                                                                |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-- |
| 0.73s]{.c1}                                                           |
+-----------------------------------------------------------------------+

[]{.c15}

2.  [On spine01: Create route-map to match loopback address.
    ]{.c50}[Route-maps allow BGP to customize behaviors for specific
    routes. In this case a route-map is being used to match routes that
    are associated with the loopback interface. This technique is often
    preferred to a "network" statement because the configuration will
    look the same on every node whereas the network statement would be
    different for each node to reflect the specific loopback ip address
    of the node in question.]{.c75}[ ]{.c15}

[]{#t.d652c00b09ef166e53ff1f2a4e38d5c51e341d2d}[]{#t.265}

  ----------------------------------------------------------------------------------------------------------------
  [cumulus\@spine01:\~\$ ]{.c7}[net add routing route-map LOOPBACK permit 10 match interface lo]{.c57 .c61 .c51}
  ----------------------------------------------------------------------------------------------------------------

[]{.c15}

3.  [On spine01: Redistribute connected routes via route-map.
    ]{.c50}[This statement will redistribute the routes matched by the
    route-map "LOOPBACK" into the BGP process. In this case the
    route-map contains the route for the loopback interface on
    spine01.]{.c75}[ ]{.c15}

[]{#t.f9c600919d384e96d463f5ee36699cb08315b0e6}[]{#t.266}

  ------------------------------------------------------------------------------------------------------
  [cumulus\@spine01:\~\$ ]{.c7}[net add bgp redistribute connected route-map LOOPBACK]{.c57 .c61 .c51}
  ------------------------------------------------------------------------------------------------------

[]{.c15}

4.  [On spine01: Commit the changes. ]{.c50}[After committing these
    changes, all devices should continue to see the spine01 loopback ip
    address advertised via BGP.]{.c75}[ ]{.c15}

[]{#t.c876caaf02f05adeb65e21a894df3a2baea61feb}[]{#t.267}

  -----------------------------------------------------------
  [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}
  -----------------------------------------------------------

[]{.c69 .c54}

[Verify Route Advertisement]{.c4} {#h.1ovdqadqsqt3 .c23}
---------------------------------

5.  [On spine01: Check that the loopback IP address is be advertised
    into BGP. ]{.c50}[The next-hop v]{.c75}[alue of
    "0.0.0.0"]{.c75}[ means that the route is being advertised into BGP
    from the local device.]{.c75}[ ]{.c15}

[]{#t.698845fad817d8274f207dedeb5939c7236ddc4d}[]{#t.268}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp]{.c17}                     |
|                                                                       |
| [show bgp ipv4 unicast]{.c1}                                          |
|                                                                       |
| [=====================]{.c1}                                          |
|                                                                       |
| [BGP table version is 1, local router ID is 10.255.255.101]{.c1}      |
|                                                                       |
| [Status codes: s suppressed, d damped, h history, \* valid, \> best,  |
| = multipath,]{.c1}                                                    |
|                                                                       |
| [              i internal, r RIB-failure, S Stale, R Removed]{.c1}    |
|                                                                       |
| [Origin codes: i - IGP, e - EGP, ? - incomplete]{.c1}                 |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [   Network          Next Hop            Metric LocPrf Weight         |
| Path]{.c1}                                                            |
|                                                                       |
| [\*\> 10.255.255.101/32]{.c29 .c12}                                   |
|                                                                       |
| [                    0.0.0.0]{.c40 .c12}[                  0          |
| 32768 ?]{.c1}                                                         |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Displayed  1 routes and 1 total paths]{.c1}                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp ipv6 unicast]{.c1}                                          |
|                                                                       |
| [=====================]{.c1}                                          |
|                                                                       |
| [No BGP prefixes displayed, 0 exist]{.c7}                             |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.7ce333cb5ffcfce04fb8db53c0ebb1da7cd766db}[]{#t.269}

[Important things to observe:]{.c15}

-   [The route-map matches on the loopback interface and as a result, a
    route shows up in ]{.c50}[BGP for any IP address applied to
    loopback.]{.c15}

[]{.c69 .c54}

[Tune the BGP timers]{.c4} {#h.1sk3yvyo6dji .c23}
--------------------------

6.  [On spine01: Reduce the keepalive and holdtimer. ]{.c50}[These
    timers are used to keep BGP neighbors healthy and inform our peer
    that we're still here. The keepalive timer in this case is 1 second,
    the holdtime is 3 seconds. That ratio of 1:3 is very common. In
    service provider settings out on the internet the typical settings
    are 60:180 for keepalive:holdtime. Inside the datacenter we can tune
    BGP to be much faster.]{.c75}[ ]{.c15}

[]{#t.be82c29c6318bed73c9f2f32f5bd9728d99aa487}[]{#t.270}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp timers 1 3]{.c17}           |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}             |
+-----------------------------------------------------------------------+

[]{.c15}

7.  [On spine01: Clear the peer on swp1 to apply the new configuration.
    ]{.c50}[Clearing the BGP peer is a disruptive process which tells
    BGP to re-form the neighborship. You would probably not want to run
    this command in production outside of a maintenance window.
    Reforming the neighborship here forces BGP to use the newly
    configured timers.]{.c75}[ ]{.c15}

[]{#t.3525db806e32c38b691cff733bee3cb215053714}[]{#t.271}

  ---------------------------------------------------------------------------
  [cumulus\@spine01:\~\$ ]{.c7}[net clear bgp 10.255.255.1]{.c57 .c61 .c51}
  ---------------------------------------------------------------------------

[]{.c15}

Verify the BGP timers are set {#h.2j8j7dym2yqg .c23}
-----------------------------

8.  [On spine01: See the new configuration is applied to the leaf01
    neighbor on swp1. ]{.c15}

[]{#t.8363418eed806e0f6b65b131c5aecdfc816e1e61}[]{#t.272}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp neighbor                   |
| 10.255.255.1]{.c17}                                                   |
|                                                                       |
| [BGP neighbor is 10.255.255.1, remote AS 65100, local AS 65100,       |
| internal link]{.c1}                                                   |
|                                                                       |
| [Hostname: leaf01]{.c1}                                               |
|                                                                       |
| [  BGP version 4, remote router ID 10.255.255.1]{.c1}                 |
|                                                                       |
| [  BGP state = Established, up for 00:00:11]{.c1}                     |
|                                                                       |
| [  Last read 00:00:00, Last write 00:00:00]{.c1}                      |
|                                                                       |
| [  ]{.c7}[Hold time is 3, keepalive interval is 1 seconds]{.c29 .c12} |
|                                                                       |
| [  Configured hold time is 3, keepalive interval is 1 seconds]{.c29   |
| .c12}                                                                 |
|                                                                       |
| [  Neighbor capabilities:]{.c1}                                       |
|                                                                       |
| [    4 Byte AS: advertised and received]{.c1}                         |
|                                                                       |
| [    AddPath:]{.c1}                                                   |
|                                                                       |
| [      IPv4 Unicast: RX advertised IPv4 Unicast and received]{.c1}    |
|                                                                       |
| [    Route refresh: advertised and received(old & new)]{.c1}          |
|                                                                       |
| [    Address Family IPv4 Unicast: advertised and received]{.c1}       |
|                                                                       |
| [    Hostname Capability: advertised (name: spine01,domain name: n/a) |
| received (name: leaf01,domain name: n/a)]{.c1}                        |
|                                                                       |
| [    Graceful Restart Capabilty: advertised and received]{.c1}        |
|                                                                       |
| [      Remote Restart timer is 120 seconds]{.c1}                      |
|                                                                       |
| [      Address families by peer:]{.c1}                                |
|                                                                       |
| [        none]{.c1}                                                   |
|                                                                       |
| [  Graceful restart informations:]{.c1}                               |
|                                                                       |
| [    End-of-RIB send: IPv4 Unicast]{.c1}                              |
|                                                                       |
| [    End-of-RIB received: IPv4 Unicast]{.c1}                          |
|                                                                       |
| [  Message statistics:]{.c1}                                          |
|                                                                       |
| [    Inq depth is 0]{.c1}                                             |
|                                                                       |
| [    Outq depth is 0]{.c1}                                            |
|                                                                       |
| [                         Sent       Rcvd]{.c1}                       |
|                                                                       |
| [    Opens:                  2          2]{.c1}                       |
|                                                                       |
| [    Notifications:          2          0]{.c1}                       |
|                                                                       |
| [    Updates:                4          2]{.c1}                       |
|                                                                       |
| [    Keepalives:            96         96]{.c1}                       |
|                                                                       |
| [    Route Refresh:          0          0]{.c1}                       |
|                                                                       |
| [    Capability:             0          0]{.c1}                       |
|                                                                       |
| [    Total:                104        100]{.c1}                       |
|                                                                       |
| [  Minimum time between advertisement runs is 0 seconds]{.c1}         |
|                                                                       |
| [  Update source is lo]{.c1}                                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ For address family: IPv4 Unicast]{.c1}                              |
|                                                                       |
| [  Update group 1, subgroup 1]{.c1}                                   |
|                                                                       |
| [  Packet Queue length 0]{.c1}                                        |
|                                                                       |
| [  Route-Reflector Client]{.c1}                                       |
|                                                                       |
| [  Community attribute sent to this neighbor(all)]{.c1}               |
|                                                                       |
| [  0 accepted prefixes]{.c1}                                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  Connections established 2; dropped 1]{.c1}                         |
|                                                                       |
| [  Last reset 00:00:13, due to User reset]{.c1}                       |
|                                                                       |
| [Local host: 10.255.255.101, Local port: 49616]{.c1}                  |
|                                                                       |
| [Foreign host: 10.255.255.1, Foreign port: 179]{.c1}                  |
|                                                                       |
| [Nexthop: 10.255.255.101]{.c1}                                        |
|                                                                       |
| [Nexthop global: ::]{.c1}                                             |
|                                                                       |
| [Nexthop local: ::]{.c1}                                              |
|                                                                       |
| [BGP connection: non shared network]{.c1}                             |
|                                                                       |
| [BGP Connect Retry Timer in Seconds: 10]{.c1}                         |
|                                                                       |
| [Read thread: on  Write thread: on]{.c7}                              |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

9.  [On spine01: Because the swp2 neighbor has not been cleared, the old
    default values for  keepalive and holdtime are still in use. ]{.c15}

[]{#t.a625481e006911c470065067df22b09c359be307}[]{#t.273}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp neighbor                   |
| 10.255.255.2]{.c17}                                                   |
|                                                                       |
| [BGP neighbor is 10.255.255.2, remote AS 65100, local AS 65100,       |
| internal link]{.c1}                                                   |
|                                                                       |
| [Hostname: leaf02]{.c1}                                               |
|                                                                       |
| [  BGP version 4, remote router ID 10.255.255.2]{.c1}                 |
|                                                                       |
| [  BGP state = Established, up for 00:05:21]{.c1}                     |
|                                                                       |
| [  Last read 00:00:03, Last write 00:00:03]{.c1}                      |
|                                                                       |
| [  ]{.c7}[Hold time is 9, keepalive interval is 3 seconds]{.c29 .c12} |
|                                                                       |
| [  Configured hold time is 3, keepalive interval is 1 seconds]{.c29   |
| .c12}                                                                 |
|                                                                       |
| [  Neighbor capabilities:]{.c1}                                       |
|                                                                       |
| [    4 Byte AS: advertised and received]{.c1}                         |
|                                                                       |
| [    AddPath:]{.c1}                                                   |
|                                                                       |
| [      IPv4 Unicast: RX advertised IPv4 Unicast and received]{.c1}    |
|                                                                       |
| [    Route refresh: advertised and received(old & new)]{.c1}          |
|                                                                       |
| [    Address Family IPv4 Unicast: advertised and received]{.c1}       |
|                                                                       |
| [    Hostname Capability: advertised (name: spine01,domain name: n/a) |
| received (name: leaf02,domain name: n/a)]{.c1}                        |
|                                                                       |
| [    Graceful Restart Capabilty: advertised and received]{.c1}        |
|                                                                       |
| [      Remote Restart timer is 120 seconds]{.c1}                      |
|                                                                       |
| [      Address families by peer:]{.c1}                                |
|                                                                       |
| [        none]{.c1}                                                   |
|                                                                       |
| [  Graceful restart informations:]{.c1}                               |
|                                                                       |
| [    End-of-RIB send: IPv4 Unicast]{.c1}                              |
|                                                                       |
| [    End-of-RIB received: IPv4 Unicast]{.c1}                          |
|                                                                       |
| [  Message statistics:]{.c1}                                          |
|                                                                       |
| [    Inq depth is 0]{.c1}                                             |
|                                                                       |
| [    Outq depth is 0]{.c1}                                            |
|                                                                       |
| [                         Sent       Rcvd]{.c1}                       |
|                                                                       |
| [    Opens:                  1          1]{.c1}                       |
|                                                                       |
| [    Notifications:          0          0]{.c1}                       |
|                                                                       |
| [    Updates:                2          1]{.c1}                       |
|                                                                       |
| [    Keepalives:           107        107]{.c1}                       |
|                                                                       |
| [    Route Refresh:          0          0]{.c1}                       |
|                                                                       |
| [    Capability:             0          0]{.c1}                       |
|                                                                       |
| [    Total:                110        109]{.c1}                       |
|                                                                       |
| [  Minimum time between advertisement runs is 0 seconds]{.c1}         |
|                                                                       |
| [  Update source is lo]{.c1}                                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ For address family: IPv4 Unicast]{.c1}                              |
|                                                                       |
| [  Update group 1, subgroup 1]{.c1}                                   |
|                                                                       |
| [  Packet Queue length 0]{.c1}                                        |
|                                                                       |
| [  Route-Reflector Client]{.c1}                                       |
|                                                                       |
| [  Community attribute sent to this neighbor(all)]{.c1}               |
|                                                                       |
| [  0 accepted prefixes]{.c1}                                          |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  Connections established 1; dropped 0]{.c1}                         |
|                                                                       |
| [  Last reset never]{.c1}                                             |
|                                                                       |
| [Local host: 10.255.255.101, Local port: 179]{.c1}                    |
|                                                                       |
| [Foreign host: 10.255.255.2, Foreign port: 52341]{.c1}                |
|                                                                       |
| [Nexthop: 10.255.255.101]{.c1}                                        |
|                                                                       |
| [Nexthop global: ::]{.c1}                                             |
|                                                                       |
| [Nexthop local: ::]{.c1}                                              |
|                                                                       |
| [BGP connection: non shared network]{.c1}                             |
|                                                                       |
| [BGP Connect Retry Timer in Seconds: 10]{.c1}                         |
|                                                                       |
| [Read thread: on  Write thread: on]{.c7}                              |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.42c3e563f797696e1af760f71cfe392a331574da}[]{#t.274}

[Important things to observe:]{.c15}

-   [BGP timers are only updated after peering is re-established]{.c15}
-   [BGP timers do not have to match between neighbors]{.c50}

```{=html}
<!-- -->
```
-   [If they do not match, the lowest timers will be used]{.c15}

```{=html}
<!-- -->
```
-   [The hold timer should]{.c50}[ be 3 times ]{.c50}[the Keepalive
    timer]{.c15}

[]{.c69 .c54}

Configure Bidirectional Forwarding Detection (BFD) {#h.25wj64z5ypsa .c23}
--------------------------------------------------

10. [On leaf01]{.c6}[: Apply BFD to the neighborship with
    spine01.]{.c50}[ ]{.c69 .c54}

[]{#t.8496b93295a5da90d1f239c3ced758fbb7a80047}[]{#t.275}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add bgp neighbor 10.255.255.101      |
| bfd]{.c17}                                                            |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c15}

11. [On leaf02]{.c57 .c47}[: Apply BFD to the neighborship with
    spine01.]{.c50} 

[]{#t.14eaeb42c017e4d8629dee0940dc54f1dffcc95e}[]{#t.276}

+-----------------------------------------------------------------------+
| [cumulus\@leaf02:\~\$ ]{.c7}[net add bgp neighbor 10.255.255.101      |
| bfd]{.c17}                                                            |
|                                                                       |
| [cumulus\@leaf02:\~\$ ]{.c7}[net commit]{.c17}                        |
+-----------------------------------------------------------------------+

[]{.c15}

[]{.c15}

[]{.c15}

12. [On spine01: Apply BFD to the neighborship with leaf01 and
    leaf02]{.c50} 

[]{#t.4a5d257b51b97d887243df38b210cdabb8df9ace}[]{#t.277}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp neighbor 10.255.255.1       |
| bfd]{.c17}                                                            |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net add bgp neighbor 10.255.255.2       |
| bfd]{.c17}                                                            |
|                                                                       |
| [cumulus\@spine01:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}             |
+-----------------------------------------------------------------------+

[]{.c69 .c54}

[Verify BFD is in use]{.c4} {#h.yxon9ih250nw .c23}
---------------------------

13. [On spine01: Check PTM output to verify the BFD sessions are up.
    ]{.c50}[PTM is the daemon which handles BFD sessions in Cumulus
    Linux. When a BFD session is created in FRR configuration, FRR
    registers a BFD session to the PTM daemon which handles the setup
    and ongoing maintenance of the BFD session. If the BFD session goes
    down, PTM then informs FRR which will take the appropriate action to
    remove the neighbor. Here we're passing the ']{.c75}[-b]{.c40
    .c75}[' flag to the ]{.c75}[ptmctl ]{.c40 .c75}[command which will
    show us BFD related information only. As BFD packets are generated
    by the Control Plane, they can also be sniffed with utilities such
    as ]{.c75}[tcpdump]{.c40 .c75}[. You could see them here using
    ']{.c75}[sudo tcpdump -enni swp1]{.c40 .c75}[']{.c75}[ ]{.c69 .c54}

[]{.c69 .c54}

[]{#t.2da452c807c4f22b94f91000bd58365d7a19910e}[]{#t.278}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[ptmctl -b]{.c17}                        |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [port  peer          state  local           type      diag  vrf       |
|  ]{.c1}                                                               |
|                                                                       |
| [                                                                     |
| ]{.c1}                                                                |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-     |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [N/A   10.255.255.1  ]{.c7}[Up]{.c40 .c12}[     10.255.255.101        |
|  multihop  N/A   N/A  ]{.c1}                                          |
|                                                                       |
| [N/A   10.255.255.2  ]{.c7}[Up]{.c40 .c12}[     10.255.255.101        |
|  multihop  N/A   N/A]{.c7}[  ]{.c1}                                   |
+-----------------------------------------------------------------------+

[]{.c15}

14. [On spine01: Check the individual BGP neighborships to verify BFD is
    in place.]{.c15}

[]{.c15}

[]{#t.077ac812f13370c16d4e22b64f0116e6b14438a7}[]{#t.279}

+-----------------------------------------------------------------------+
| [cumulus\@spine01:\~\$ ]{.c7}[net show bgp neighbor                   |
| 10.255.255.1]{.c17}                                                   |
|                                                                       |
| [BGP neighbor is 10.255.255.1, remote AS 65100, local AS 65100,       |
| internal link]{.c1}                                                   |
|                                                                       |
| [Hostname: leaf01]{.c1}                                               |
|                                                                       |
| [  BGP version 4, remote router ID 10.255.255.1]{.c1}                 |
|                                                                       |
| [  BGP state = Established, up for 00:03:24]{.c7}                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [\<\...output not shown for brevity\...\>]{.c7 .c43}                  |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  BFD: Type: multi hop]{.c29 .c12}                                   |
|                                                                       |
| [    Detect Multiplier: 3, Min Rx interval: 300, Min Tx interval:     |
| 300]{.c29 .c12}                                                       |
|                                                                       |
| [    Status: Up, Last update: 0:00:01:00]{.c29 .c12}                  |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.c13e983898a4c7760fb3b879f20c4516921f4e3a}[]{#t.280}

[Important things to observe:]{.c15}

-   [BFD sessions are tracked through PTM]{.c15}
-   [BFD is enabled per routing protocol]{.c15}
-   [BFD needs to be enabled on both endpoints]{.c15}

[]{.c4} {#h.34313tcpjics .c23 .c137}
-------

Configure PTM link verification for all routing adjacencies {#h.uo2k1bjvlag3 .c23}
-----------------------------------------------------------

15. [On leaf01]{.c6}[: ]{.c50}[Check the current PTM output.]{.c50}

[]{#t.8646b19dbbea75bbc743871b1d61dd888635af9d}[]{#t.281}

+-----------------------------------------------------------------------+
| [cumulus\@l]{.c7}[eaf01:\~\$ ]{.c7}[ptmctl]{.c57 .c61 .c51}           |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [port   cbl     BFD     BFD   BFD    BFD   ]{.c1}                     |
|                                                                       |
| [       status  status  peer  local  type  ]{.c1}                     |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-                                                 |
| \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [swp46  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp48  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp51  pass]{.c40 .c58}[    N/A     N/A   N/A    N/A   ]{.c1}        |
|                                                                       |
| [swp49  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp50  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp52  N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [eth0   pass    N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp1   N/A     N/A     N/A   N/A    N/A   ]{.c1}                     |
|                                                                       |
| [swp2   N/A     N/A     N/A   N/A    N/A]{.c7}[ ]{.c1}                |
+-----------------------------------------------------------------------+

[]{.c15}

16. [On leaf01]{.c6}[: Check that PTM status shows as disabled for the
    interface which connects to the spine01 switch. ]{.c15}

[]{#t.77dd445d24843ebcefe9c05b5c9cd391ec75a98d}[]{#t.282}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net show interface swp51]{.c17}          |
|                                                                       |
| [    Name   MAC                Speed  MTU   Mode]{.c1}                |
|                                                                       |
| [\--  \-\-\-\--  \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--  \-\-\-\--         |
|  \-\-\--  \-\-\-\-\-\-\-\-\-\-\--]{.c1}                               |
|                                                                       |
| [UP  swp51  44:38:39:00:00:21  1G     1500  Interface/L3]{.c1}        |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [IP Details]{.c1}                                                     |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--                    |
|  \-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1}                                  |
|                                                                       |
| [IP:                        10.255.255.1/32]{.c1}                     |
|                                                                       |
| [IP Neighbor(ARP) Entries:  1]{.c1}                                   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cl-netstat counters]{.c1}                                            |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1}                          |
|                                                                       |
| [ RX\_OK  RX\_ERR  RX\_DRP  RX\_OVR   TX\_OK  TX\_ERR  TX\_DRP        |
|  TX\_OVR]{.c1}                                                        |
|                                                                       |
| [\-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--      |
|  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--]{.c1}                          |
|                                                                       |
| [453500       0       1       0  452599       0       0       0]{.c1} |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [LLDP Details]{.c1}                                                   |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\--]{.c1}                                        |
|                                                                       |
| [LocalPort  RemotePort(RemoteHost)]{.c1}                              |
|                                                                       |
| [\-\-\-\-\-\-\-\--  \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [swp51      swp1(spine01)]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Routing]{.c1}                                                        |
|                                                                       |
| [\-\-\-\-\-\--]{.c1}                                                  |
|                                                                       |
| [  Interface swp51 is up, line protocol is up]{.c1}                   |
|                                                                       |
| [  Link ups:       0    last: (never)]{.c1}                           |
|                                                                       |
| [  Link downs:     0    last: (never)]{.c1}                           |
|                                                                       |
| [  PTM status: disabled]{.c29 .c58}                                   |
|                                                                       |
| [  vrf: default]{.c1}                                                 |
|                                                                       |
| [  index 12 metric 0 mtu 1500 speed 1000]{.c1}                        |
|                                                                       |
| [  flags: \<UP,BROADCAST,RUNNING,MULTICAST\>]{.c1}                    |
|                                                                       |
| [  Type: Ethernet]{.c1}                                               |
|                                                                       |
| [  HWaddr: 44:38:39:00:00:21]{.c1}                                    |
|                                                                       |
| [  inet 10.255.255.1/32 unnumbered]{.c1}                              |
|                                                                       |
| [  inet6 fe80::4638:39ff:fe00:21/64]{.c1}                             |
|                                                                       |
| [  Interface Type Other]{.c7}                                         |
+-----------------------------------------------------------------------+

[]{.c15}

17. [On leaf01]{.c6}[: Enable the enforcement of PTM link verification
    for BGP neighbor relationships to form. ]{.c50}[In Cumulus
    ptm-enable is already part of the default interface configuration
    within FRR. To enable the ptm support for FRR as a whole we need to
    add a global configuration line in the FRR configuration. This is
    performed with the following NCLU commands.]{.c75}[ ]{.c15}

[]{#t.61e40ad7aa9923c6e835c3a3c1cf9dc6da68aed1}[]{#t.283}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net add routing ptm-enable ]{.c57 .c61   |
| .c51}                                                                 |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net commit]{.c57 .c61 .c51}[ ]{.c1}      |
+-----------------------------------------------------------------------+

[]{.c15}

18. [On leaf01]{.c6}[: Check that PTM link verification is passing.
    ]{.c50}[In the case of a virtual topology like this one the link
    cannot become miswired. ]{.c64 .c54}

[]{#t.b08303c480aeeaeb65969c14b73e905218bf8ae4}[]{#t.284}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net show interface swp51]{.c89 .c102     |
| .c57 .c61}                                                            |
|                                                                       |
| [    Name   MAC                Speed  MTU   Mode]{.c1}                |
|                                                                       |
| [\--  \-\-\-\--  \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--  \-\-\-\--         |
|  \-\-\--  \-\-\-\-\-\-\-\-\-\-\--]{.c1}                               |
|                                                                       |
| [UP  swp51  44:38:39:00:00:21  1G     1500  Interface/L3]{.c1}        |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [IP Details]{.c1}                                                     |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--                    |
|  \-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1}                                  |
|                                                                       |
| [IP:                        10.255.255.1/32]{.c1}                     |
|                                                                       |
| [IP Neighbor(ARP) Entries:  1]{.c1}                                   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cl-netstat counters]{.c1}                                            |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1}                          |
|                                                                       |
| [ RX\_OK  RX\_ERR  RX\_DRP  RX\_OVR   TX\_OK  TX\_ERR  TX\_DRP        |
|  TX\_OVR]{.c1}                                                        |
|                                                                       |
| [\-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--      |
|  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--]{.c1}                          |
|                                                                       |
| [453811       0       1       0  452882       0       0       0]{.c1} |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [LLDP Details]{.c1}                                                   |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\--]{.c1}                                        |
|                                                                       |
| [LocalPort  RemotePort(RemoteHost)]{.c1}                              |
|                                                                       |
| [\-\-\-\-\-\-\-\--  \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [swp51      swp1(spine01)]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Routing]{.c1}                                                        |
|                                                                       |
| [\-\-\-\-\-\--]{.c1}                                                  |
|                                                                       |
| [  Interface swp51 is up, line protocol is up]{.c1}                   |
|                                                                       |
| [  Link ups:       0    last: (never)]{.c1}                           |
|                                                                       |
| [  Link downs:     0    last: (never)]{.c1}                           |
|                                                                       |
| [  PTM status: pass]{.c29 .c58}                                       |
|                                                                       |
| [  vrf: default]{.c1}                                                 |
|                                                                       |
| [  index 12 metric 0 mtu 1500 speed 1000]{.c1}                        |
|                                                                       |
| [  flags: \<UP,BROADCAST,RUNNING,MULTICAST\>]{.c1}                    |
|                                                                       |
| [  Type: Ethernet]{.c1}                                               |
|                                                                       |
| [  HWaddr: 44:38:39:00:00:21]{.c1}                                    |
|                                                                       |
| [  inet 10.255.255.1/32 unnumbered]{.c1}                              |
|                                                                       |
| [  inet6 fe80::4638:39ff:fe00:21/64]{.c1}                             |
|                                                                       |
| [  Interface Type Other]{.c7}[ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c15}

[]{#t.19adad60d36c5aeb6da0ea4c464ba6b8da2ed7fb}[]{#t.285}

[Important things to observe:]{.c15}

-   [PTM link verification can be used to confirm the connection before
    peering BGP]{.c15}

[]{.c15}

19. [(OPTIONAL) ]{.c50}[On leaf01]{.c6}[: Alter the Topology.dot file to
    make PTM verification fail for the interface connected to spine01.
    ]{.c50}[In the topology.dot file we're modifying the first link to
    change the name of the spine from spine01 to spine0A but you can
    change the spine's name or interface to whatever you like. ]{.c64
    .c54}

[]{#t.c3d29fda13251a41d4c978097c10de19bd81b844}[]{#t.286}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo cp /etc/ptm.d/topology.dot          |
| /etc/ptm.d/topology.dot.original]{.c57 .c61 .c51}                     |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo nano /etc/ptm.d/topology.dot]{.c57  |
| .c61 .c51}                                                            |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [  GNU nano 2.2.6              File: /etc/ptm.d/topology.dot          |
|       Modified]{.c37}[  ]{.c1}                                        |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [graph vx {]{.c1}                                                     |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ \# Leaf to Spine Connections]{.c1}                                  |
|                                                                       |
| [ \"leaf01\":\"swp51\" \-- \"spine0]{.c7}[A]{.c57 .c184 .c61          |
| .c90}[\":\"swp1\"]{.c1}                                               |
|                                                                       |
| [ \"leaf02\":\"swp51\" \-- \"spine01\":\"swp2\"]{.c1}                 |
|                                                                       |
| [ \"leaf03\":\"swp51\" \-- \"spine01\":\"swp3\"]{.c1}                 |
|                                                                       |
| [ \"leaf04\":\"swp51\" \-- \"spine01\":\"swp4\"]{.c1}                 |
|                                                                       |
| [ \"leaf01\":\"swp52\" \-- \"spine02\":\"swp1\"]{.c1}                 |
|                                                                       |
| [ \"leaf02\":\"swp52\" \-- \"spine02\":\"swp2\"]{.c1}                 |
|                                                                       |
| [ \"leaf03\":\"swp52\" \-- \"spine02\":\"swp3\"]{.c1}                 |
|                                                                       |
| [ \"leaf04\":\"swp52\" \-- \"spine02\":\"swp4\"]{.c1}                 |
|                                                                       |
| [ \"exit01\":\"swp51\" \-- \"spine01\":\"swp30\"]{.c1}                |
|                                                                       |
| [ \"exit01\":\"swp52\" \-- \"spine02\":\"swp30\"]{.c1}                |
|                                                                       |
| [ \"exit02\":\"swp51\" \-- \"spine01\":\"swp29\"]{.c1}                |
|                                                                       |
| [ \"exit02\":\"swp52\" \-- \"spine02\":\"swp29\"]{.c1}                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [ \# Leaf Peerlink Connections]{.c1}                                  |
|                                                                       |
| [                               ]{.c7}[\[ Read 77 lines \]]{.c37 .c89 |
| .c106}                                                                |
|                                                                       |
| [\^G]{.c37}[ Get Help  ]{.c7}[\^O]{.c37}[ WriteOut                    |
|  ]{.c7}[\^R]{.c37}[ Read File ]{.c7}[\^Y]{.c37}[ Prev Page            |
| ]{.c7}[\^K]{.c37}[ Cut Text  ]{.c7}[\^C]{.c37}[ Cur Pos]{.c1}         |
|                                                                       |
| [\^X]{.c37}[ Exit      ]{.c7}[\^J]{.c37}[ Justify                     |
| ]{.c7}[\^W]{.c37}[ Where Is  ]{.c7}[\^V]{.c37}[ Next Page             |
| ]{.c7}[\^U]{.c37}[ UnCut Text]{.c7}[\^T]{.c37}[ To Spell]{.c1}        |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

[Restart the PTM daemon and look again at the output of]{.c75}[ net show
interface swp51]{.c50 .c61}[ to see that ]{.c75}["PTM status"]{.c50
.c61}[ has changed to ]{.c75}["fail"]{.c50 .c61}[ ]{.c75}[.]{.c64 .c54}

[]{#t.f0b5bdd04e903f74b446ce55bd5b7f8fde88bf9e}[]{#t.287}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo systemctl restart ptmd]{.c17}       |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[net show interface swp51]{.c89 .c102     |
| .c57 .c61}                                                            |
|                                                                       |
| [    Name   MAC                Speed  MTU   Mode]{.c1}                |
|                                                                       |
| [\--  \-\-\-\--  \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--  \-\-\-\--         |
|  \-\-\--  \-\-\-\-\-\-\-\-\-\-\--]{.c1}                               |
|                                                                       |
| [UP  swp51  44:38:39:00:00:21  1G     1500  Interface/L3]{.c1}        |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [IP Details]{.c1}                                                     |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--                    |
|  \-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1}                                  |
|                                                                       |
| [IP:                        10.255.255.1/32]{.c1}                     |
|                                                                       |
| [IP Neighbor(ARP) Entries:  1]{.c1}                                   |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [cl-netstat counters]{.c1}                                            |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1}                          |
|                                                                       |
| [ RX\_OK  RX\_ERR  RX\_DRP  RX\_OVR   TX\_OK  TX\_ERR  TX\_DRP        |
|  TX\_OVR]{.c1}                                                        |
|                                                                       |
| [\-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--      |
|  \-\-\-\-\--  \-\-\-\-\--  \-\-\-\-\--]{.c1}                          |
|                                                                       |
| [454265       0       1       0  453288       0       0       0]{.c1} |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [LLDP Details]{.c1}                                                   |
|                                                                       |
| [\-\-\-\-\-\-\-\-\-\-\--]{.c1}                                        |
|                                                                       |
| [LocalPort  RemotePort(RemoteHost)]{.c1}                              |
|                                                                       |
| [\-\-\-\-\-\-\-\--  \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--]{.c1} |
|                                                                       |
| [swp51      swp1(spine01)]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Routing]{.c1}                                                        |
|                                                                       |
| [\-\-\-\-\-\--]{.c1}                                                  |
|                                                                       |
| [  Interface swp51 is up, line protocol is up]{.c1}                   |
|                                                                       |
| [  Link ups:       0    last: (never)]{.c1}                           |
|                                                                       |
| [  Link downs:     1    last: 2019/12/18 17:52:13.93]{.c1}            |
|                                                                       |
| [  PTM status: fail]{.c29 .c58}                                       |
|                                                                       |
| [  vrf: default]{.c1}                                                 |
|                                                                       |
| [  index 12 metric 0 mtu 1500 speed 1000]{.c1}                        |
|                                                                       |
| [  flags: \<UP,BROADCAST,RUNNING,MULTICAST\>]{.c1}                    |
|                                                                       |
| [  Type: Ethernet]{.c1}                                               |
|                                                                       |
| [  HWaddr: 44:38:39:00:00:21]{.c1}                                    |
|                                                                       |
| [  inet 10.255.255.1/32 unnumbered]{.c1}                              |
|                                                                       |
| [  inet6 fe80::4638:39ff:fe00:21/64]{.c1}                             |
|                                                                       |
| [  Interface Type Other]{.c7}[ ]{.c1}                                 |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

[You will also notice the output from ]{.c75}[net show bgp sum]{.c50
.c61}[  will show the spine01 peer]{.c75}[ is no longer established if
viewed.]{.c75}

[]{#t.bb41a34b4499f6057ebba57d7da06d692b92d8d0}[]{#t.288}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[net show bgp summary]{.c17}              |
|                                                                       |
| [show bgp ipv4 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [BGP router identifier 10.255.255.1, local AS number 65100 vrf-id     |
| 0]{.c1}                                                               |
|                                                                       |
| [BGP table version 4]{.c1}                                            |
|                                                                       |
| [RIB entries 0, using 0 bytes of memory]{.c1}                         |
|                                                                       |
| [Peers 1, using 19 KiB of memory]{.c1}                                |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Neighbor                V         AS MsgRcvd MsgSent   TblVer  InQ   |
| OutQ  Up/Down State/PfxRcd]{.c1}                                      |
|                                                                       |
| [spine01(10.255.255.101)]{.c40 .c58}[ 4      65100   79885   79876    |
|      0    0    0 00:00:55       ]{.c7}[Active]{.c29 .c58}             |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [Total number of neighbors 1]{.c1}                                    |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp ipv6 unicast summary]{.c1}                                  |
|                                                                       |
| [=============================]{.c1}                                  |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| []{.c1}                                                               |
|                                                                       |
| [show bgp l2vpn evpn summary]{.c1}                                    |
|                                                                       |
| [===========================]{.c1}                                    |
|                                                                       |
| [% No BGP neighbors found]{.c1}                                       |
+-----------------------------------------------------------------------+

[]{.c89 .c50 .c106 .c61}

[Restore the original topology.dot file.]{.c64 .c54}

[]{#t.76a68929a4d7cab4805bc21286db45b285a0ed75}[]{#t.289}

+-----------------------------------------------------------------------+
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo cp /etc/ptm.d/topology.dot.original |
| /etc/ptm.d/topology.dot]{.c57 .c61 .c51}                              |
|                                                                       |
| [cumulus\@leaf01:\~\$ ]{.c7}[sudo systemctl restart                   |
| ptmd.service]{.c57 .c61 .c51}                                         |
+-----------------------------------------------------------------------+

[]{.c64 .c54}

[]{.c64 .c54}

**This concludes Lab 12.**

<!-- AIR:page -->
### About Cumulus Networks

Cumulus Networks is leading the transformation of bringing web-scale
networking to enterprise cloud. Its network switch, Cumulus Linux, is
the only solution that allows you to affordably build and efficiently
operate your network like the world's largest data center operators,
unlocking vertical network stacks. By allowing operators to use standard
hardware components, Cumulus Linux offers unprecedented operational
speed and agility, at the industry's most competitive cost. Cumulus
Networks has received venture funding from Andreessen Horowitz, Battery
Ventures, Sequoia Capital, Peter Wagner and four of the original VMware
founders. 

For more information visit
[cumulusnetworks.com](https://www.google.com/url?q=http://cumulusnetworks.com&sa=D&ust=1578375996597000)

or follow
[@cumulusnetworks](https://www.google.com/url?q=https://twitter.com/cumulusnetworks&sa=D&ust=1578375996598000)

©2017 Cumulus Networks. All rights reserved

CUMULUS, the Cumulus Logo, CUMULUS NETWORKS, and the Rocket Turtle Logo (the "Marks")  
are trademarks and service marks of Cumulus Networks, Inc. in the U.S. and other  
countries. You are not permitted to use the Marks without the prior written  
consent of Cumulus Networks. The registered trademark Linux is used pursuant  
to a sublicense from LMI, the exclusive licensee of Linus Torvalds, owner of  
the mark on a worldwide basis. All other marks are used under fair use or license  
from their respective owners.


>[cumulusnetworks.com](https://www.google.com/url?q=http://cumulusnetworks.com/&sa=D&ust=1578375996601000)

